#include <jni.h>
#include <string>

extern "C"
JNIEXPORT jstring JNICALL
Java_com_amcblanand_android_activity_SplashActivity_liveURLFromJNI(JNIEnv *env, jobject instance) {
    std::string liveURL = "https://anandmerc.tmsplmailpro.com/";
    return env->NewStringUTF(liveURL.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_amcblanand_android_activity_SplashActivity_stagingURLFromJNI(JNIEnv *env,
                                                                      jobject instance) {
//    std::string stagingURL = "http://192.168.200.134/MAutoBankSecClient/"; /* CLIENT ID - 442001284 */
    std::string stagingURL = "http://demo.tmsys.co.in:9091/MAutoBankSecClient/"; // SACHINBHAI
//    std::string stagingURL = "http://demo.tmsys.co.in:9099/MAutoBankSecClient/"; // KHUSHBU
    return env->NewStringUTF(stagingURL.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_amcblanand_android_activity_SplashActivity_publicKeyFromJNI(JNIEnv *env,
                                                                     jobject instance) {
    std::string publicKey = "M}^C?xa]gG[7qQ3=.5Wkb~";
    return env->NewStringUTF(publicKey.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_amcblanand_android_activity_SplashActivity_privateKeyFromJNI(JNIEnv *env,
                                                                      jobject instance) {
    std::string privateKey = "D1JoeVaA0yMaNt6I";
    return env->NewStringUTF(privateKey.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_amcblanand_android_activity_SplashActivity_secretKeyFromJNI(JNIEnv *env,
                                                                     jobject instance) {
    std::string secretKey = "b8jk#R!gjhp1^3~2";
    return env->NewStringUTF(secretKey.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_amcblanand_android_activity_SplashActivity_authKeyFromJNI(JNIEnv *env,
                                                                   jobject instance) {
    std::string authKey = "Basic 2L@7/b3hE-1$0h0-wo@9/2R9n@01+2H1";
    return env->NewStringUTF(authKey.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_amcblanand_android_activity_SplashActivity_connectionIDFromJNI(JNIEnv *env,
                                                                        jobject instance) {
    std::string connectionID = "22";
    return env->NewStringUTF(connectionID.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_amcblanand_android_activity_SplashActivity_apiVersionNumberFromJNI(JNIEnv *env,
                                                                            jobject instance) {
//    std::string apiVersionNumber = "2"; //OLD
//    std::string apiVersionNumber = "3";
//    std::string apiVersionNumber = "4";
    std::string apiVersionNumber = "5";
    return env->NewStringUTF(apiVersionNumber.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_amcblanand_android_activity_SplashActivity_stagingConnectionIDFromJNI(JNIEnv *env,
                                                                               jobject instance) {
//    std::string stagingConnectionID = "2";
    std::string stagingConnectionID = "17";
    return env->NewStringUTF(stagingConnectionID.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_amcblanand_android_activity_SplashActivity_deviceTypeFromJNI(JNIEnv *env,
                                                                      jobject instance) {
    std::string deviceType = "A";
    return env->NewStringUTF(deviceType.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_amcblanand_android_activity_SplashActivity_appVersionNumberFromJNI(JNIEnv *env,
                                                                            jobject instance) {
//    std::string appVersionNumber = "16"; //OLD
//    std::string appVersionNumber = "18";
//    std::string appVersionNumber = "24";
    std::string appVersionNumber = "30";
    return env->NewStringUTF(appVersionNumber.c_str());
}
