package com.amcblanand.android.adapter;

import android.app.AlertDialog;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.callbackmodel.DebitCardCallBack;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.DebitCardPojo;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by amit.singh on 12/27/2017.
 */

public class DebitCardActiveDeactivateAdapter extends RecyclerView.Adapter<DebitCardActiveDeactivateAdapter.VHDebitCard> {

	private static final String APPTAG = Constants.TAG + DebitCardActiveDeactivateAdapter.class.getSimpleName();


	private Context context;
	private List<DebitCardPojo> debitCardPojoList;
	private Progress progress;
	private DecryptData decryptData;
	private boolean switchState;

	public DebitCardActiveDeactivateAdapter(Context context, List<DebitCardPojo> debitCardPojoList) {
		this.context = context;
		this.debitCardPojoList = debitCardPojoList;

		progress = new Progress(context);
		decryptData = new DecryptData();
	}

	@Override
	public VHDebitCard onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(context).inflate(R.layout.single_debit_card_a_d_row_view, parent, false);
		return new VHDebitCard(view);
	}


	@Override
	public void onBindViewHolder(final VHDebitCard holder, int position) {
		holder.tvCardNumber.setText(debitCardPojoList.get(position).getATM_CARD_SRNO());

		if (debitCardPojoList.get(position).getATM_EXPIRE_DT().equals("null")) {
			holder.tvExDate.setText("");
		} else {
			holder.tvExDate.setText("Expire Date:- " + debitCardPojoList.get(position).getATM_EXPIRE_DT());
		}


		if (debitCardPojoList.get(position).getATM_YN().equals("Active")) {
			holder.switchDebitCard.setChecked(true);
			switchState = true;
			holder.tvStatus.setText("Active");
		} else {
			holder.switchDebitCard.setChecked(false);
			switchState = false;
			holder.tvStatus.setText("In-Active");
		}

		holder.switchDebitCard.setOnClickListener(view -> {
			if (switchState) {
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("Do you want to Deactivate Debit-card?");
				builder.setPositiveButton("Ok", (dialog, id) -> {
					progress.createDialog(false);
					switchState = false;
					progress.dialogMessage("Please wait..");
					progress.showDialog();
					BlockUnBlockATMCard("B");
				});
				builder.setNegativeButton("Cancel", (dialog, id) -> {
					if (switchState) {
						holder.switchDebitCard.setChecked(true);
					}
					dialog.dismiss();
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			} else {
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("Do you want to Active Debit-card?");
				builder.setPositiveButton("Ok", (dialog, id) -> {
					progress.createDialog(false);
					progress.dialogMessage("Please wait..");
					progress.showDialog();
					switchState = true;
					BlockUnBlockATMCard("U");
				});
				builder.setNegativeButton("Cancel", (dialog, id) -> {

					if (!switchState) {
						holder.switchDebitCard.setChecked(false);
					}

					dialog.dismiss();
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});
	}

	@Override
	public int getItemCount() {
		return debitCardPojoList.size();
	}

	private void BlockUnBlockATMCard(String activeType) {
		try {
			JSONObject reqJsonObject = new JSONObject();
			reqJsonObject.put("function", "BlockUnBlockATMCard");

			JSONObject reqParamJsonObject = new JSONObject();
			reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
			reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
			reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
			reqParamJsonObject.put("ArgEcsAccCode", Constants.ACCOUNT_NUMBER);
			reqParamJsonObject.put("ArgBlockUnblock", activeType);

			reqJsonObject.putOpt("parameter", reqParamJsonObject);

			String dataValue = reqJsonObject.toString();
			Timber.d(APPTAG + " - " + dataValue);

			API.getInstance().encryptRequest(context, dataValue, Constants.PUBLIC_KEY,
					Constants.CONNECTION_ID, Constants.AUTH_KEY,
					Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
					new RetrofitCallbacks<ResponseBody>(context) {
						@Override
						public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
							super.onResponse(call, response);

							try {
								String responseString = response.body().string();
								Timber.e(responseString, APPTAG);
								String responseDataValue = decryptData.parseResponse(responseString);
								Timber.e(responseDataValue, APPTAG);

								progress.hideDialog();
								JSONObject jsonObject = new JSONObject(responseDataValue);
								if (jsonObject.getString("Status").equals("1")) {
									EventBus.getDefault().post(new DebitCardCallBack("Y"));
									Toast.makeText(context, jsonObject.getString("ResponseMessage"), Toast.LENGTH_SHORT).show();
								} else {
									Toast.makeText(context, jsonObject.getString("ResponseMessage"), Toast.LENGTH_SHORT).show();
								}

							} catch (Exception e) {
								e.printStackTrace();
								progress.hideDialog();
							}
						}

						@Override
						public void onFailure(Call<ResponseBody> call, Throwable t) {
							super.onFailure(call, t);
							progress.hideDialog();
							Toast.makeText(context, "Something wrong! Please try after sometimes.", Toast.LENGTH_SHORT).show();
						}
					});

		} catch (Exception e) {
			e.printStackTrace();
			progress.hideDialog();
		}
	}

	public class VHDebitCard extends RecyclerView.ViewHolder {

		@BindView(R.id.tv_card_number)
		TextView tvCardNumber;
		@BindView(R.id.switch_debit_card)
		SwitchCompat switchDebitCard;
		@BindView(R.id.tv_ex_date)
		TextView tvExDate;
		@BindView(R.id.tv_status)
		TextView tvStatus;

		public VHDebitCard(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
		}
	}

}
