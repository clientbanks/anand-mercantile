package com.amcblanand.android.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;

import com.amcblanand.android.R;
import com.amcblanand.android.activity.VerifySecurityQuestionActivity;
import com.amcblanand.android.model.SQuestions;
import timber.log.Timber;

/**
 * Created by amit.singh on 09-03-2018.
 */

public class VerifySecurityQuestionAdapter extends RecyclerView.Adapter<VerifySecurityQuestionAdapter.MyViewHolder> {

    private List<SQuestions> responseJSONBeans;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        private EditText editText;
        private ImageView ivError;
        private TextView tvAnsNumber;

        public MyViewHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.tv_row_forgot_s_question);
            editText = view.findViewById(R.id.et_row_forgot_s_question);
            ivError = view.findViewById(R.id.iv_error);
            tvAnsNumber = view.findViewById(R.id.tv_ans_number);
        }
    }


    public VerifySecurityQuestionAdapter(Context context, List<SQuestions> responseJSONBeans) {
        this.context = context;
        this.responseJSONBeans = responseJSONBeans;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_verify_security_questions, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SQuestions sQuestions = responseJSONBeans.get(holder.getAdapterPosition());

        holder.textView.setText(sQuestions.getQUESTION());

        Timber.e(sQuestions.getIsError(), "ADAPTER");
        int ansNo = position + 1;
        holder.tvAnsNumber.setText(String.valueOf(ansNo));

        if (sQuestions.getIsError().equalsIgnoreCase("T")) {
            holder.ivError.setVisibility(View.GONE);
        } else {
            holder.ivError.setVisibility(View.VISIBLE);
        }


        holder.editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                VerifySecurityQuestionActivity.stringStringForgotMap.put("" + holder.getAdapterPosition(), "" + sQuestions.getSRNO() + "~"
                        + s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return responseJSONBeans.size();

    }
}