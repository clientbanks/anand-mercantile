package com.amcblanand.android.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;


import java.util.List;

import com.amcblanand.android.R;
import com.amcblanand.android.model.BannerListModel;

/**
 * Created by rakshit.sathwara on 1/25/2018.
 */

public class ImageSliderAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    private List<BannerListModel> bannerListModelList;

    public ImageSliderAdapter(Context context, List<BannerListModel> bannerListModels) {
        mContext = context;
        bannerListModelList = bannerListModels;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return bannerListModelList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_slider_view, container, false);

        ImageView imageView = itemView.findViewById(R.id.slider_imageView);
        Glide.with(mContext).load(bannerListModelList.get(position).getPATH()).apply(new RequestOptions().error(R.drawable.vvcc_banner).placeholder(R.drawable.vvcc_banner)).into(imageView);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}