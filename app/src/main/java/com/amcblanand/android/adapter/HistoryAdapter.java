package com.amcblanand.android.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.model.HistoryModel;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.VHHistory> {

    private Context context;
    private List<HistoryModel> historyModelList;
    private String transType;

    public HistoryAdapter(Context context, List<HistoryModel> historyModelList, String TYPE) {
        this.context = context;
        this.historyModelList = historyModelList;
        transType = TYPE;
    }

    @Override
    public VHHistory onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_history_view, parent, false);
        return new VHHistory(view);
    }

    @Override
    public void onBindViewHolder(VHHistory holder, int position) {

        if (transType.equals(historyModelList.get(position).getPAYMENT_TYPE())) {
            holder.linearView.setVisibility(View.VISIBLE);
            holder.tvDate.setText(historyModelList.get(position).getTDT());
            holder.tvType.setText("\u20B9 " + historyModelList.get(position).getAMOUNT());
            holder.tcDesc.setText(historyModelList.get(position).getBENE_DETAILS());
        } else {
            holder.linearView.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return historyModelList.size();
    }

    public class VHHistory extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_type)
        TextView tvType;
        @BindView(R.id.tc_desc)
        TextView tcDesc;
        @BindView(R.id.linear_view)
        LinearLayout linearView;

        public VHHistory(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
