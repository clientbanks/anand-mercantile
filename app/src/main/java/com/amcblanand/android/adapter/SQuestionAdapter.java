package com.amcblanand.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.util.List;

import com.amcblanand.android.R;
import com.amcblanand.android.model.SQuestions;

/**
 * Created by amit.singh on 12/27/2017.
 */

public class SQuestionAdapter extends BaseAdapter {

    private Context context;
    private List<SQuestions> sQuestions;
    private LayoutInflater mInflater;
    private int position;

    public SQuestionAdapter(Context context, List<SQuestions> sQuestions) {
        this.context = context;
        this.sQuestions = sQuestions;
        this.position = position;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return sQuestions.size();
    }

    @Override
    public Object getItem(int i) {
        return sQuestions.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;

        if (view == null) {
            view = mInflater.inflate(R.layout.single_que_row_view, null);
            viewHolder = new ViewHolder();
            viewHolder.spText = (TextView) view.findViewById(R.id.tv_chequebook_page);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.spText.setText(sQuestions.get(i).getQUESTION());

        return view;
    }

    private class ViewHolder {
        private TextView spText;
    }
}
