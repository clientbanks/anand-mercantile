package com.amcblanand.android.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.fragment.PreviewFundTransferFragment;
import com.amcblanand.android.model.PayeeListPojo;

/**
 * Created by rakshit.sathwara on 8/1/2017.
 */

public class PayeeListAdapter extends RecyclerView.Adapter<PayeeListAdapter.VHPayeeList> {

    private Context context;
    private List<PayeeListPojo> payeeListPojoList;
    private String payeeValue = "";

    public PayeeListAdapter(Context context, List<PayeeListPojo> payeeListPojoList, String payeeValue) {
        this.context = context;
        this.payeeListPojoList = payeeListPojoList;
        this.payeeValue = payeeValue;
    }

    @Override
    public VHPayeeList onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_payee_list_row_view, parent, false);
        VHPayeeList vhViewStatementList = new VHPayeeList(view);
        return vhViewStatementList;
    }


    @Override
    public void onBindViewHolder(VHPayeeList holder, final int position) {
        holder.tvBeneName.setText(payeeListPojoList.get(position).getBene_Name());
//        holder.tvBaneCode.setText(payeeListPojoList.get(position).getBene_Code());
        holder.tvBeneAcNo.setText(payeeListPojoList.get(position).getBene_Ac_No());

        holder.linearPayee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new PreviewFundTransferFragment();
                Bundle bundle = new Bundle();
                bundle.putString("FROM_ACC_CODE", Constants.ACCOUNT_NUMBER);
                bundle.putString("ACC_NUMBER", payeeListPojoList.get(position).getBene_Ac_No());
                bundle.putString("BANK_NAME", payeeListPojoList.get(position).getBene_Bank());
                bundle.putString("BRANCH_NAME", payeeListPojoList.get(position).getBene_Brach());
                bundle.putString("BENEFICIARY_CODE", payeeListPojoList.get(position).getBene_Code());
                bundle.putString("BENEFICIARY_NAME", payeeListPojoList.get(position).getBene_Name());
                bundle.putString("IFSC_CODE", payeeListPojoList.get(position).getIFSCCODE());
                bundle.putString("BENEFICIARY_ID", payeeListPojoList.get(position).getBeneficiary_ID());
                bundle.putString("TYPE", payeeValue);
                fragment.setArguments(bundle);
                FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_container, fragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
    }


    @Override
    public int getItemCount() {
        return payeeListPojoList.size();
    }

    public class VHPayeeList extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_bene_name)
        TextView tvBeneName;
        //        @BindView(R.id.tv_bane_code)
//        TextView tvBaneCode;
        @BindView(R.id.tv_bene_ac_no)
        TextView tvBeneAcNo;
        //        @BindView(R.id.card_payee_list)
//        CardView cardPayeeList;
        @BindView(R.id.linear_payee)
        LinearLayout linearPayee;

        public VHPayeeList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public void updateListData(List<PayeeListPojo> list) {
        payeeListPojoList = list;
        notifyDataSetChanged();
    }
}
