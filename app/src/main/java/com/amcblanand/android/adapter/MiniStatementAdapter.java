package com.amcblanand.android.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.model.MiniStatement;

/**
 * Created by rakshit.sathwara on 8/21/2017.
 */

public class MiniStatementAdapter extends RecyclerView.Adapter<MiniStatementAdapter.VHMiniStatement> {

    private Context context;
    private List<MiniStatement> miniStatementList;

    public MiniStatementAdapter(Context context, List<MiniStatement> miniStatementList) {
        this.context = context;
        this.miniStatementList = miniStatementList;
    }

    @Override
    public VHMiniStatement onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_mini_statement_row_view, parent, false);
        VHMiniStatement vhMiniStatement = new VHMiniStatement(view);
        return vhMiniStatement;
    }


    @Override
    public void onBindViewHolder(VHMiniStatement holder, int position) {
        holder.tvMiniStatementDate.setText(miniStatementList.get(position).getTranDateTime());
        holder.tvMiniStatementAmount.setText("\u20B9 " + miniStatementList.get(position).getTranAmount());
        holder.tvMiniStatementBeneName.setText(miniStatementList.get(position).getBeneName());
        holder.tvMiniStatementTransRefNo.setText("TransRefNo :- " + miniStatementList.get(position).getTransRefNo());
        holder.tvMiniStatementStatus.setText("Status :- " + miniStatementList.get(position).getStatus());
    }


    @Override
    public int getItemCount() {
        return miniStatementList.size();
    }

    public class VHMiniStatement extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_mini_statement_date)
        TextView tvMiniStatementDate;
        @BindView(R.id.tv_mini_statement_amount)
        TextView tvMiniStatementAmount;
        @BindView(R.id.tv_mini_statement_bene_name)
        TextView tvMiniStatementBeneName;
        @BindView(R.id.tv_mini_statement_transRefNo)
        TextView tvMiniStatementTransRefNo;
        @BindView(R.id.tv_mini_statement_status)
        TextView tvMiniStatementStatus;

        public VHMiniStatement(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
