package com.amcblanand.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import com.amcblanand.android.R;
import com.amcblanand.android.model.FeedbackPojo;

/**
 * Created by rakshit.sathwara on 8/31/2017.
 */

public class FeedbackCategoryAdapter extends BaseAdapter {

    private Context context;
    private List<FeedbackPojo> feedbackPojoList;
    private LayoutInflater mInflater;

    public FeedbackCategoryAdapter(Context context, List<FeedbackPojo> feedbackPojoList) {
        this.context = context;
        this.feedbackPojoList = feedbackPojoList;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return feedbackPojoList.size();
    }

    @Override
    public Object getItem(int i) {
        return feedbackPojoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;

        if (view == null) {
            view = mInflater.inflate(R.layout.single_chequebook_request_row_view, null);
            viewHolder = new ViewHolder();
            viewHolder.spText = (TextView) view.findViewById(R.id.tv_chequebook_page);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.spText.setText(feedbackPojoList.get(i).getSUBJECT());

        return view;
    }

    private class ViewHolder {
        private TextView spText;
    }

}
