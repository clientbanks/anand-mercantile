package com.amcblanand.android.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.callbackmodel.AccountListCallback;
import com.amcblanand.android.model.AccountListModel;

/**
 * Created by rakshit.sathwara on 2/7/2017.
 */

public class AccountListAdapter extends RecyclerView.Adapter<AccountListAdapter.VHAccountList> {

    private Context context;
    private List<AccountListModel> accountListModelList;

    public AccountListAdapter(final Context context, final List<AccountListModel> accountListModelList) {
        this.context = context;
        this.accountListModelList = accountListModelList;
    }


    @Override
    public VHAccountList onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_account_list_row_view, parent, false);
        VHAccountList vhRefBranch = new VHAccountList(view);
        return vhRefBranch;
    }


    @Override
    public void onBindViewHolder(VHAccountList holder, final int position) {
        holder.tvAccount.setText(accountListModelList.get(position).getAcno() + " - " + accountListModelList.get(position).getName() + " - " + accountListModelList.get(position).getActyp());
        holder.tvAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new AccountListCallback(accountListModelList.get(position).getBankId(),
                        accountListModelList.get(position).getBranchCode(), accountListModelList.get(position).getActyp(),
                        accountListModelList.get(position).getAccCode(), accountListModelList.get(position).getCurBal(),
                        accountListModelList.get(position).getName(), accountListModelList.get(position).getGender(),
                        accountListModelList.get(position).getAcno(), accountListModelList.get(position).getEmail()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return accountListModelList.size();
    }

    public class VHAccountList extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_account)
        TextView tvAccount;

        public VHAccountList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateListData(List<AccountListModel> list) {
        accountListModelList = list;
        notifyDataSetChanged();
    }


}
