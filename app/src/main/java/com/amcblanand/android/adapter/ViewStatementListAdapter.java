package com.amcblanand.android.adapter;

import android.content.Context;

import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.amcblanand.android.R;
import com.amcblanand.android.model.ViewStatementPojo;

/**
 * Created by rakshit.sathwara on 7/31/2017.
 */

public class ViewStatementListAdapter extends RecyclerView.Adapter<ViewStatementListAdapter.VHViewStatementList> {

    private static final int ITEM_TYPE_ODD = 1;
    private static final int ITEM_TYPE_EVEN = 0;
    @BindView(R.id.tv_statement_date)
    TextView tvStatementDate;
    @BindView(R.id.tv_statement_amount)
    TextView tvStatementAmount;
    @BindView(R.id.tv_statement_desc)
    TextView tvStatementDesc;
    private Context context;
    private List<ViewStatementPojo> viewStatementPojoList;

    public ViewStatementListAdapter(Context context, List<ViewStatementPojo> viewStatementPojoList) {
        this.context = context;
        this.viewStatementPojoList = viewStatementPojoList;
    }

    @Override
    public VHViewStatementList onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_view_statement_row_view, parent, false);
        VHViewStatementList vhViewStatementList = new VHViewStatementList(view);
        return vhViewStatementList;
    }


    @Override
    public void onBindViewHolder(VHViewStatementList holder, int position) {

        int itemType = getItemViewType(position);

        String[] test = viewStatementPojoList.get(position).getAmount().split(" ");
        String crDr = test[1];

        if (crDr.equals("Dr.")) {
            holder.tvStatementAmount.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        } else {
            holder.tvStatementAmount.setTextColor(ContextCompat.getColor(context, R.color.cr_color));

        }

        holder.tvStatementDate.setText(viewStatementPojoList.get(position).getTrandate());
        holder.tvStatementAmount.setText("\u20B9 " + viewStatementPojoList.get(position).getAmount().trim());
        holder.tvStatementDesc.setText(viewStatementPojoList.get(position).getDescrip());
    }


    @Override
    public int getItemCount() {
        return viewStatementPojoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if ((position % 2) == 0) {
            return ITEM_TYPE_EVEN;
        } else {
            return ITEM_TYPE_ODD;
        }
    }


    public class VHViewStatementList extends RecyclerView.ViewHolder {

        @BindView(R.id.ll_statement)
        CardView llStatement;
        @BindView(R.id.tv_statement_date)
        TextView tvStatementDate;
        @BindView(R.id.tv_statement_amount)
        TextView tvStatementAmount;
        @BindView(R.id.tv_statement_desc)
        TextView tvStatementDesc;

        public VHViewStatementList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
