package com.amcblanand.android.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;



import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.model.ChequebookStatusPojo;

/**
 * Created by amit.singh on 12/01/2017.
 */

public class ChequebookStatusAdapter extends RecyclerView.Adapter<ChequebookStatusAdapter.VHChequebookStatus> {

    private Context context;
    private List<ChequebookStatusPojo> chequebookStatusPojoList;

    public ChequebookStatusAdapter(Context context, List<ChequebookStatusPojo> chequebookStatusPojoList) {
        this.context = context;
        this.chequebookStatusPojoList = chequebookStatusPojoList;
    }

    @Override
    public VHChequebookStatus onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_chequebook_status_row_view, parent, false);
        VHChequebookStatus vhChequebookStatus = new VHChequebookStatus(view);
        return vhChequebookStatus;
    }


    @Override
    public void onBindViewHolder(VHChequebookStatus holder, int position) {

        if (chequebookStatusPojoList.size() > 0) {
            holder.linearChequeBook.setVisibility(View.VISIBLE);
            holder.tvEmptyStatus.setVisibility(View.GONE);

            holder.tvChequebookDateTime.setText(chequebookStatusPojoList.get(position).getREQ_DATE() + " " +
                    chequebookStatusPojoList.get(position).getREQ_TIME());
            holder.tvChequebookStatus.setText(chequebookStatusPojoList.get(position).getSTATUS());
            holder.tvAccNumber.setText(chequebookStatusPojoList.get(position).getECS_ACC_CODE());
            holder.tvPageNumber.setText("Pages :- " + chequebookStatusPojoList.get(position).getCHQ_BK_LEAF());

        } else {
            holder.linearChequeBook.setVisibility(View.GONE);
            holder.tvEmptyStatus.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public int getItemCount() {
        return chequebookStatusPojoList.size();
    }

    public class VHChequebookStatus extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_chequebook_date_time)
        TextView tvChequebookDateTime;
        @BindView(R.id.tv_chequebook_status)
        TextView tvChequebookStatus;
        @BindView(R.id.tv_acc_number)
        TextView tvAccNumber;
        @BindView(R.id.tv_page_number)
        TextView tvPageNumber;
        @BindView(R.id.linear_cheque_book)
        LinearLayout linearChequeBook;
        @BindView(R.id.tv_empty_status)
        TextView tvEmptyStatus;

        public VHChequebookStatus(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
