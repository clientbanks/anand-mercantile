package com.amcblanand.android.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.PayeeListPojo;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 8/29/2017.
 */

public class DeletePayeeAdapter extends RecyclerView.Adapter<DeletePayeeAdapter.VHPayeeList> {

    private static final String TAG = Constants.TAG + DeletePayeeAdapter.class.getSimpleName();

    private Context context;
    private List<PayeeListPojo> payeeListPojoList;
    private String payeeValue = "";

    private Progress progress;
    private DecryptData decryptData;

    public DeletePayeeAdapter(Context context, List<PayeeListPojo> payeeListPojoList, String payeeValue) {
        this.context = context;
        this.payeeListPojoList = payeeListPojoList;
        this.payeeValue = payeeValue;

        progress = new Progress(context);
        decryptData = new DecryptData();
    }

    @Override
    public VHPayeeList onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_payee_list_row_view, parent, false);
        VHPayeeList vhViewStatementList = new VHPayeeList(view);
        return vhViewStatementList;
    }


    @Override
    public void onBindViewHolder(VHPayeeList holder, final int position) {
        holder.tvBeneName.setText(payeeListPojoList.get(position).getBene_Name());
        holder.tvBeneAcNo.setText(payeeListPojoList.get(position).getBene_Ac_No());

        holder.linearPayee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Do you want to delete payee?");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                        progress.createDialog(false);
                        progress.dialogMessage("Please wait. . ");
                        progress.showDialog();
                        RemovePayee(payeeListPojoList.get(position).getBeneficiary_ID());
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    private void RemovePayee(String beneficiary_id) {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "RemovePayee");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBeneId", beneficiary_id);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgSystemIp", "");
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(context, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(context) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();
                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    Alert.ShowAlert(context, jsonObject.getString("ResponseMessage"));
                                    notifyDataSetChanged();
//                                    EventBus.getDefault().post(new DeletePayeeCallback("Y"));
                                } else {
                                    Alert.ShowAlert(context, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }


    @Override
    public int getItemCount() {
        return payeeListPojoList.size();
    }

    public class VHPayeeList extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_bene_name)
        TextView tvBeneName;

        @BindView(R.id.tv_bene_ac_no)
        TextView tvBeneAcNo;

        @BindView(R.id.linear_payee)
        LinearLayout linearPayee;

        public VHPayeeList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
