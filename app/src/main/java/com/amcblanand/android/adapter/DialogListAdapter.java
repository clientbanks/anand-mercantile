package com.amcblanand.android.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.callbackmodel.DialogCallback;
import com.amcblanand.android.model.BankListPojo;

/**
 * Created by rakshit.sathwara on 2/7/2017.
 */

public class DialogListAdapter extends RecyclerView.Adapter<DialogListAdapter.VHRefBranch> {

    private Context context;
    private List<BankListPojo> bankListPojoList;
    private String type = "";

    public DialogListAdapter(final Context context, final List<BankListPojo> bankListPojoList, String type) {
        this.context = context;
        this.bankListPojoList = bankListPojoList;
        this.type = type;
    }


    @Override
    public VHRefBranch onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_dialog_list_row_view, parent, false);
        VHRefBranch vhRefBranch = new VHRefBranch(view);
        return vhRefBranch;
    }


    @Override
    public void onBindViewHolder(VHRefBranch holder, final int position) {
        holder.tvDialogText.setText(bankListPojoList.get(position).getText());

        holder.tvDialogText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Adapter", "onClick: " + bankListPojoList.get(position).getValue());
                EventBus.getDefault().post(new DialogCallback(bankListPojoList.get(position).getText(),
                        bankListPojoList.get(position).getValue(), "c", type));
            }
        });
    }

    @Override
    public int getItemCount() {
        return bankListPojoList.size();
    }

    public class VHRefBranch extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_dialog_text)
        TextView tvDialogText;

        public VHRefBranch(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateListData(List<BankListPojo> list) {
        bankListPojoList = list;
        notifyDataSetChanged();
    }

}
