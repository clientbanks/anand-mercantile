package com.amcblanand.android.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.model.ContactusPojo;

/**
 * Created by rakshit.sathwara on 9/1/2017.
 */

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.VHContactList> {

    private Context context;
    private List<ContactusPojo> contactusPojoList;

    public ContactListAdapter(Context context, List<ContactusPojo> contactusPojoList) {
        this.context = context;
        this.contactusPojoList = contactusPojoList;
    }

    @Override
    public VHContactList onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_contact_us_row_view, parent, false);
        return new VHContactList(view);
    }


    @Override
    public void onBindViewHolder(VHContactList holder, int position) {
        holder.tbBankName.setText(contactusPojoList.get(position).getBRANCH_NAME());
        holder.tbBankName.setTextColor(context.getResources().getColor(R.color.primaryTextColor));
        holder.tbBankAddress.setText(
                contactusPojoList.get(position).getBR_ADD1() + "," + contactusPojoList.get(position).getBR_ADD2() + "," +
                        contactusPojoList.get(position).getBR_CITY() + "," + contactusPojoList.get(position).getBR_PIN());

        if (contactusPojoList.get(position).getBR_TEL_NO().equals("null")) {
            holder.tbBankNo.setText("Contact Details Not Found");
        } else {
            holder.tbBankNo.setText("Contact NO. " + contactusPojoList.get(position).getBR_TEL_NO());
        }

        holder.tbBranchIfsc.setText("IFSC :- " + contactusPojoList.get(position).getIFSC_CODE());
        holder.tbBankWeb.setText("WEB URL :- " + contactusPojoList.get(position).getWEB_URL());
        holder.tbBankEmail.setText("EMAIL ID:- " + contactusPojoList.get(position).getEMAIL_ID());
        holder.tbBankFax.setText("FAX :- " + contactusPojoList.get(position).getFAX());
        holder.tbBankBalEnq.setText("BALANCE INQUIRY No.:- " + contactusPojoList.get(position).getBAL_ENQ_NO());
        holder.tbBankAtmBlock.setText("ATM BLOCK No. :- " + contactusPojoList.get(position).getATM_BLOCK_NO());
    }


    @Override
    public int getItemCount() {
        return contactusPojoList.size();
    }

    public class VHContactList extends RecyclerView.ViewHolder {

        @BindView(R.id.tb_bank_name)
        TextView tbBankName;
        @BindView(R.id.tb_bank_address)
        TextView tbBankAddress;
        @BindView(R.id.tb_bank_no)
        TextView tbBankNo;
        @BindView(R.id.tb_branch_ifsc)
        TextView tbBranchIfsc;
        @BindView(R.id.tb_bank_fax)
        TextView tbBankFax;
        @BindView(R.id.tb_bank_email)
        TextView tbBankEmail;
        @BindView(R.id.tb_bank_bal_enq)
        TextView tbBankBalEnq;
        @BindView(R.id.tb_bank_atm_block)
        TextView tbBankAtmBlock;
        @BindView(R.id.tb_bank_web)
        TextView tbBankWeb;

        public VHContactList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
