package com.amcblanand.android.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;



import java.util.List;
import java.util.Map;

import com.amcblanand.android.R;
import com.amcblanand.android.activity.SecurityQuestionActivity;
import com.amcblanand.android.model.SQuestions;

public class SecQueAdapter extends RecyclerView.Adapter<SecQueAdapter.MyViewHolder> {

    private Map<String, List<SQuestions>> responseJSONBeans;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private Spinner spinner;
        private EditText editText;
        private TextView queNumber;

        public MyViewHolder(View view) {
            super(view);
            spinner = view.findViewById(R.id.spinner_row_s_question);
            editText = view.findViewById(R.id.et_row_s_question);
            queNumber = view.findViewById(R.id.tv_que_number);
        }
    }


    public SecQueAdapter(Context context, Map<String, List<SQuestions>> responseJSONBeans) {
        this.context = context;
        this.responseJSONBeans = responseJSONBeans;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_squestions, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        List<SQuestions> sQuestions = responseJSONBeans.get("" + holder.getAdapterPosition());

        int quePos = position + 1;

        SQuestionAdapter sQuestionAdapter = new SQuestionAdapter(context, sQuestions);
        holder.spinner.setAdapter(sQuestionAdapter);

        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        holder.queNumber.setText(String.valueOf(quePos));

        holder.editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                SecurityQuestionActivity.stringStringMap.put("" + holder.getAdapterPosition(), "" + sQuestions.get(holder.spinner.getSelectedItemPosition()).getSRNO() + "~"
                        + s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public int getItemCount() {
        Log.e("Amit", "getItemCount: " + responseJSONBeans.size());
        return responseJSONBeans.size();

    }
}