package com.amcblanand.android.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.model.ChequeTransactionPojo;

/**
 * Created by rakshit.sathwara on 7/31/2017.
 */

public class ChequeTransactiontAdapter extends RecyclerView.Adapter<ChequeTransactiontAdapter.VHChequeTransaction> {

    private Context context;
    private List<ChequeTransactionPojo> viewStatementPojoList;

    public ChequeTransactiontAdapter(Context context, List<ChequeTransactionPojo> viewStatementPojoList) {
        this.context = context;
        this.viewStatementPojoList = viewStatementPojoList;
    }

    @Override
    public VHChequeTransaction onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_view_statement_row_view, parent, false);
        VHChequeTransaction vhViewStatementList = new VHChequeTransaction(view);
        return vhViewStatementList;
    }


    @Override
    public void onBindViewHolder(VHChequeTransaction holder, int position) {
        holder.tvStatementDate.setText(viewStatementPojoList.get(position).getTrandate());
        holder.tvStatementAmount.setText("\u20B9 " + viewStatementPojoList.get(position).getAmount());
        holder.tvStatementDesc.setText(viewStatementPojoList.get(position).getDescrip());
    }


    @Override
    public int getItemCount() {
        return viewStatementPojoList.size();
    }

    public class VHChequeTransaction extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_statement_date)
        TextView tvStatementDate;
        @BindView(R.id.tv_statement_amount)
        TextView tvStatementAmount;
        @BindView(R.id.tv_statement_desc)
        TextView tvStatementDesc;

        public VHChequeTransaction(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
