package com.amcblanand.android.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amcblanand.android.R;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.fragment.ServiceViewFragment;
import com.amcblanand.android.model.OtherServiceModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OtherServicesAdapter extends RecyclerView.Adapter<OtherServicesAdapter.VHServices> {

    private Context context;

    @NonNull
    private List<OtherServiceModel> otherServiceModelList;

    public OtherServicesAdapter(Context context, @NonNull List<OtherServiceModel> otherServiceModelList) {
        this.context = context;
        this.otherServiceModelList = otherServiceModelList;
    }

    @Override
    public VHServices onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VHServices(LayoutInflater.from(context).inflate(R.layout.single_other_services, parent, false));
    }

    @Override
    public void onBindViewHolder(VHServices holder, int position) {
        holder.tvServiceName.setText(otherServiceModelList.get(position).getServiceName());
        holder.cardService.setCardBackgroundColor(ContextCompat.getColor(context, otherServiceModelList.get(position).getServiceColor()));
        holder.ivService.setBackgroundResource(otherServiceModelList.get(position).getServiceImg());

        holder.cardService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    Bundle bundle = new Bundle();
                    bundle.putString("URL", "http://amcblanand.com/deposit_rates.php");
                    Constants.TransmitFragment(new ServiceViewFragment(), ((AppCompatActivity) context).getSupportFragmentManager(), R.id.frame_container, true, bundle);

                } else if (position == 1) {
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("URL", "http://amcblanand.com/loan_rates.php");
                    Constants.TransmitFragment(new ServiceViewFragment(), ((AppCompatActivity) context).getSupportFragmentManager(), R.id.frame_container, true, bundle1);

                } else if (position == 2) {
                    Bundle bundle = new Bundle();
                    bundle.putString("URL", "http://amcblanand.com/emicalculator.php");
                    Constants.TransmitFragment(new ServiceViewFragment(), ((AppCompatActivity) context).getSupportFragmentManager(), R.id.frame_container, true, bundle);

                } else if (position == 3) {
                    Bundle bundle = new Bundle();
                    bundle.putString("URL", "http://amcblanand.com/fdcalc.php");
                    Constants.TransmitFragment(new ServiceViewFragment(), ((AppCompatActivity) context).getSupportFragmentManager(), R.id.frame_container, true, bundle);

                } else {
                    Bundle bundle5 = new Bundle();
                    bundle5.putString("URL", "http://amcblanand.com/contactus.php");
                    Constants.TransmitFragment(new ServiceViewFragment(), ((AppCompatActivity) context).getSupportFragmentManager(), R.id.frame_container, true, bundle5);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return otherServiceModelList.size();
    }

    public class VHServices extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_service)
        ImageView ivService;
        @BindView(R.id.tv_service_name)
        TextView tvServiceName;
        @BindView(R.id.card_service)
        CardView cardService;

        public VHServices(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
