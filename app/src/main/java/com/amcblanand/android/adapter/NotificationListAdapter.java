package com.amcblanand.android.adapter;

import android.content.Context;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;



import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.model.NotificationPojo;

/**
 * Created by amit.singh on 16-09-2017.
 */


public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.VHNotificationList> {

    private static final int ITEM_TYPE_ODD = 1;
    private static final int ITEM_TYPE_EVEN = 0;


    private Context context;
    private List<NotificationPojo> viewTransactionPojoList;

    public NotificationListAdapter(Context context, List<NotificationPojo> viewTransactionPojoList) {
        this.context = context;
        this.viewTransactionPojoList = viewTransactionPojoList;
    }

    @Override
    public VHNotificationList onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_view_transaction_row_view, parent, false);
        VHNotificationList vhViewTransactionList = new VHNotificationList(view);
        return vhViewTransactionList;
    }


    @Override
    public void onBindViewHolder(VHNotificationList holder, int position) {

        int itemType = getItemViewType(position);

        if (itemType == ITEM_TYPE_ODD) {
            holder.llTransaction.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
        } else {
            holder.llTransaction.setBackgroundColor(Color.WHITE);
        }


        holder.tvTransactionDate.setText(viewTransactionPojoList.get(position).getTdate() + " " + viewTransactionPojoList.get(position).getTime());
        holder.tvTransactionAmount.setText("\u20B9 " + viewTransactionPojoList.get(position).getAmount().trim());
        holder.tvTransactionDesc.setText(viewTransactionPojoList.get(position).getTrantype());
//        holder.tvTransactiontoAcc.setText(viewTransactionPojoList.get(position).getBeneficiary());

    }


    @Override
    public int getItemCount() {
        return viewTransactionPojoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if ((position % 2) == 0) {
            return ITEM_TYPE_EVEN;
        } else {
            return ITEM_TYPE_ODD;
        }
    }


    public class VHNotificationList extends RecyclerView.ViewHolder {

        @BindView(R.id.ll_transaction)
        LinearLayout llTransaction;
        @BindView(R.id.tv_transaction_date)
        TextView tvTransactionDate;
        @BindView(R.id.tv_transaction_amount)
        TextView tvTransactionAmount;
        @BindView(R.id.tv_transaction_desc)
        TextView tvTransactionDesc;


        public VHNotificationList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}







