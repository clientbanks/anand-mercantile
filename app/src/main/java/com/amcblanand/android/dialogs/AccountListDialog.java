package com.amcblanand.android.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.adapter.AccountListAdapter;
import com.amcblanand.android.model.AccountListModel;

/**
 * Created by rakshit.sathwara on 7/25/2017.
 */

public class AccountListDialog extends CustomDialog {


    @BindView(R.id.et_search_data)
    EditText etSearchData;
    @BindView(R.id.rv_account)
    RecyclerView rvAccount;
    private Context context;
    private List<AccountListModel> accountListModelList;
    private LinearLayoutManager linearLayoutManager;
    AccountListAdapter refBranchAdapter;

    public AccountListDialog(final Context context, List<AccountListModel> accountListModelList) {
        super(context);
        this.context = context;
        this.accountListModelList = accountListModelList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_account_list);
        ButterKnife.bind(this);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);

        rvAccount.hasFixedSize();
        linearLayoutManager = new LinearLayoutManager(context);
        rvAccount.setLayoutManager(linearLayoutManager);

        etSearchData.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        refBranchAdapter = new AccountListAdapter(context, accountListModelList);
        rvAccount.setAdapter(refBranchAdapter);


    }

    void filter(String text) {
        List<AccountListModel> temp = new ArrayList();
        for (AccountListModel d : accountListModelList) {
            if (d.getName().toLowerCase().contains(text) || d.getAccCode().toLowerCase().contains(text)) {
                temp.add(d);
            }
        }
        refBranchAdapter.updateListData(temp);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }

    @Override
    public void onClick(View v) {

    }
}
