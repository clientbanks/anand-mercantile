package com.amcblanand.android.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.adapter.DialogListAdapter;
import com.amcblanand.android.model.BankListPojo;

/**
 * Created by rakshit.sathwara on 8/1/2017.
 */

public class BankListDialog extends CustomDialog {

    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.rv_dialog_list)
    RecyclerView rvDialogList;
    private Context context;
    private List<BankListPojo> bankListPojoList;
    private DialogListAdapter dialogListAdapter;
    private String type = "";

    public BankListDialog(Context context, List<BankListPojo> bankListPojoList, String type) {
        super(context);
        this.context = context;
        this.bankListPojoList = bankListPojoList;
        this.type = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_list);
        ButterKnife.bind(this);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);

        rvDialogList.hasFixedSize();
        rvDialogList.setLayoutManager(new LinearLayoutManager(context));
        dialogListAdapter = new DialogListAdapter(context, bankListPojoList, type);
        rvDialogList.setAdapter(dialogListAdapter);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    void filter(String text) {
        List<BankListPojo> temp = new ArrayList();
        for (BankListPojo d : bankListPojoList) {
            if (d.getText().toLowerCase().contains(text)) {
                temp.add(d);
            }
        }
        dialogListAdapter.updateListData(temp);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

    }

    @Override
    public void onClick(View view) {

    }
}
