package com.amcblanand.android.dialogs

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.google.android.material.textfield.TextInputLayout
import androidx.fragment.app.DialogFragment
import androidx.appcompat.widget.Toolbar
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.amcblanand.android.R
import com.amcblanand.android.activity.SplashActivity
import com.amcblanand.android.api.API
import com.amcblanand.android.api.RetrofitCallbacks
import com.amcblanand.android.extra.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import timber.log.Timber


class FullScreenPasswordChange : DialogFragment() {

    private val TAG = FullScreenPasswordChange::class.java.simpleName

    private lateinit var etOldPin: EditText
    private lateinit var etNewPin: EditText
    private lateinit var etConfirmNewPin: EditText

    private lateinit var txtOldPin: TextInputLayout
    private lateinit var txtNewPin: TextInputLayout
    private lateinit var txtConfirmPin: TextInputLayout

    private lateinit var btnSubmit: Button
    private lateinit var progress: Progress
    private lateinit var decryptData: DecryptData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(width, height)
        }
        dialog.window.setGravity(Gravity.CENTER)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_full_screen, container, false)
        val toolbar = view.findViewById<Toolbar>(R.id.dialog_toolbar)
        toolbar.title = "Change PIN"

        etOldPin = view.findViewById(R.id.et_old_pin)
        etNewPin = view.findViewById(R.id.et_new_pin)
        etConfirmNewPin = view.findViewById(R.id.et_confirm_new_pin)

        txtOldPin = view.findViewById(R.id.txt_old_pin)
        txtNewPin = view.findViewById(R.id.txt_new_pin)
        txtConfirmPin = view.findViewById(R.id.txt_confirm_pin)

        btnSubmit = view.findViewById(R.id.btn_change_pin)

        progress = Progress(activity!!)
        decryptData = DecryptData()

        etOldPin.transformationMethod = AsteriskPasswordTransformationMethod()
        etConfirmNewPin.transformationMethod = AsteriskPasswordTransformationMethod()
        etNewPin.transformationMethod = AsteriskPasswordTransformationMethod()

        btnSubmit.setOnClickListener {
            if (etOldPin.text.toString().trim().length < 6) {
                txtOldPin.error = "6 digit required"
            } else if (etNewPin.text.toString().trim().length < 6) {
                txtNewPin.error = "6 digit required"
            } else if (etConfirmNewPin.text.toString().trim().length < 6) {
                txtConfirmPin.error = "6 digit required"
            } else if (!etConfirmNewPin.text.toString().trim().equals(etNewPin.text.toString().trim())) {
                Toast.makeText(activity, "PIN not match", Toast.LENGTH_SHORT).show()
            } else {
                progress.createDialog(false)
                progress.dialogMessage("Please wait..")
                progress.showDialog()
                newPINUpdate()
            }
        }

        etOldPin.setOnKeyListener { _, i_, _ ->
            if (!etOldPin.text.toString().trim().isNotEmpty()) {
                txtOldPin.error = null
            }
            false
        }

        etNewPin.setOnKeyListener { _, i_, _ ->
            if (!etNewPin.text.toString().trim().isNotEmpty()) {
                txtNewPin.error = null
            }
            false
        }

        etConfirmNewPin.setOnKeyListener { _, i_, _ ->
            if (!etConfirmNewPin.text.toString().trim().isNotEmpty()) {
                txtConfirmPin.error = null
            }
            false
        }

        return view
    }

    private fun newPINUpdate() {
        try {
            val reqJsonObject = JSONObject()
            reqJsonObject.put("function", "NewPINUpdate")
            val reqParamJsonObject = JSONObject()
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID)
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER)
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID)
            reqParamJsonObject.put("ArgOldPIN", etOldPin.text.toString().trim { it <= ' ' })
            reqParamJsonObject.put("ArgNewPIN", etNewPin.text.toString().trim { it <= ' ' })
            reqParamJsonObject.put("ArgFCMKey", getFcm())
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID)
            reqJsonObject.putOpt("parameter", reqParamJsonObject)
            val dataValue = reqJsonObject.toString()
            Timber.e(dataValue, TAG)

            API.getInstance().encryptRequest(activity, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    object : RetrofitCallbacks<ResponseBody>(activity) {
                        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                            super.onResponse(call, response)
                            try {
                                val responseString = response.body()!!.string()
                                Timber.e(responseString, TAG)
                                val responseDataValue = decryptData.parseResponse(responseString)
                                Timber.e(responseDataValue, TAG)

                                progress.hideDialog()

                                val jsonObject = JSONObject(responseDataValue)
                                if (jsonObject.getString("Status") == "1") {
                                    etConfirmNewPin.setText("")
                                    etOldPin.setText("")
                                    etNewPin.setText("")
                                    Alert.ShowAlert(activity, jsonObject.getString("ResponseMessage"))
                                    val handler = Handler()
                                    handler.postDelayed({
                                        activity!!.finish()
                                        val i = Intent(activity, SplashActivity::class.java)
                                        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        startActivity(i)
                                        val pid = android.os.Process.myPid()
                                        android.os.Process.killProcess(pid)
                                        System.exit(0)
                                    }, 1000)

                                } else {
                                    Alert.ShowAlert(activity, jsonObject.getString("ResponseMessage"))
                                }

                            } catch (ex: Exception) {
                                progress.hideDialog()
                                Toast.makeText(activity, "Something wrong!", Toast.LENGTH_SHORT).show()
                            }
                        }

                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                            super.onFailure(call, t)
                            progress.hideDialog()
                            Toast.makeText(activity, "Server Error", Toast.LENGTH_SHORT).show()
                        }
                    })

        } catch (ex: Exception) {
            progress.hideDialog()
            ex.printStackTrace()
        }
    }

    private fun getFcm(): String {
        val pref = activity!!.getSharedPreferences(Constants.SHARED_PREF, 0)
        return pref.getString("regId", "test")
    }

}