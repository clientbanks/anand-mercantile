package com.amcblanand.android.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;


import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.fragment.AddBeneficiaryFragment;
import com.amcblanand.android.fragment.DeletePayeeListFragment;

/**
 * Created by rakshit.sathwara on 8/28/2017.
 */

public class AddPayeeDialog extends CustomDialog {
    @BindView(R.id.dialog_linear_yba)
    LinearLayout dialogLinearYba;
    @BindView(R.id.dialog_linear_oba)
    LinearLayout dialogLinearOba;
    private Context context;
    private String type;

    public AddPayeeDialog(Context context, String type) {
        super(context);
        this.context = context;
        this.type = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_payee);
        ButterKnife.bind(this);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);

        dialogLinearYba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (type.equals("P")) {
                    Fragment fragment = new AddBeneficiaryFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("payee_type", "YBA");
                    fragment.setArguments(bundle);
                    FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_container, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                } else {
                    Fragment fragment = new DeletePayeeListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("payee_type", "YBA");
                    fragment.setArguments(bundle);
                    FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_container, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }

            }
        });

        dialogLinearOba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (type.equals("P")) {
                    Fragment fragment = new AddBeneficiaryFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("payee_type", "OBA");
                    fragment.setArguments(bundle);
                    FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_container, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                } else {
                    Fragment fragment = new DeletePayeeListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("payee_type", "OBA");
                    fragment.setArguments(bundle);
                    FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_container, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }

            }
        });
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

    }

    @Override
    public void onClick(View view) {
        dismiss();
    }
}
