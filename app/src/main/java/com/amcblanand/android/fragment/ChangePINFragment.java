package com.amcblanand.android.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.amcblanand.android.R;
import com.amcblanand.android.activity.SplashActivity;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.AsteriskPasswordTransformationMethod;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 8/18/2017.
 */

public class ChangePINFragment extends Fragment implements Validator.ValidationListener, View.OnClickListener {

    private static final String TAG = Constants.TAG + ChangePINFragment.class.getSimpleName();

    @NotEmpty
    @Length(min = 6, message = "6 digit required.")
    @BindView(R.id.et_old_pin)
    EditText etOldPin;

    @NotEmpty
    @Password
    @Length(min = 6, message = "6 digit required.")
    @BindView(R.id.et_new_pin)
    EditText etNewPin;

    @NotEmpty
    @ConfirmPassword
    @Length(min = 6, message = "6 digit required.")
    @BindView(R.id.et_confirm_new_pin)
    EditText etConfirmNewPin;

    @BindView(R.id.btn_change_pin)
    Button btnChangePin;
    Unbinder unbinder;

    private Validator validator;
    private Progress progress;
    private DecryptData decryptData;

    private Context context;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pin_password, container, false);
        unbinder = ButterKnife.bind(this, view);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Change PIN");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        validator = new Validator(this);
        validator.setValidationListener(this);

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        btnChangePin.setOnClickListener(this);

        etOldPin.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        etConfirmNewPin.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        etNewPin.setTransformationMethod(new AsteriskPasswordTransformationMethod());


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onValidationSucceeded() {
        progress.createDialog(false);
        progress.dialogMessage("Changing PIN. . ");
        progress.showDialog();
        NewPINUpdate();
    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((TextView) (EditText) view).setError(message);
            } else if (view instanceof Spinner) {
                ((TextView) ((Spinner) view).getSelectedView()).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_change_pin:
                validator.validate();
                break;
            default:
                break;
        }
    }

    private void NewPINUpdate() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "NewPINUpdate");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgOldPIN", etOldPin.getText().toString().trim());
            reqParamJsonObject.put("ArgNewPIN", etNewPin.getText().toString().trim());
            reqParamJsonObject.put("ArgFCMKey", getFcm());
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    etConfirmNewPin.setText("");
                                    etOldPin.setText("");
                                    etNewPin.setText("");
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            getActivity().finish();
                                            Intent i = new Intent(getActivity(), SplashActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(i);
                                            int pid = android.os.Process.myPid();
                                            android.os.Process.killProcess(pid);
                                            System.exit(0);
                                        }
                                    }, 1000);

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                                Toast.makeText(getActivity(), "Something wrong!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Toast.makeText(getActivity(), "Something wrong!Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
            Toast.makeText(getActivity(), "Something wrong!", Toast.LENGTH_SHORT).show();
        }
    }

    private String getFcm() {
        SharedPreferences pref = context.getSharedPreferences(Constants.SHARED_PREF, 0);
        return pref.getString("regId", "test");
    }
}