package com.amcblanand.android.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 1/11/2018.
 */

public class AddPayeeOtpVerificationFragment extends Fragment /*implements Validator.ValidationListener*/ {

    private static final String TAG = Constants.TAG + AddPayeeOtpVerificationFragment.class.getSimpleName();

    //    @NotEmpty
//    @Length(min = 7, message = "7 digit required.")
    @BindView(R.id.et_verify_otp)
    EditText etVerifyOtp;

    @BindView(R.id.btn_verify_otp)
    Button btnVerifyOtp;
    Unbinder unbinder;

    //    private Validator validator;
    private Progress progress;
    private DecryptData decryptData;

    private String ArgSource = "", ArgBeneCode = "", ArgBeneName = "", ArgPayeeType = "", ArgEcsacccode = "",
            ArgBeneIFSCCode = "", ArgBeneEmail = "", ArgBeneMobile = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_pinverification, container, false);
        unbinder = ButterKnife.bind(this, view);
        ArgSource = getArguments().getString("ArgSource");
        ArgBeneCode = getArguments().getString("ArgBeneCode");
        ArgBeneName = getArguments().getString("ArgBeneName");
        ArgPayeeType = getArguments().getString("ArgPayeeType");
        ArgEcsacccode = getArguments().getString("ArgEcsacccode");
        ArgBeneIFSCCode = getArguments().getString("ArgBeneIFSCCode");
        ArgBeneEmail = getArguments().getString("ArgBeneEmail");
        ArgBeneMobile = getArguments().getString("ArgBeneMobile");
//        validator = new Validator(this);
//        validator.setValidationListener(this);

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

//        btnVerifyOtp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                validator.validate();
//            }
//        });

        etVerifyOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 7 && !etVerifyOtp.getText().toString().trim().isEmpty()) {
                    progress.createDialog(false);
                    progress.dialogMessage("Please wait. . ");
                    progress.showDialog();
                    CheckOTPCodeOpt();
                } else {
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

   /* @Override
    public void onValidationSucceeded() {



    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((TextView) (EditText) view).setError(message);
            } else if (view instanceof Spinner) {
                ((TextView) ((Spinner) view).getSelectedView()).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }*/


    private void CheckOTPCodeOpt() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "CheckOTPCodeOpt");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgOtpCode", etVerifyOtp.getText().toString().trim());
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgSource", ArgSource);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.createDialog(false);
                                    progress.dialogMessage("Please wait. . ");
                                    progress.showDialog();
                                    AddPayee();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }


    private void AddPayee() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "AddPayee");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBeneCode", ArgBeneCode);
            reqParamJsonObject.put("ArgBeneName", ArgBeneName);
            reqParamJsonObject.put("ArgPayeeType", ArgPayeeType);
            reqParamJsonObject.put("ArgEcsacccode", ArgEcsacccode);
            reqParamJsonObject.put("ArgBeneIFSCCode", ArgBeneIFSCCode);
            reqParamJsonObject.put("ArgRemeEcsAccCode", Constants.ACCOUNT_NUMBER);
            reqParamJsonObject.put("ArgSystemIp", Constants.IP);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqParamJsonObject.put("ArgPayeeLimit", "0");
            reqParamJsonObject.put("ArgOTP", etVerifyOtp.getText().toString().trim());
            reqParamJsonObject.put("ArgBeneEmail", ArgBeneEmail);
            reqParamJsonObject.put("ArgBeneMobile", ArgBeneMobile);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {

                                    View view = getActivity().getCurrentFocus();
                                    if (view != null) {
                                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                    }

                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                    Fragment fragment = new HomeFragment();
                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.frame_container, fragment);
                                    fragmentTransaction.addToBackStack(null);
                                    fragmentTransaction.commit();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();

                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
