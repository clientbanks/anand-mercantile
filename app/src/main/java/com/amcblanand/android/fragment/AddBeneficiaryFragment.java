package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.callbackmodel.DialogCallback;
import com.amcblanand.android.dialogs.BankListDialog;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.BankListPojo;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;


/**
 * Created by rakshit.sathwara on 8/1/2017.
 */

public class AddBeneficiaryFragment extends Fragment implements Validator.ValidationListener {

    private static final String TAG = Constants.TAG + AddBeneficiaryFragment.class.getSimpleName();

    @BindView(R.id.et_acc_number)
    EditText etAccNumber;

    @NotEmpty
    @BindView(R.id.et_beneficiary_code)
    EditText etBeneficiaryCode;

    @NotEmpty
    @BindView(R.id.et_beneficiary_name)
    EditText etBeneficiaryName;

    @NotEmpty
    @BindView(R.id.et_to_acc_number)
    EditText etToAccNumber;


    @BindView(R.id.et_beneficiary_email_id)
    EditText etBeneficiaryEmailId;

    @BindView(R.id.et_beneficiary_mobile)
    EditText etBeneficiaryMobile;

    @NotEmpty
    @Length(min = 11, message = "Min 11 Length Required")
    @BindView(R.id.et_ifsc_code)
    EditText etIfscCode;
    @BindView(R.id.btn_add_beneficiary)
    Button btnAddBeneficiary;
    @BindView(R.id.et_select_bank)
    EditText etSelectBank;
    @BindView(R.id.et_select_state)
    EditText etSelectState;
    @BindView(R.id.et_select_branch)
    EditText etSelectBranch;

    Unbinder unbinder;
    @BindView(R.id.rb_ifsc)
    RadioButton rbIfsc;
    @BindView(R.id.rb_branch)
    RadioButton rbBranch;
    @BindView(R.id.rg_selection_type)
    RadioGroup rgSelectionType;
    @BindView(R.id.linear_branch_selection)
    LinearLayout linearBranchSelection;
    @BindView(R.id.linear_type_for_ifsc)
    LinearLayout linearTypeForIfsc;
    private Progress progress;
    private DecryptData decryptData;
    private List<BankListPojo> bankListPojoList;
    private BankListDialog bankListDialog;
    private String bankName = "", stateName = "", branchName = "";
    private String selectionType = "", payeeType = "";

    private Validator validator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_beneficiary, container, false);
        unbinder = ButterKnife.bind(this, view);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Add Beneficiary");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        payeeType = getArguments().getString("payee_type");
        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        validator = new Validator(this);
        validator.setValidationListener(this);

        etAccNumber.setText(Constants.ACCOUNT_NUMBER);

        if (rbIfsc.isChecked()) {
            etIfscCode.setVisibility(View.VISIBLE);
            selectionType = "ifsc";
        } else {
            etIfscCode.setVisibility(View.GONE);
        }

        if (payeeType.equals("YBA")) {
            linearTypeForIfsc.setVisibility(View.GONE);
        } else {
            linearTypeForIfsc.setVisibility(View.VISIBLE);
        }

        rgSelectionType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                if (i == R.id.rb_ifsc) {
                    etIfscCode.setVisibility(View.VISIBLE);
                    linearBranchSelection.setVisibility(View.GONE);
                    selectionType = "ifsc";
                } else if (i == R.id.rb_branch) {
                    linearBranchSelection.setVisibility(View.VISIBLE);
                    etIfscCode.setVisibility(View.GONE);
                    selectionType = "other";
                }
            }
        });

        etSelectBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSelectState.setVisibility(View.GONE);
                etSelectState.setText("");
                etSelectBranch.setVisibility(View.GONE);
                etSelectBranch.setText("");
                progress.createDialog(false);
                progress.dialogMessage("Please wait. . ");
                progress.showDialog();
                getBankList();
            }
        });

        etSelectState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSelectBranch.setVisibility(View.GONE);
                etSelectBranch.setText("");
                progress.createDialog(false);
                progress.dialogMessage("Please wait. . ");
                progress.showDialog();
                FillState();
            }
        });

        etSelectBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.createDialog(false);
                progress.dialogMessage("Please wait. . ");
                progress.showDialog();
                FillBranch();
            }
        });


        etBeneficiaryCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable et) {

                String s = et.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    etBeneficiaryCode.setText(s);
                }
                etBeneficiaryCode.setSelection(etBeneficiaryCode.getText().length());

            }
        });

        etBeneficiaryName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable et) {

                String s = et.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    etBeneficiaryName.setText(s);
                }
                etBeneficiaryName.setSelection(etBeneficiaryName.getText().length());

            }
        });

        etToAccNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable et) {

                String s = et.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    etToAccNumber.setText(s);
                }
                etToAccNumber.setSelection(etToAccNumber.getText().length());

            }
        });

        etIfscCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable et) {

                String s = et.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    etIfscCode.setText(s);
                }
                etIfscCode.setSelection(etIfscCode.getText().length());

            }
        });


        btnAddBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                validator.validate();

                if (etBeneficiaryEmailId.getText().toString().trim().length() > 0) {
                    if (!Patterns.EMAIL_ADDRESS.matcher(
                            etBeneficiaryEmailId.getText().toString()
                    ).matches()) {
                        Toast.makeText(getActivity(), "Invalid email-id", Toast.LENGTH_SHORT).show();
                    } else {
                        validator.validate();
                    }
                } else if (etBeneficiaryMobile.getText().toString().trim().length() > 0) {
                    if (etBeneficiaryMobile.getText().toString().trim().length() < 10) {
                        Toast.makeText(getActivity(), "10 digit required", Toast.LENGTH_SHORT).show();
                    } else {
                        validator.validate();
                    }
                } else {
                    validator.validate();
                }

            }
        });

        return view;
    }

    private void CheckIFSCCode() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "CheckIFSCCode");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIFSCCode", etIfscCode.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        bankName = jObj.getString("BANK_NAME");
                                        stateName = jObj.getString("STATE_BRANCH_NAME");
                                        branchName = jObj.getString("IFSCCODE");
                                    }
                                    progress.createDialog(false);
                                    progress.dialogMessage("Sending otp..");
                                    progress.showDialog();
                                    SendOTPCode();

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void getBankList() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "FillBank");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {

                                    bankListPojoList = new ArrayList<BankListPojo>();
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        BankListPojo bankListPojo = new BankListPojo();
                                        bankListPojo.setText(jObj.getString("Text"));
                                        bankListPojo.setValue(jObj.getString("Value"));
                                        bankListPojoList.add(bankListPojo);
                                    }

                                    bankListDialog = new BankListDialog(getActivity(), bankListPojoList, "b");
                                    bankListDialog.show();

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();

                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void FillState() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "FillState");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBankName", etSelectBank.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {

                                    bankListPojoList = new ArrayList<BankListPojo>();
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        BankListPojo bankListPojo = new BankListPojo();
                                        bankListPojo.setText(jObj.getString("Text"));
                                        bankListPojo.setValue(jObj.getString("Value"));
                                        bankListPojoList.add(bankListPojo);
                                    }

                                    bankListDialog = new BankListDialog(getActivity(), bankListPojoList, "s");
                                    bankListDialog.show();

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();

                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void FillBranch() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "FillBranch");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBankName", etSelectBank.getText().toString().trim());
            reqParamJsonObject.put("ArgState", etSelectState.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {

                                    bankListPojoList = new ArrayList<BankListPojo>();
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        BankListPojo bankListPojo = new BankListPojo();
                                        bankListPojo.setText(jObj.getString("Text"));
                                        bankListPojo.setValue(jObj.getString("Value"));
                                        bankListPojoList.add(bankListPojo);
                                    }

                                    bankListDialog = new BankListDialog(getActivity(), bankListPojoList, "br");
                                    bankListDialog.show();

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();

                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Subscribe
    public void onMessageEvent(DialogCallback dialogCallback) {
        Log.e("SHARE_CALL", "onMessageEvent: " + dialogCallback.getText() + dialogCallback.getValue() + dialogCallback.getDialogData() +
                dialogCallback.getType());
        if (dialogCallback.getDialogData().equals("c") && dialogCallback.getType().equals("b")) {
            bankListDialog.dismiss();
            etSelectBank.setText(dialogCallback.getText());
            bankName = dialogCallback.getValue();
            etSelectState.setVisibility(View.VISIBLE);
        } else if (dialogCallback.getDialogData().equals("c") && dialogCallback.getType().equals("s")) {
            bankListDialog.dismiss();
            etSelectState.setText(dialogCallback.getText());
            stateName = dialogCallback.getValue();
            etSelectBranch.setVisibility(View.VISIBLE);
        } else if (dialogCallback.getDialogData().equals("c") && dialogCallback.getType().equals("br")) {
            bankListDialog.dismiss();
            etSelectBranch.setText(dialogCallback.getText());
            branchName = dialogCallback.getValue();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onValidationSucceeded() {

        if (payeeType.equals("YBA")) {
            progress.createDialog(false);
            progress.dialogMessage("Please wait. . ");
            progress.showDialog();
            SendOTPCode();
        } else {
            progress.createDialog(false);
            progress.dialogMessage("Please wait. . ");
            progress.showDialog();

            if (selectionType.equals("ifsc")) {
                CheckIFSCCode();
            } else {
                SendOTPCode();
            }
        }
    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((TextView) (EditText) view).setError(message);
            } else if (view instanceof Spinner) {
                ((TextView) ((Spinner) view).getSelectedView()).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void SendOTPCode() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "SendOTPCode");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("Argacccode", Constants.ACC_CODE);
            reqParamJsonObject.put("Argbranchcode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("Argactyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgSource", "AP");
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("ArgSource", "AP");
                                    bundle.putString("ArgBeneCode", etBeneficiaryCode.getText().toString().trim());
                                    bundle.putString("ArgBeneName", etBeneficiaryName.getText().toString().trim());
                                    bundle.putString("ArgPayeeType", payeeType);
                                    bundle.putString("ArgEcsacccode", etToAccNumber.getText().toString().trim());
                                    bundle.putString("ArgBeneIFSCCode", branchName);
                                    bundle.putString("ArgBeneEmail", etBeneficiaryEmailId.getText().toString().trim());
                                    bundle.putString("ArgBeneMobile", etBeneficiaryMobile.getText().toString().trim());
                                    Constants.TransmitFragment(new AddPayeeOtpVerificationFragment(), getActivity().getSupportFragmentManager(),
                                            R.id.frame_container, true, bundle);
//                                    Intent intent = new Intent(getActivity(), PINVerificationActivity.class);
//                                    intent.putExtra("ArgSource", "AP");
//                                    startActivityForResult(intent, 200);
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (payeeType.equals("YBA")) {
//            progress.createDialog(false);
//            progress.dialogMessage("Please wait. . ");
//            progress.showDialog();
//            AddPayee();
//        } else {
//            if (selectionType.equals("ifsc")) {
//                progress.createDialog(false);
//                progress.dialogMessage("Please wait. . ");
//                progress.showDialog();
//                AddPayee();
//            } else if (selectionType.equals("other")) {
//                progress.createDialog(false);
//                progress.dialogMessage("Please wait. . ");
//                progress.showDialog();
//                AddPayee();
//            }
//        }
//    }
}
