package com.amcblanand.android.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.amcblanand.android.R;
import com.amcblanand.android.adapter.ViewStatementListAdapter;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.ViewStatementPojo;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 11/14/2017.
 */

public class ViewTransactionFragment extends Fragment {

    private static final String TAG = Constants.TAG + ViewTransactionFragment.class.getSimpleName();

    public static EditText etFromdate, etTodate;
    @BindView(R.id.tv_account_number)
    TextView tvAccountNumber;
    @BindView(R.id.linear_balance)
    LinearLayout linearBalance;
    @BindView(R.id.btn_tran_submit)
    Button btnTranSubmit;
    @BindView(R.id.rv_view_statement)
    RecyclerView rvViewStatement;
    Unbinder unbinder;

    private Progress progress;
    private DecryptData decryptData;
    public static int dataValue;

    //datepicker
    public static int from_date, from_month, from_year, to_date, to_month, to_year;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_transaction, container, false);
        ////// Calendar init
        unbinder = ButterKnife.bind(this, view);
        final Calendar calendar = Calendar.getInstance();
        int yyyy = calendar.get(Calendar.YEAR);
        int MM = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        from_date = dd;
        from_month = MM;
        to_month = MM;
        to_date = dd;
        from_year = yyyy;
        to_year = yyyy;

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("View Transaction");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);
        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        etFromdate = (EditText) view.findViewById(R.id.et_from_date);
        etTodate = (EditText) view.findViewById(R.id.et_to_date);

        progress.createDialog(false);
        progress.dialogMessage("Getting transaction details. .");
        progress.showDialog();
        GetLastTenTrans();

        Timber.e(Constants.ACCOUNT_NUMBER, TAG);
        tvAccountNumber.setText(Constants.ACCOUNT_NUMBER);

        etFromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new SelectFromDateFragment();
                newFragment.show(getActivity().getSupportFragmentManager(), "From Date");
            }
        });

        etTodate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new SelectToDateFragment();
                newFragment.show(getActivity().getSupportFragmentManager(), "To Date");
            }
        });

        btnTranSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
                    Date sdate = formatter.parse(from_date + "/" + from_month + "/" + from_year);
                    Calendar startCalendar = new GregorianCalendar();
                    startCalendar.setTime(sdate);
                    Date edate = formatter.parse(to_date + "/" + to_month + "/" + to_year);
                    Calendar endCalendar = new GregorianCalendar();
                    endCalendar.setTime(edate);
                    int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
                    int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date date1 = simpleDateFormat.parse(etFromdate.getText().toString().trim());
                    Date date2 = simpleDateFormat.parse(etTodate.getText().toString().trim());

                    if (etFromdate.getText().toString().length() == 0) {
                        Alert.ShowAlert(getActivity(), "Please Select From Date!");
                    } else if (etTodate.getText().toString().length() == 0) {
                        Alert.ShowAlert(getActivity(), "Please Select To Date!");
                    } else if (date1.compareTo(date2) > 0) {
                        Alert.ShowAlert(getActivity(), "To Date must be after From Date!");
                    } else {
                        progress.createDialog(false);
                        progress.dialogMessage("Getting transaction details. .");
                        progress.showDialog();
                        GetTransactions();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        return view;
    }

    private void GetLastTenTrans() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetLastTenTrans");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                List<ViewStatementPojo> viewStatementPojoList = new ArrayList<ViewStatementPojo>();

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        ViewStatementPojo viewStatementPojo = new ViewStatementPojo();
                                        viewStatementPojo.setAmount(jObj.getString("Amount"));
                                        viewStatementPojo.setDescrip(jObj.getString("Descrip"));
                                        viewStatementPojo.setTrandate(jObj.getString("Trandate"));
                                        viewStatementPojoList.add(viewStatementPojo);
                                    }

                                    rvViewStatement.setHasFixedSize(true);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                    rvViewStatement.setLayoutManager(mLayoutManager);
                                    rvViewStatement.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                                    rvViewStatement.setItemAnimator(new DefaultItemAnimator());
                                    ViewStatementListAdapter accountListAdapter = new ViewStatementListAdapter(getActivity(), viewStatementPojoList);
                                    rvViewStatement.setAdapter(accountListAdapter);


                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void GetTransactions() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetTransactions");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgFromdt", etFromdate.getText().toString().trim());
            reqParamJsonObject.put("ArgTodt", etTodate.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                List<ViewStatementPojo> viewStatementPojoList = new ArrayList<ViewStatementPojo>();

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        ViewStatementPojo viewStatementPojo = new ViewStatementPojo();
                                        viewStatementPojo.setAmount(jObj.getString("Amount").trim());
                                        viewStatementPojo.setDescrip(jObj.getString("Descrip").trim());
                                        viewStatementPojo.setTrandate(jObj.getString("Trandate").trim());
                                        viewStatementPojoList.add(viewStatementPojo);
                                    }
                                    rvViewStatement.hasFixedSize();
                                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                                    rvViewStatement.setLayoutManager(linearLayoutManager);
                                    ViewStatementListAdapter accountListAdapter = new ViewStatementListAdapter(getActivity(), viewStatementPojoList);
                                    rvViewStatement.setAdapter(accountListAdapter);
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @SuppressLint("ValidFragment")
    public static class SelectFromDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            DatePickerDialog dpd = new DatePickerDialog(getActivity(), this, from_year, from_month, from_date);
            dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
            return dpd;
        }

        public void onDateSet(DatePicker view, int yyyy, int MM, int dd) {
            populateSetDate(yyyy, MM, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            etFromdate.setText(formatDate(year, month, day));
            from_date = day;
            from_month = month;
            from_year = year;
        }

        private String formatDate(int year, int month, int day) {

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.set(year, month, day);
            Date date = cal.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            return sdf.format(date);
        }

    }

    @SuppressLint("ValidFragment")
    public static class SelectToDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            DatePickerDialog dpd = new DatePickerDialog(getActivity(), this, to_year, to_month, to_date);
            dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
            return dpd;
        }

        public void onDateSet(DatePicker view, int yy, int MM, int dd) {
            populateSetDate(yy, MM, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            etTodate.setText(formatDate(year, month, day));
            to_date = day;
            to_month = month;
            to_year = year;
        }

        private String formatDate(int year, int month, int day) {

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.set(year, month, day);
            Date date = cal.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            return sdf.format(date);
        }

    }
}
