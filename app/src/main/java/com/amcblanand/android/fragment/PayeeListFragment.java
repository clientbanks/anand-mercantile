package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.amcblanand.android.R;
import com.amcblanand.android.adapter.PayeeListAdapter;
import com.amcblanand.android.model.PayeeListPojo;

/**
 * Created by rakshit.sathwara on 11/15/2017.
 */

public class PayeeListFragment extends Fragment {

    @BindView(R.id.rv_payee_list)
    RecyclerView rvPayeeList;
    Unbinder unbinder;
    private String payeeType, payeeList;
    private List<PayeeListPojo> payeeListPojoList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payee_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Payee List");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        payeeType = getArguments().getString("PAYEE_TYPE");
        payeeList = getArguments().getString("PAYEE_LIST");
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<List<PayeeListPojo>>() {
            }.getType();
            payeeListPojoList = gson.fromJson(payeeList, type);

            rvPayeeList.hasFixedSize();
            rvPayeeList.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvPayeeList.setAdapter(new PayeeListAdapter(getActivity(), payeeListPojoList, payeeType));

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
