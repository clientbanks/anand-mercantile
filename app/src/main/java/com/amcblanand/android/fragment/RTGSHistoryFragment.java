package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.amcblanand.android.R;
import com.amcblanand.android.adapter.HistoryAdapter;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.HistoryModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class RTGSHistoryFragment extends Fragment {

    @BindView(R.id.rv_history)
    RecyclerView rvHistory;
    Unbinder unbinder;

    private Progress progress;
    private DecryptData decryptData;
    private List<HistoryModel> historyModelList;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            progress = new Progress(getActivity());
            progress.createDialog(false);
            progress.dialogMessage("Please wait..");
            progress.showDialog();
            GetLastTenTransHist();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.single_history_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        decryptData = new DecryptData();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void GetLastTenTransHist() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetLastTenTransHist");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgTranType", "N");
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, "Request");

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, "Response");

                                progress.hideDialog();
                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    historyModelList = new ArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        HistoryModel historyModel = new HistoryModel();
                                        historyModel.setTDT(jObj.getString("TDT"));
                                        historyModel.setTDATE(jObj.getString("TDATE"));
                                        historyModel.setPAYMENT_TYPE(jObj.getString("PAYMENT_TYPE"));
                                        historyModel.setBENE_DETAILS(jObj.getString("BENE_DETAILS"));
                                        historyModel.setAMOUNT(jObj.getString("AMOUNT"));
                                        historyModelList.add(historyModel);
                                    }

                                    rvHistory.setVisibility(View.VISIBLE);
                                    rvHistory.hasFixedSize();
                                    rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    rvHistory.setAdapter(new HistoryAdapter(getActivity(), historyModelList,"RTGS"));

                                } else {
                                    rvHistory.setVisibility(View.GONE);
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
