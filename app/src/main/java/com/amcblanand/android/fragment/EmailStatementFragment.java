package com.amcblanand.android.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 7/31/2017.
 */

public class EmailStatementFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = Constants.TAG + EmailStatementFragment.class.getSimpleName();

    @BindView(R.id.tv_acc_number)
    TextView tvAccNumber;

    public static EditText etFromdate, etTodate;

    @BindView(R.id.et_email_id)
    EditText etEmailId;
    @BindView(R.id.btn_submit_email_statement)
    Button btnSubmitEmailStatement;

    @BindView(R.id.rg_send_download)
    RadioGroup rgSendDownload;
    @BindView(R.id.rb_send_email)
    RadioButton rgSendEmail;
    @BindView(R.id.rb_email)
    RadioButton rgEmail;

    private String rgValue = "E";

    Unbinder unbinder;

    private Progress progress;
    private DecryptData decryptData;
    private FragmentManager fragmentManager;

    public static int dataValue;

    //datepicker
    public static int from_date, from_month, from_year, to_date, to_month, to_year;

    private SeekBar seekBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_email_statement, container, false);
        unbinder = ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Email Statement");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        final Calendar calendar = Calendar.getInstance();
        int yyyy = calendar.get(Calendar.YEAR);
        int MM = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        from_date = dd;
        from_month = MM;
        to_month = MM;
        to_date = dd;
        from_year = yyyy;
        to_year = yyyy;

        progress = new Progress(getActivity());
        decryptData = new DecryptData();


        etFromdate = (EditText) view.findViewById(R.id.et_from_date_email_statement);
        etTodate = (EditText) view.findViewById(R.id.et_to_date_email_statement);


        etFromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new SelectFromDateFragment();
                newFragment.show(getActivity().getSupportFragmentManager(), "From Date");
            }
        });


        etTodate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new SelectToDateFragment();
                newFragment.show(getActivity().getSupportFragmentManager(), "To Date");
            }
        });

        progress.createDialog(false);
        progress.dialogMessage("Please wait..");
        progress.showDialog();
        GetEmailID();


        fragmentManager = getActivity().getFragmentManager();

        tvAccNumber.setText(Constants.ACCOUNT_NUMBER);
        btnSubmitEmailStatement.setOnClickListener(this);

        rgSendDownload.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                if (rgSendEmail.isChecked()) {

                    rgValue = "E";

                } else if (rgEmail.isChecked()) {

                    rgValue = "D";

                } else {
                    Toast.makeText(getContext(), "Please select one option", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return view;
    }

    private void GetEmailID() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetEmailID");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);
                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    String EMAIL = " ";
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        EMAIL = jObj.getString("EMAIL");
                                    }
                                    etEmailId.setText(EMAIL);

                                } else {

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void GetEmailStatement() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetEmailStatement");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgStartDT", etFromdate.getText().toString().trim());
            reqParamJsonObject.put("ArgEndDT", etTodate.getText().toString().trim());
            reqParamJsonObject.put("ArgEmailID", etEmailId.getText().toString().trim());
            reqParamJsonObject.put("ArgSend_Download", rgValue);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);
                                String baseData = "";
                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {


                                    if (rgValue.equals("E")) {

                                        Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));

                                    } else if (rgValue.equals("D")) {


                                        JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jOng = jsonArray.getJSONObject(i);
                                            baseData = jOng.getString("PDF");
                                        }

                                        Timber.e(baseData, TAG);
                                        Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));

                                        Handler handler = new Handler();
                                        String finalBaseData = baseData;
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    String fpath = Environment.getExternalStorageDirectory() + "/" + "EmailStatement" + ".pdf";
//                                    String fpath = "/sdcard/" + "EmailStatement" + ".pdf";
                                                    File file = new File(fpath);
                                                    if (!file.exists()) {
                                                        file.createNewFile();
                                                    }

                                                    byte[] data = Base64.decode(finalBaseData, Base64.DEFAULT);

                                                    FileOutputStream fOut = new FileOutputStream(file);
                                                    fOut.write(data);
                                                    fOut.close();

//                                    Alert.ShowAlert(getActivity(), "Your file Location " + fpath + "\n\n" + jsonObject.getString("ResponseMessage"));

                                                    openPdf(fpath);
                                                } catch (Exception ex) {
                                                    ex.printStackTrace();
                                                }

                                            }
                                        }, 3000);
                                    }

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit_email_statement:
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
                    Date sdate = formatter.parse(from_date + "/" + from_month + "/" + from_year);
                    Calendar startCalendar = new GregorianCalendar();
                    startCalendar.setTime(sdate);
                    Date edate = formatter.parse(to_date + "/" + to_month + "/" + to_year);
                    Calendar endCalendar = new GregorianCalendar();
                    endCalendar.setTime(edate);
                    int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
                    int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date date1 = simpleDateFormat.parse(etFromdate.getText().toString().trim());
                    Date date2 = simpleDateFormat.parse(etTodate.getText().toString().trim());

                    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                    if (etFromdate.getText().toString().length() == 0) {
                        Alert.ShowAlert(getActivity(), "Please Select From Date!");
                    } else if (etTodate.getText().toString().length() == 0) {
                        Alert.ShowAlert(getActivity(), "Please Select To Date!");
                    } else if (date1.compareTo(date2) > 0) {
                        Alert.ShowAlert(getActivity(), "To Date must be after From Date!");
                    } else if (rgValue.equals("E") && /*(!etEmailId.getText().toString().trim().equals("")) && */!etEmailId.getText().toString().trim().matches(emailPattern)) {
                        Toast.makeText(getActivity(), "Please enter email-ID.", Toast.LENGTH_SHORT).show();
                    } else {
                        progress.createDialog(false);
                        progress.dialogMessage("Please wait. . ");
                        progress.showDialog();
                        GetEmailStatement();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Toast.makeText(getActivity(), "Select Date.", Toast.LENGTH_SHORT).show();
                }

                break;

            default:
                break;
        }
    }

    void openPdf(String path) {
        File file = new File(path);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file), "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getActivity(), "No app to read PDF File", Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("ValidFragment")
    public static class SelectFromDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            DatePickerDialog dpd = new DatePickerDialog(getActivity(), this, from_year, from_month, from_date);
            dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
            return dpd;
        }

        public void onDateSet(DatePicker view, int yyyy, int MM, int dd) {
            populateSetDate(yyyy, MM, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            etFromdate.setText(formatDate(year, month, day));
            from_date = day;
            from_month = month;
            from_year = year;
        }

        private String formatDate(int year, int month, int day) {

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.set(year, month, day);
            Date date = cal.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            return sdf.format(date);
        }

    }

    @SuppressLint("ValidFragment")
    public static class SelectToDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            DatePickerDialog dpd = new DatePickerDialog(getActivity(), this, to_year, to_month, to_date);
            dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
            return dpd;
        }

        public void onDateSet(DatePicker view, int yy, int MM, int dd) {
            populateSetDate(yy, MM, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            etTodate.setText(formatDate(year, month, day));
            to_date = day;
            to_month = month;
            to_year = year;
        }

        private String formatDate(int year, int month, int day) {

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.set(year, month, day);
            Date date = cal.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            return sdf.format(date);
        }

    }
}
