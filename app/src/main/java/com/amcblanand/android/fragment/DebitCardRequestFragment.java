package com.amcblanand.android.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.amcblanand.android.R;
import com.amcblanand.android.adapter.DebitCardActiveDeactivateAdapter;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.callbackmodel.AccountListCallback;
import com.amcblanand.android.callbackmodel.DebitCardCallBack;
import com.amcblanand.android.dialogs.AccountListDialog;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.AccountListModel;
import com.amcblanand.android.model.DebitCardPojo;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by amit.singh on 12/27/2017.
 */

public class DebitCardRequestFragment extends Fragment {

	private static final String TAG = Constants.TAG + DebitCardRequestFragment.class.getSimpleName();

	@BindView(R.id.et_account_list)
	EditText etAccountList;
	Unbinder unbinder;
	@BindView(R.id.rv_debit_card)
	RecyclerView rvDebitCard;

	private Progress progress;
	private DecryptData decryptData;

	private Context context;

	private List<AccountListModel> accountListModelList;
	private AccountListDialog accountListDialog;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		this.context = context;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_debitcard_request, container, false);
		unbinder = ButterKnife.bind(this, view);

		((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Debit Card");
		((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

		progress = new Progress(getActivity());
		decryptData = new DecryptData();

		progress.createDialog(false);
		progress.dialogMessage("Getting account list . . ");
		progress.showDialog();
		getAccountList();

		etAccountList.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				accountListDialog = new AccountListDialog(getActivity(), accountListModelList);
				accountListDialog.show();
			}
		});

		return view;
	}

	private void getAccountList() {
		try {
			JSONObject reqJsonObject = new JSONObject();
			reqJsonObject.put("function", "GetAccountList");
			JSONObject reqParamJsonObject = new JSONObject();
			reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
			reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
			reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
			reqJsonObject.putOpt("parameter", reqParamJsonObject);
			String dataValue = reqJsonObject.toString();
			Timber.e(dataValue, TAG);

			API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
					Constants.CONNECTION_ID, Constants.AUTH_KEY,
					Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
					new RetrofitCallbacks<ResponseBody>(getActivity()) {
						@Override
						public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
							super.onResponse(call, response);
							try {
								String responseString = response.body().string();
								Timber.e(responseString, TAG);
								String responseDataValue = decryptData.parseResponse(responseString);
								Timber.e(responseDataValue, TAG);

								accountListModelList = new ArrayList<AccountListModel>();

								progress.hideDialog();

								JSONObject jsonObject = new JSONObject(responseDataValue);
								if (jsonObject.getString("Status").equals("1")) {
									JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");

									for (int i = 0; i < jsonArray.length(); i++) {
										AccountListModel accountListPojo = new AccountListModel();
										JSONObject jObj = jsonArray.getJSONObject(i);
										accountListPojo.setBankId(jObj.getString("BankId"));
										accountListPojo.setAccCode(jObj.getString("AccCode"));
										accountListPojo.setAcno(jObj.getString("Acno"));
										accountListPojo.setActyp(jObj.getString("Actyp"));
										accountListPojo.setBranchCode(jObj.getString("BranchCode"));
										accountListPojo.setCurBal(jObj.getString("CurBal"));
										accountListPojo.setEmail(jObj.getString("Email"));
										accountListPojo.setGender(jObj.getString("Gender"));
										accountListPojo.setName(jObj.getString("Name"));
										accountListModelList.add(accountListPojo);
									}
								} else {
									Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
								}


							} catch (Exception e) {
								e.printStackTrace();
								progress.hideDialog();
							}
						}

						@Override
						public void onFailure(Call<ResponseBody> call, Throwable t) {
							super.onFailure(call, t);
							progress.hideDialog();
						}
					});

		} catch (Exception e) {
			e.printStackTrace();
			progress.hideDialog();
		}
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMessageEvent(AccountListCallback event) {
		accountListDialog.dismiss();
		etAccountList.setText(event.getAcno());

		Constants.ACCOUNT_BANK_ID = event.getBankId();
		Constants.ACTYP = event.getActyp();
		Constants.ACC_CODE = event.getAccCode();
		Constants.ACCOUNT_CUR_BAL = event.getCurBal();
		Constants.BRANCH_CODE = event.getBranchCode();
		Constants.ACCOUNT_NUMBER = event.getAcno();

		progress.createDialog(false);
		progress.dialogMessage("Please wait..");
		progress.showDialog();
		GetATMCardDetails(event.getAcno());

	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onDebitCardCall(DebitCardCallBack debitCardCallBack) {
		if (debitCardCallBack.getStatus().equalsIgnoreCase("Y")) {
			progress.createDialog(false);
			progress.dialogMessage("Please wait..");
			progress.showDialog();
			GetATMCardDetails(Constants.ACCOUNT_NUMBER);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		EventBus.getDefault().unregister(this);
	}


	private void GetATMCardDetails(String acno) {
		try {
			JSONObject reqJsonObject = new JSONObject();
			reqJsonObject.put("function", "GetATMCardDetails");

			JSONObject reqParamJsonObject = new JSONObject();
			reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
			reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
			reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
			reqParamJsonObject.put("ArgEcsAccCode", acno);

			reqJsonObject.putOpt("parameter", reqParamJsonObject);

			String dataValue = reqJsonObject.toString();
			Timber.d(TAG + " - " + dataValue);

			API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
					Constants.CONNECTION_ID, Constants.AUTH_KEY,
					Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
					new RetrofitCallbacks<ResponseBody>(getActivity()) {
						@Override
						public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
							super.onResponse(call, response);

							try {
								String responseString = response.body().string();
								Timber.e(responseString, TAG);
								String responseDataValue = decryptData.parseResponse(responseString);
								Timber.e(responseDataValue, TAG);

								progress.hideDialog();

								List<DebitCardPojo> debitCardPojoList = new ArrayList<DebitCardPojo>();

								JSONObject jsonObject = new JSONObject(responseDataValue);
								if (jsonObject.getString("Status").equals("1")) {
									JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
									for (int i = 0; i < jsonArray.length(); i++) {
										JSONObject jObj = jsonArray.getJSONObject(i);
										DebitCardPojo debitCardPojo = new DebitCardPojo();
										debitCardPojo.setATM_YN(jObj.getString("ATM_YN"));
										debitCardPojo.setATM_CARD_SRNO(jObj.getString("ATM_CARD_SRNO"));
										debitCardPojo.setATM_ISSUE_DT(jObj.getString("ATM_ISSUE_DT"));
										debitCardPojo.setATM_EXPIRE_DT(jObj.getString("ATM_EXPIRE_DT"));
										debitCardPojoList.add(debitCardPojo);
									}

									rvDebitCard.setVisibility(View.VISIBLE);
									rvDebitCard.hasFixedSize();
									rvDebitCard.setLayoutManager(new LinearLayoutManager(getActivity()));
									rvDebitCard.setAdapter(new DebitCardActiveDeactivateAdapter(getActivity(), debitCardPojoList));

								} else {
									Alert.showAlertWithFinish(getActivity(), jsonObject.getString("ResponseMessage"));
									rvDebitCard.setVisibility(View.GONE);
								}

							} catch (Exception e) {
								e.printStackTrace();
								progress.hideDialog();
							}
						}

						@Override
						public void onFailure(Call<ResponseBody> call, Throwable t) {
							super.onFailure(call, t);
							progress.hideDialog();
							Toast.makeText(getActivity(), "Something wrong! Please try after sometimes.", Toast.LENGTH_SHORT).show();
						}
					});

		} catch (Exception e) {
			e.printStackTrace();
			progress.hideDialog();
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		unbinder.unbind();
	}
}
