package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.mobsandgeeks.saripaar.Validator;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class EditPayeeFragment extends Fragment {


    @BindView(R.id.et_beneficiary_code)
    EditText etBeneficiaryCode;

    @BindView(R.id.et_beneficiary_name)
    EditText etBeneficiaryName;

    @BindView(R.id.et_to_acc_number)
    EditText etToAccNumber;

    @BindView(R.id.et_beneficiary_email_id)
    EditText etBeneficiaryEmailId;

    @BindView(R.id.et_beneficiary_mobile)
    EditText etBeneficiaryMobile;

    @BindView(R.id.btn_add_beneficiary)
    Button btnAddBeneficiary;

    private Validator validator;
    private Progress progress;
    private DecryptData decryptData;
    private String beneId, beneCode, beneName, beneEmailID, beneMobileNumber, accountNumber;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_beneficiary, container, false);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Edit Beneficiary");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

       /* view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener( new View.OnKeyListener()
        {
            @Override
            public boolean onKey( View v, int keyCode, KeyEvent event )
            {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    Fragment fragment = new HomeFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.commit();
                    return true;
                }
                return false;
            }
        } );*/

        beneId = getArguments().getString("bene_id");
        beneCode = getArguments().getString("bene_code");
        beneName = getArguments().getString("bene_name");
        beneEmailID = getArguments().getString("bene_email_id");
        beneMobileNumber = getArguments().getString("bene_mobile_number");
        accountNumber = getArguments().getString("account_number");

        etBeneficiaryCode.setText(beneCode);
        etBeneficiaryName.setText(beneName);
        etBeneficiaryEmailId.setText(beneEmailID);
        etBeneficiaryMobile.setText(beneMobileNumber);
        etToAccNumber.setText(accountNumber);

        btnAddBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etBeneficiaryEmailId.getText().toString().trim().length() > 0) {
                    if (!Patterns.EMAIL_ADDRESS.matcher(
                            etBeneficiaryEmailId.getText().toString()
                    ).matches()) {
                        Toast.makeText(getActivity(), "Invalid email-id", Toast.LENGTH_SHORT).show();
                    } else {
                        progress.createDialog(false);
                        progress.dialogMessage("Please wait. . ");
                        progress.showDialog();
                        updateBeneficiary();
                    }
                } else if (etBeneficiaryMobile.getText().toString().trim().length() > 1) {
                    if (etBeneficiaryMobile.getText().toString().trim().length() < 10) {
                        Toast.makeText(getActivity(), "10 digit required", Toast.LENGTH_SHORT).show();
                    } else {
                        progress.createDialog(false);
                        progress.dialogMessage("Please wait. . ");
                        progress.showDialog();
                        updateBeneficiary();
                    }
                } else {
                    progress.createDialog(false);
                    progress.dialogMessage("Please wait. . ");
                    progress.showDialog();
                    updateBeneficiary();
                }

            }
        });

        return view;
    }


    private void updateBeneficiary() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("function", "ModifyPayeeDetail");

            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqJsonObject.put("ArgClientId", Constants.CUST_ID);
            reqJsonObject.put("ArgBeneId", beneId);
            reqJsonObject.put("ArgBeneEmail", etBeneficiaryEmailId.getText().toString().trim());
            reqJsonObject.put("ArgBeneMobile", etBeneficiaryMobile.getText().toString().trim());

            jsonObject.putOpt("parameter", reqJsonObject);

            Timber.e(jsonObject.toString(), "REQUEST");

            API.getInstance().encryptRequest(getActivity(), jsonObject.toString(), Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {

                                String responseString = response.body().string();
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, "Response");

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                    Fragment fragment = new HomeFragment();
                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.frame_container, fragment);
                                    fragmentTransaction.commit();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });


        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
