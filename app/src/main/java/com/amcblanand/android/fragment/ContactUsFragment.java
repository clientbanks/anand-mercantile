package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.amcblanand.android.R;
import com.amcblanand.android.adapter.ContactListAdapter;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.ContactusPojo;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 9/1/2017.
 */

public class ContactUsFragment extends Fragment {
    private static final String TAG = Constants.TAG + ContactUsFragment.class.getSimpleName();
    @BindView(R.id.rv_contact_list)
    RecyclerView rvContactList;
    Unbinder unbinder;

    private Progress progress;
    private DecryptData decryptData;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        unbinder = ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Contact US");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        progress.createDialog(false);
        progress.dialogMessage("Please wait..");
        progress.showDialog();
        BindContactUSDetails();


        return view;
    }

    private void BindContactUSDetails() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "BindContactUSDetails");

            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);

            String dataValue = reqJsonObject.toString();
            Timber.d(TAG + " - " + dataValue);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);

                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                List<ContactusPojo> contactusPojoList = new ArrayList<ContactusPojo>();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        ContactusPojo contactusPojo = new ContactusPojo();
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        contactusPojo.setBANK_NAME(jObj.getString("BANK_NAME"));
                                        contactusPojo.setBRANCH_NAME(jObj.getString("BRANCH_NAME"));
                                        contactusPojo.setBR_ADD1(jObj.getString("BR_ADD1"));
                                        contactusPojo.setBR_ADD2(jObj.getString("BR_ADD2"));
                                        contactusPojo.setBR_ADD3(jObj.getString("BR_ADD3"));
                                        contactusPojo.setBR_CITY(jObj.getString("BR_CITY"));
                                        contactusPojo.setBR_PIN(jObj.getString("BR_PIN"));
                                        contactusPojo.setBR_TEL_NO(jObj.getString("BR_TEL_NO"));
                                        contactusPojo.setIFSC_CODE(jObj.getString("IFSC_CODE"));
                                        contactusPojo.setFAX(jObj.getString("FAX"));
                                        contactusPojo.setEMAIL_ID(jObj.getString("EMAIL_ID"));
                                        contactusPojo.setWEB_URL(jObj.getString("WEB_URL"));
                                        contactusPojo.setBAL_ENQ_NO(jObj.getString("BAL_ENQ_NO"));
                                        contactusPojo.setATM_BLOCK_NO(jObj.getString("ATM_BLOCK_NO"));
                                        contactusPojoList.add(contactusPojo);
                                    }

                                    rvContactList.hasFixedSize();
                                    rvContactList.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    rvContactList.setAdapter(new ContactListAdapter(getActivity(), contactusPojoList));

                                } else {
                                    Alert.showAlertWithFinish(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Toast.makeText(getActivity(), "Something wrong! Please try after sometimes.", Toast.LENGTH_SHORT).show();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
