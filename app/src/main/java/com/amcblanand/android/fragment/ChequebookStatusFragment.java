package com.amcblanand.android.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.amcblanand.android.R;
import com.amcblanand.android.adapter.ChequebookStatusAdapter;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.ChequebookStatusPojo;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by amit.singh on 12/27/2017.
 */

public class ChequebookStatusFragment extends Fragment {

    private static final String APPTAG = Constants.TAG + ChequebookStatusFragment.class.getSimpleName();
    @BindView(R.id.rv_chequebook_status)
    RecyclerView rvChequebookStatus;
    Unbinder unbinder;
    private List<ChequebookStatusPojo> chequebookStatusPojoList;
    private Progress progress;
    private DecryptData decryptData;

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chequebook_status, container, false);
        unbinder = ButterKnife.bind(this, view);

        ((AppCompatActivity) context).getSupportActionBar().setTitle("Chequebook Status");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        progress = new Progress(context);
        decryptData = new DecryptData();

        progress.createDialog(false);
        progress.dialogMessage("Please wait..");
        progress.showDialog();
        ChequeBookStatus();
        return view;
    }

    private void ChequeBookStatus() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "ChequeBookStatus");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, APPTAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(context) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, APPTAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, APPTAG);
                                progress.hideDialog();
                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                chequebookStatusPojoList = new ArrayList<ChequebookStatusPojo>();

                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        ChequebookStatusPojo chequebookStatusPojo = new ChequebookStatusPojo();
                                        chequebookStatusPojo.setUSER_ID(jObj.getString("USER_ID"));
                                        chequebookStatusPojo.setECS_ACC_CODE(jObj.getString("ECS_ACC_CODE"));
                                        chequebookStatusPojo.setREQ_DATE(jObj.getString("REQ_DATE"));
                                        chequebookStatusPojo.setREQ_TIME(jObj.getString("REQ_TIME"));
                                        chequebookStatusPojo.setCHQ_BK_LEAF(jObj.getString("CHQ_BK_LEAF"));
                                        chequebookStatusPojo.setSTATUS(jObj.getString("STATUS"));
                                        chequebookStatusPojoList.add(chequebookStatusPojo);
                                    }

                                    rvChequebookStatus.hasFixedSize();
                                    rvChequebookStatus.setLayoutManager(new LinearLayoutManager(context));
                                    rvChequebookStatus.setAdapter(new ChequebookStatusAdapter(context, chequebookStatusPojoList));

                                } else {
                                    Alert.ShowAlert(context, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Alert.ShowAlert(context, "Something wrong!");
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
            Alert.ShowAlert(context, "Something wrong!");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
