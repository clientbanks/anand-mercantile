package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.base.BaseFragment;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 8/2/2017.
 */

public class PreviewFundTransferFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = Constants.TAG + PreviewFundTransferFragment.class.getSimpleName();
    @BindView(R.id.et_nick_name)
    EditText etNickName;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_to_acc)
    EditText etToAcc;
    @BindView(R.id.et_from)
    EditText etFrom;
    @BindView(R.id.rb_neft)
    RadioButton rbNeft;
    @BindView(R.id.rb_rtgs)
    RadioButton rbRtgs;
    @BindView(R.id.rg_type_neft)
    RadioGroup rgTypeNeft;
    @BindView(R.id.et_bank_ifsc_code)
    EditText etBankIfscCode;
    @BindView(R.id.et_amount)
    EditText etAmount;
    @BindView(R.id.et_remarks)
    EditText etRemarks;
    @BindView(R.id.btn_preview_submit)
    Button btnPreviewSubmit;
    @BindView(R.id.linear_internal)
    LinearLayout linearInternal;
    @BindView(R.id.linear_neft)
    LinearLayout linearNeft;
    @BindView(R.id.linear_rtgs)
    LinearLayout linearRtgs;
    @BindView(R.id.linear_imps)
    LinearLayout linearImps;
    @BindView(R.id.linear_xyz)
    LinearLayout linearXyz;
    Unbinder unbinder;
    @BindView(R.id.tv_check_nr_time)
    TextView tvCheckNrTime;
    @BindView(R.id.et_charges_amount)
    EditText etChargesAmount;
    @BindView(R.id.txt_charges_amount)
    TextInputLayout txtChargesAmount;

    private Progress progress;
    private DecryptData decryptData;

    private String FROM_ACC_CODE = "", ACC_NUMBER = "", BANK_NAME = "", BRANCH_NAME = "", BENEFICIARY_CODE = "",
            BENEFICIARY_NAME = "", IFSC_CODE = "", BENEFICIARY_ID = "", TYPE = "", chargeAmount = "", PAY_TYPE = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preview_fund_transfer, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Preview Fund Transfer");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);
        unbinder = ButterKnife.bind(this, view);

        FROM_ACC_CODE = getArguments().getString("FROM_ACC_CODE");
        ACC_NUMBER = getArguments().getString("ACC_NUMBER");
        BANK_NAME = getArguments().getString("BANK_NAME");
        BRANCH_NAME = getArguments().getString("BRANCH_NAME");
        BENEFICIARY_CODE = getArguments().getString("BENEFICIARY_CODE");
        BENEFICIARY_NAME = getArguments().getString("BENEFICIARY_NAME");
        IFSC_CODE = getArguments().getString("IFSC_CODE");
        BENEFICIARY_ID = getArguments().getString("BENEFICIARY_ID");
        TYPE = getArguments().getString("TYPE");

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        etNickName.setText(BENEFICIARY_CODE);
        etName.setText(BENEFICIARY_NAME);
        etToAcc.setText(ACC_NUMBER);
        etFrom.setText(FROM_ACC_CODE);
        etBankIfscCode.setText(IFSC_CODE);

        if (TYPE.equals("YBA")) {
            linearInternal.setVisibility(View.VISIBLE);
            linearNeft.setVisibility(View.GONE);
            linearImps.setVisibility(View.GONE);
            linearRtgs.setVisibility(View.GONE);
            tvCheckNrTime.setVisibility(View.GONE);
        } else if (TYPE.equals("OBA")) {
            linearNeft.setVisibility(View.VISIBLE);
            linearRtgs.setVisibility(View.VISIBLE);
            linearImps.setVisibility(View.VISIBLE);
            linearInternal.setVisibility(View.GONE);
            linearImps.setVisibility(View.GONE);

            if (Constants.CHECK_NR_TIME.equals("Y")) {
                tvCheckNrTime.setVisibility(View.VISIBLE);

                tvCheckNrTime.setText("NEFT START TIME :- " + Constants.NEFT_START_TIME + " , NEFT END TIME :- " + Constants.NEFT_END_TIME + "\n" +
                        "RTGS START TIME :- " + Constants.RTGS_START_TIME + " , RTGS END TIME :- " + Constants.RTGS_END_TIME);
            }
        } else {
            linearInternal.setVisibility(View.GONE);
            linearNeft.setVisibility(View.GONE);
            linearImps.setVisibility(View.VISIBLE);
            linearRtgs.setVisibility(View.GONE);
            rgTypeNeft.setVisibility(View.GONE);
            tvCheckNrTime.setVisibility(View.GONE);
        }

        linearInternal.setOnClickListener(this);
        linearNeft.setOnClickListener(this);
        linearRtgs.setOnClickListener(this);
        linearImps.setOnClickListener(this);

//        etAmount.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                progress.createDialog(false);
//                progress.dialogMessage("Please wait..");
//                progress.showDialog();
//                CalcOnlineTransactionCharge();
//            }
//        });

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linear_internal:
                if (etAmount.getText().toString().trim().equals("") || etAmount.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please enter amount", Toast.LENGTH_SHORT).show();
                } else {
                    hideKeyboard();
                    Constants.TRANSACTION_TYPE = "INTERNAL";
                    progress.createDialog(false);
                    progress.dialogMessage("Please wait. . ");
                    progress.showDialog();
                    CheckBalance();
                }
                break;

            case R.id.linear_neft:
                if (etAmount.getText().toString().trim().equals("") || etAmount.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please enter amount", Toast.LENGTH_SHORT).show();
                } else {
                    hideKeyboard();
                    PAY_TYPE = "NEFT";
                    Constants.TRANSACTION_TYPE = "NEFT";
                    progress.createDialog(false);
                    progress.dialogMessage("Please wait. . ");
                    progress.showDialog();
                    CheckBalance();
                }
                break;

            case R.id.linear_rtgs:
                if (etAmount.getText().toString().trim().equals("") || etAmount.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please enter amount", Toast.LENGTH_SHORT).show();
                } else {
                    int amountdata = Integer.parseInt(etAmount.getText().toString().trim());
                    if (amountdata == 0) {
                        Toast.makeText(getActivity(), "Please enter valid amount.", Toast.LENGTH_SHORT).show();
                    } else if (amountdata < 200000) {
                        Alert.ShowAlert(getActivity(), "RTGS Transaction Is Only Possible For Amount 2,00,000 Or More");
                    } else {
                        PAY_TYPE = "RTGS";
                        Constants.TRANSACTION_TYPE = "RTGS";
                        progress.createDialog(false);
                        progress.dialogMessage("Please wait. . ");
                        progress.showDialog();
                        CheckBalance();
                    }
                }
                break;

            case R.id.linear_imps:
                if (etAmount.getText().toString().trim().equals("") || etAmount.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please enter amount", Toast.LENGTH_SHORT).show();
                } else {
                    Constants.TRANSACTION_TYPE = "IMPS";
                    progress.createDialog(false);
                    progress.dialogMessage("Please wait. . ");
                    progress.showDialog();
                    Check_IMPS_Active();
                }
                break;
            default:
                Toast.makeText(getActivity(), "Select any.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void CheckBalance() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "CheckBalance");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("Argacccode", Constants.ACC_CODE);
            reqParamJsonObject.put("Argbranchcode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("Argactyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAmount", etAmount.getText().toString().trim());
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.createDialog(false);
                                    progress.dialogMessage("Processing. . ");
                                    progress.showDialog();
                                    if (TYPE.equals("YBA")) {
                                        InternalTransferValidation();
                                    } else if (TYPE.equals("OBA")) {
                                        CalcOnlineTransactionCharge();
//                                        if (Constants.TRANSACTION_TYPE.equals("IMPS")) {
//
//                                        } else {
//
//                                        }
                                    } else {
                                        CheckIMPSValidation();
                                    }
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void CheckIMPSValidation() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "CheckIMPSValidation");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgEcsAccCode", ACC_NUMBER);
            reqParamJsonObject.put("ArgAmount", etAmount.getText().toString().trim());
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgBeneId", BENEFICIARY_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.createDialog(false);
                                    progress.dialogMessage("Sending OTP. . ");
                                    progress.showDialog();
                                    SendOTPCode();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void ExternalTransferValidation(String chargeAmount) {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "ExternalTransferValidation");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgPayeeType", TYPE);
            reqParamJsonObject.put("ArgAmount", etAmount.getText().toString().trim());
            reqParamJsonObject.put("ArgBeneId", BENEFICIARY_ID);
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgPaytype", PAY_TYPE);
            reqParamJsonObject.put("ArgChargeAmt", chargeAmount);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.createDialog(false);
                                    progress.dialogMessage("Sending OTP. . ");
                                    progress.showDialog();
                                    SendOTPCode();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void InternalTransferValidation() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "InternalTransferValidation");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgPayeeType", TYPE);
            reqParamJsonObject.put("ArgAmount", etAmount.getText().toString().trim());
            reqParamJsonObject.put("ArgFromAccountId", etFrom.getText().toString().trim());
            reqParamJsonObject.put("Argacccode", Constants.ACC_CODE);
            reqParamJsonObject.put("Argbranchcode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("Argactyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgToEcsacccode", etToAcc.getText().toString().trim());
            reqParamJsonObject.put("ArgTerminalIp", "");
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgSystemIp", Constants.IP);
            reqParamJsonObject.put("ArgBeneId", BENEFICIARY_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.createDialog(false);
                                    progress.dialogMessage("Sending OTP. . ");
                                    progress.showDialog();
                                    SendOTPCode();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void SendOTPCode() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "SendOTPCode");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("Argacccode", Constants.ACC_CODE);
            reqParamJsonObject.put("Argbranchcode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("Argactyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);

            if (Constants.TRANSACTION_TYPE.equals("IMPS")) {
                reqParamJsonObject.put("ArgSource", "P2A");
            } else if (Constants.TRANSACTION_TYPE.equals("INTERNAL")) {
                reqParamJsonObject.put("ArgSource", "IT");
            } else if (Constants.TRANSACTION_TYPE.equals("NEFT") || Constants.TRANSACTION_TYPE.equals("RTGS")) {
                reqParamJsonObject.put("ArgSource", "ET");
            }
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    if (Constants.TRANSACTION_TYPE.equals("IMPS")) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("NICK_NAME", BENEFICIARY_CODE);
                                        bundle.putString("NAME", BENEFICIARY_NAME);
                                        bundle.putString("TO_ACCOUNT", ACC_NUMBER);
                                        bundle.putString("FROM_ACCOUNT", FROM_ACC_CODE);
                                        bundle.putString("IFSC", IFSC_CODE);
                                        bundle.putString("AMOUNT", etAmount.getText().toString().trim());
                                        bundle.putString("REMARKS", etRemarks.getText().toString().trim());
                                        bundle.putString("TRANSFER_TYPE", "IMPS");
                                        bundle.putString("PAYEE_TYPE", "");
                                        bundle.putString("CHARGE_AMOUNT", "");
                                        bundle.putString("BENEFICIARY_ID", BENEFICIARY_ID);
                                        bundle.putString("PAY_TYPE", PAY_TYPE);
                                        bundle.putString("OTP_TYPE", "P2A");
                                        Constants.TransmitFragment(new FinalTransferFragment(), getActivity().getSupportFragmentManager(),
                                                R.id.frame_container, true, bundle);

                                    } else if (Constants.TRANSACTION_TYPE.equals("INTERNAL")) {


                                        Bundle bundle = new Bundle();
                                        bundle.putString("NICK_NAME", BENEFICIARY_CODE);
                                        bundle.putString("NAME", BENEFICIARY_NAME);
                                        bundle.putString("TO_ACCOUNT", ACC_NUMBER);
                                        bundle.putString("FROM_ACCOUNT", FROM_ACC_CODE);
                                        bundle.putString("IFSC", IFSC_CODE);
                                        bundle.putString("AMOUNT", etAmount.getText().toString().trim());
                                        bundle.putString("REMARKS", etRemarks.getText().toString().trim());
                                        bundle.putString("TRANSFER_TYPE", "INT");
                                        bundle.putString("PAYEE_TYPE", TYPE);
                                        bundle.putString("CHARGE_AMOUNT", "");
                                        bundle.putString("BENEFICIARY_ID", BENEFICIARY_ID);
                                        bundle.putString("PAY_TYPE", PAY_TYPE);
                                        bundle.putString("OTP_TYPE", "IT");
                                        Constants.TransmitFragment(new FinalTransferFragment(), getActivity().getSupportFragmentManager(),
                                                R.id.frame_container, true, bundle);

                                    } else if (Constants.TRANSACTION_TYPE.equals("NEFT")) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("NICK_NAME", BENEFICIARY_CODE);
                                        bundle.putString("NAME", BENEFICIARY_NAME);
                                        bundle.putString("TO_ACCOUNT", ACC_NUMBER);
                                        bundle.putString("FROM_ACCOUNT", FROM_ACC_CODE);
                                        bundle.putString("IFSC", IFSC_CODE);
                                        bundle.putString("AMOUNT", etAmount.getText().toString().trim());
                                        bundle.putString("REMARKS", etRemarks.getText().toString().trim());
                                        bundle.putString("TRANSFER_TYPE", "NEFT");
                                        bundle.putString("PAYEE_TYPE", TYPE);
                                        bundle.putString("CHARGE_AMOUNT", chargeAmount);
                                        bundle.putString("BENEFICIARY_ID", BENEFICIARY_ID);
                                        bundle.putString("PAY_TYPE", PAY_TYPE);
                                        bundle.putString("OTP_TYPE", "ET");
                                        Constants.TransmitFragment(new FinalTransferFragment(), getActivity().getSupportFragmentManager(),
                                                R.id.frame_container, true, bundle);
                                    } else if (Constants.TRANSACTION_TYPE.equals("RTGS")) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("NICK_NAME", BENEFICIARY_CODE);
                                        bundle.putString("NAME", BENEFICIARY_NAME);
                                        bundle.putString("TO_ACCOUNT", ACC_NUMBER);
                                        bundle.putString("FROM_ACCOUNT", FROM_ACC_CODE);
                                        bundle.putString("IFSC", IFSC_CODE);
                                        bundle.putString("AMOUNT", etAmount.getText().toString().trim());
                                        bundle.putString("REMARKS", etRemarks.getText().toString().trim());
                                        bundle.putString("TRANSFER_TYPE", "NEFT");
                                        bundle.putString("PAYEE_TYPE", TYPE);
                                        bundle.putString("CHARGE_AMOUNT", chargeAmount);
                                        bundle.putString("BENEFICIARY_ID", BENEFICIARY_ID);
                                        bundle.putString("PAY_TYPE", PAY_TYPE);
                                        bundle.putString("OTP_TYPE", "ET");
                                        Constants.TransmitFragment(new FinalTransferFragment(), getActivity().getSupportFragmentManager(),
                                                R.id.frame_container, true, bundle);
                                    }

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void Check_IMPS_Active() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "Check_IMPS_Active");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.createDialog(false);
                                    progress.dialogMessage("Please wait..");
                                    progress.showDialog();
                                    CheckBalance();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void CalcOnlineTransactionCharge() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "CalcOnlineTransactionCharge");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgAmount", etAmount.getText().toString().trim());
            reqParamJsonObject.put("ArgBeneIFSC", IFSC_CODE);
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgPaytype", PAY_TYPE);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        chargeAmount = jObj.getString("CHARGE_AMT");
                                    }
                                    progress.createDialog(false);
                                    progress.dialogMessage("Please wait..");
                                    progress.showDialog();
                                    ExternalTransferValidation(chargeAmount);
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
