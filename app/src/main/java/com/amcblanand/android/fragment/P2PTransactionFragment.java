package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.amcblanand.android.R;
import com.amcblanand.android.extra.Constants;

/**
 * Created by rakshit.sathwara on 8/29/2017.
 */

public class P2PTransactionFragment extends Fragment implements Validator.ValidationListener {

    private static final String TAG = Constants.TAG+ P2PTransactionFragment.class.getSimpleName();

    @NotEmpty
    @BindView(R.id.et_from_acc_imps)
    EditText etFromAccImps;

    @NotEmpty
    @Length(min = 10, message = "10 digit required.")
    @BindView(R.id.et_mobile)
    EditText etMobile;

    @NotEmpty
    @BindView(R.id.et_mmid)
    EditText etMmid;

    @NotEmpty
    @BindView(R.id.et_transfer_amount)
    EditText etTransferAmount;

    @BindView(R.id.et_remarks)
    EditText etRemarks;

    @BindView(R.id.btn_fund_transfer)
    Button btnFundTransfer;
    Unbinder unbinder;

    private Validator validator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_p_to_p_transaction, container, false);
        unbinder = ButterKnife.bind(this, view);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("IMPS Transfer(P2P)");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        validator = new Validator(this);
        validator.setValidationListener(this);

        etFromAccImps.setText(Constants.ACCOUNT_NUMBER);

        btnFundTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * Called when all {@link Rule}s pass.
     */
    @Override
    public void onValidationSucceeded() {
        Fragment fragment = new PreviewIMPSTransferFragment();
        Bundle bundle = new Bundle();
        bundle.putString("fromAcc", etFromAccImps.getText().toString().trim());
        bundle.putString("mobile", etMobile.getText().toString().trim());
        bundle.putString("mmid", etMmid.getText().toString().trim());
        bundle.putString("amount", etTransferAmount.getText().toString().trim());
        bundle.putString("remarks", etRemarks.getText().toString().trim());
        fragment.setArguments(bundle);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_container, fragment);
        ft.addToBackStack(null);
        ft.commit();

        etFromAccImps.setText("");
        etMobile.setText("");
        etMmid.setText("");
        etTransferAmount.setText("");
        etRemarks.setText("");
    }

    /**
     * Called when one or several {@link Rule}s fail.
     *
     * @param errors List containing references to the {@link View}s and
     *               {@link Rule}s that failed.
     */
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((TextView) (EditText) view).setError(message);
            } else if (view instanceof Spinner) {
                ((TextView) ((Spinner) view).getSelectedView()).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
