package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.callbackmodel.AccountListCallback;
import com.amcblanand.android.dialogs.AccountListDialog;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.AccountListModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 11/16/2017.
 */

public class ServiceFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = Constants.TAG + AccountsFragment.class.getSimpleName();

    @BindView(R.id.et_account_list)
    EditText etAccountList;
    @BindView(R.id.etx_account_list)
    TextInputLayout etxAccountList;
    @BindView(R.id.linear_imps_mini_statement)
    LinearLayout linearImpsMiniStatement;
    @BindView(R.id.linear_email_statement)
    LinearLayout linearEmailStatement;
    @BindView(R.id.linear_view_statement)
    LinearLayout linearViewStatement;
    @BindView(R.id.linear_cheque_book_request)
    LinearLayout linearChequeBookRequest;
    @BindView(R.id.linear_cheque_transaction)
    LinearLayout linearChequeTransaction;
    @BindView(R.id.linear_stop_cheque)
    LinearLayout linearStopCheque;
    Unbinder unbinder;
    @BindView(R.id.linear_imps_transaction_status)
    LinearLayout linearImpsTransactionStatus;
    @BindView(R.id.linear_cheque_status)
    LinearLayout linearChequeStatus;
    @BindView(R.id.linear_other_services)
    LinearLayout linearOtherServices;

    private Progress progress;
    private DecryptData decryptData;

    private List<AccountListModel> accountListModelList;
    private AccountListDialog accountListDialog;
    private Fragment fragment = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Services");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);
        unbinder = ButterKnife.bind(this, view);

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        progress.createDialog(false);
        progress.dialogMessage("Getting account list . . ");
        progress.showDialog();
        getAccountList();

        etAccountList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accountListDialog = new AccountListDialog(getActivity(), accountListModelList);
                accountListDialog.show();
            }
        });

        linearViewStatement.setOnClickListener(this);
        linearImpsMiniStatement.setOnClickListener(this);
        linearEmailStatement.setOnClickListener(this);
        linearChequeBookRequest.setOnClickListener(this);
        linearChequeTransaction.setOnClickListener(this);
        linearStopCheque.setOnClickListener(this);
        linearImpsTransactionStatus.setOnClickListener(this);
        linearChequeStatus.setOnClickListener(this);
        linearOtherServices.setOnClickListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void getAccountList() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetAccountList");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                accountListModelList = new ArrayList<AccountListModel>();

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        AccountListModel accountListPojo = new AccountListModel();
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        accountListPojo.setBankId(jObj.getString("BankId"));
                                        accountListPojo.setAccCode(jObj.getString("AccCode"));
                                        accountListPojo.setAcno(jObj.getString("Acno"));
                                        accountListPojo.setActyp(jObj.getString("Actyp"));
                                        accountListPojo.setBranchCode(jObj.getString("BranchCode"));
                                        accountListPojo.setCurBal(jObj.getString("CurBal"));
                                        accountListPojo.setEmail(jObj.getString("Email"));
                                        accountListPojo.setGender(jObj.getString("Gender"));
                                        accountListPojo.setName(jObj.getString("Name"));
                                        accountListModelList.add(accountListPojo);
                                    }
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AccountListCallback event) {
        accountListDialog.dismiss();
        etAccountList.setText(event.getAcno());

        Constants.ACCOUNT_BANK_ID = event.getBankId();
        Constants.ACTYP = event.getActyp();
        Constants.ACC_CODE = event.getAccCode();
        Constants.ACCOUNT_CUR_BAL = event.getCurBal();
        Constants.BRANCH_CODE = event.getBranchCode();
        Constants.ACCOUNT_NUMBER = event.getAcno();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linear_view_statement:
                if (etAccountList.getText().toString().trim().isEmpty() || etAccountList.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please select account.", Toast.LENGTH_SHORT).show();
                } else {
                    etAccountList.setText("");
                    Constants.TransmitFragment(new ViewTransactionFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                    fragment = new ViewTransactionFragment();
//                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                    ft.replace(R.id.frame_container, fragment);
//                    ft.addToBackStack(null);
//                    ft.commit();
                }
                break;

            case R.id.linear_imps_mini_statement:
                if (etAccountList.getText().toString().trim().isEmpty() || etAccountList.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please select account.", Toast.LENGTH_SHORT).show();
                } else {
                    if (Constants.IMPS_ACTIVE.equals("Y")) {
                        etAccountList.setText("");
                        Constants.TransmitFragment(new MiniStatementFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                        fragment = new MiniStatementFragment();
//                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                        ft.replace(R.id.frame_container, fragment);
//                        ft.addToBackStack(null);
//                        ft.commit();
                    } else {
                        Alert.ShowAlert(getActivity(), "IMPS not active.");
                    }

                }
                break;

            case R.id.linear_email_statement:
                if (etAccountList.getText().toString().trim().isEmpty() || etAccountList.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please select account.", Toast.LENGTH_SHORT).show();
                } else {
                    etAccountList.setText("");
                    Constants.TransmitFragment(new EmailStatementFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                    fragment = new EmailStatementFragment();
//                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                    ft.replace(R.id.frame_container, fragment);
//                    ft.addToBackStack(null);
//                    ft.commit();
                }
                break;

            case R.id.linear_cheque_book_request:
                if (etAccountList.getText().toString().trim().isEmpty() || etAccountList.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please select account.", Toast.LENGTH_SHORT).show();
                } else {
                    etAccountList.setText("");
                    Constants.TransmitFragment(new ChequeBookRequestFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                    fragment = new ChequeBookRequestFragment();
//                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                    ft.replace(R.id.frame_container, fragment);
//                    ft.addToBackStack(null);
//                    ft.commit();
                }
                break;

            case R.id.linear_cheque_transaction:
                if (etAccountList.getText().toString().trim().isEmpty() || etAccountList.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please select account.", Toast.LENGTH_SHORT).show();
                } else {
                    etAccountList.setText("");
                    Constants.TransmitFragment(new ChequeTransactionFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                    fragment = new ChequeTransactionFragment();
//                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                    ft.replace(R.id.frame_container, fragment);
//                    ft.addToBackStack(null);
//                    ft.commit();
                }
                break;

            case R.id.linear_stop_cheque:
                if (etAccountList.getText().toString().trim().isEmpty() || etAccountList.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please select account.", Toast.LENGTH_SHORT).show();
                } else {
                    etAccountList.setText("");
                    Constants.TransmitFragment(new StopChequePaymentFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                    fragment = new StopChequePaymentFragment();
//                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                    ft.replace(R.id.frame_container, fragment);
//                    ft.addToBackStack(null);
//                    ft.commit();
                }
                break;

            case R.id.linear_imps_transaction_status:
                if (etAccountList.getText().toString().trim().isEmpty() || etAccountList.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please select account.", Toast.LENGTH_SHORT).show();
                } else {
                    if (Constants.IMPS_ACTIVE.equals("Y")) {
                        etAccountList.setText("");
                        Constants.TransmitFragment(new IMPSTransactionStatus(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                        fragment = new IMPSTransactionStatus();
//                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                        ft.replace(R.id.frame_container, fragment);
//                        ft.addToBackStack(null);
//                        ft.commit();
                    } else {
                        Alert.ShowAlert(getActivity(), "IMPS not active.");
                    }

                }
                break;


            case R.id.linear_cheque_status:
                if (etAccountList.getText().toString().trim().isEmpty() || etAccountList.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please select account.", Toast.LENGTH_SHORT).show();
                } else {
                    etAccountList.setText("");
                    Constants.TransmitFragment(new ChequebookStatusFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                    fragment = new ChequeTransactionFragment();
//                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                    ft.replace(R.id.frame_container, fragment);
//                    ft.addToBackStack(null);
//                    ft.commit();
                }
                break;

            case R.id.linear_other_services:
                etAccountList.setText("");
                Constants.TransmitFragment(new OtherBankServicesFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
                break;


            default:
                Toast.makeText(getActivity(), "Select any.", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
