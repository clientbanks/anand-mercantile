package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;


import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;


public class IMPSTransactionStatus extends Fragment implements Validator.ValidationListener, View.OnClickListener {

    private static final String TAG = Constants.TAG + IMPSTransactionStatus.class.getSimpleName();

    @NotEmpty
    @BindView(R.id.et_transaction_no)
    EditText etTransactionNo;
    @BindView(R.id.btn_submit_transaction_status)
    Button btnSubmitTransactionStatus;
    Unbinder unbinder;
    @BindView(R.id.tv_BeneName)
    TextView tvBeneName;
    @BindView(R.id.tv_BeneAccNo)
    TextView tvBeneAccNo;
    @BindView(R.id.tv_Status)
    TextView tvStatus;
    @BindView(R.id.tv_TranAmount)
    TextView tvTranAmount;
    @BindView(R.id.tv_TrandateTime)
    TextView tvTrandateTime;

    private Validator validator;
    private Progress progress;
    private DecryptData decryptData;

    public IMPSTransactionStatus() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_impstransaction_status, container, false);
        unbinder = ButterKnife.bind(this, view);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("IMPS Status");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        validator = new Validator(this);
        validator.setValidationListener(this);

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        btnSubmitTransactionStatus.setOnClickListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit_transaction_status:
                validator.validate();
                break;
            default:
                break;
        }
    }


    @Override
    public void onValidationSucceeded() {
        progress.createDialog(false);
        progress.dialogMessage("Please wait..");
        progress.showDialog();
        Check_IMPS_Active();
    }

    private void TransactionStatus() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "TransactionStatus");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgUtrNo", etTransactionNo.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);
                                progress.hideDialog();
                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {

                                    String jsonXML = jsonObject.getString("ResponseJSON");
                                    JSONObject xml = new JSONObject(jsonXML);
                                    String xmlData = xml.getString("XML");
                                    JSONObject data = new JSONObject(xmlData);

                                    tvBeneName.setText("Bene Name :- " + data.getString("BeneName"));
                                    tvBeneAccNo.setText("Bene AccNo :- " + data.getString("BeneAccNo"));
                                    tvStatus.setText("Status :- " + data.getString("Status"));
                                    tvTranAmount.setText("Amount :- " + "\u20B9 " + data.getString("TranAmount"));
                                    tvTrandateTime.setText("Tran Date/Time :- " + data.getString("TrandateTime"));

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Alert.ShowAlert(getActivity(), "Something wrong!");
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
            Alert.ShowAlert(getActivity(), "Something wrong!");
        }
    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((TextView) (EditText) view).setError(message);
            } else if (view instanceof Spinner) {
                ((TextView) ((Spinner) view).getSelectedView()).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void Check_IMPS_Active() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "Check_IMPS_Active");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.createDialog(false);
                                    progress.dialogMessage("Please wait..");
                                    progress.showDialog();
                                    TransactionStatus();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
