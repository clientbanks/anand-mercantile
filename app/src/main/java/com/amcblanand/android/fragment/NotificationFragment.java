package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.amcblanand.android.R;
import com.amcblanand.android.adapter.NotificationListAdapter;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.NotificationPojo;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 11/18/2017.
 */

public class NotificationFragment extends Fragment {

    private static final String TAG = Constants.TAG + NotificationFragment.class.getSimpleName();

    @BindView(R.id.rv_notification)
    RecyclerView rvNotification;
    Unbinder unbinder;

    private Progress progress;
    private DecryptData decryptData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        unbinder = ButterKnife.bind(this, view);

        decryptData = new DecryptData();
        progress = new Progress(getActivity());

        progress.createDialog(false);
        progress.dialogMessage("Getting transaction details. .");
        progress.showDialog();
        GetNotificationDetails();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void GetNotificationDetails() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetNotificationDetails");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                List<NotificationPojo> viewTransactionPojoList = new ArrayList<>();

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        NotificationPojo viewTransactionPojo = new NotificationPojo();
                                        viewTransactionPojo.setRemittance(jObj.getString("REMITTANCE"));
                                        viewTransactionPojo.setBeneficiary(jObj.getString("BENEFICIARY"));
                                        viewTransactionPojo.setBeneficiary(jObj.getString("BENEFICIARY"));
                                        viewTransactionPojo.setAmount(jObj.getString("MESSAGE"));
                                        viewTransactionPojo.setTdate(jObj.getString("TDATE"));
                                        viewTransactionPojo.setTime(jObj.getString("TIME"));
                                        viewTransactionPojo.setTrantype(jObj.getString("TRAN_TYPE"));
                                        //toaccount still to add
                                        viewTransactionPojoList.add(viewTransactionPojo);
                                    }

                                    rvNotification.setHasFixedSize(true);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                    rvNotification.setLayoutManager(mLayoutManager);
                                    rvNotification.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                                    rvNotification.setItemAnimator(new DefaultItemAnimator());
                                    NotificationListAdapter accountListAdapter = new NotificationListAdapter(getActivity(), viewTransactionPojoList);
                                    rvNotification.setAdapter(accountListAdapter);

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
