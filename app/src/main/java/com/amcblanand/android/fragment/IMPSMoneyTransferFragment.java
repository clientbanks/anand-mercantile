package com.amcblanand.android.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.callbackmodel.AccountListCallback;
import com.amcblanand.android.dialogs.AccountListDialog;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.AccountListModel;
import com.amcblanand.android.model.PayeeListPojo;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class IMPSMoneyTransferFragment extends Fragment {

    private static final String TAG = Constants.TAG + FundTransferFragment.class.getSimpleName();

    @BindView(R.id.et_account_list)
    EditText etAccountList;
    @BindView(R.id.etx_account_list)
    TextInputLayout etxAccountList;
    @BindView(R.id.tv_cur_bal)
    TextView tvCurBal;
    @BindView(R.id.linear_oba)
    LinearLayout linearOba;
    Unbinder unbinder;

    private Progress progress;
    private DecryptData decryptData;

    private List<AccountListModel> accountListModelList;
    private AccountListDialog accountListDialog;
    private Fragment fragment = null;
    private List<PayeeListPojo> payeeListPojoList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_imps_transfer, container, false);
        unbinder = ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("IMPS Transfer");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        progress.createDialog(false);
        progress.dialogMessage("Getting account list . . ");
        progress.showDialog();
        getAccountList();

        etAccountList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accountListDialog = new AccountListDialog(getActivity(), accountListModelList);
                accountListDialog.show();
            }
        });

        linearOba.setOnClickListener(view1 -> {
            if (etAccountList.getText().toString().trim().equals("") || etAccountList.getText().toString().trim().isEmpty()) {
                Toast.makeText(getActivity(), "Select account.", Toast.LENGTH_SHORT).show();
            } else {
                etAccountList.setText("");
                progress.createDialog(false);
                progress.dialogMessage("Please wait. . ");
                progress.showDialog();
                GetPayeeListOBA();
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void getAccountList() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetAccountList");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                accountListModelList = new ArrayList<AccountListModel>();

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        AccountListModel accountListPojo = new AccountListModel();
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        accountListPojo.setBankId(jObj.getString("BankId"));
                                        accountListPojo.setAccCode(jObj.getString("AccCode"));
                                        accountListPojo.setAcno(jObj.getString("Acno"));
                                        accountListPojo.setActyp(jObj.getString("Actyp"));
                                        accountListPojo.setBranchCode(jObj.getString("BranchCode"));
                                        accountListPojo.setCurBal(jObj.getString("CurBal"));
                                        accountListPojo.setEmail(jObj.getString("Email"));
                                        accountListPojo.setGender(jObj.getString("Gender"));
                                        accountListPojo.setName(jObj.getString("Name"));
                                        accountListModelList.add(accountListPojo);
                                    }
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AccountListCallback event) {
        accountListDialog.dismiss();
        etAccountList.setText(event.getAcno());
        tvCurBal.setText("Current Balance :- " + event.getCurBal());

        Constants.ACCOUNT_BANK_ID = event.getBankId();
        Constants.ACTYP = event.getActyp();
        Constants.ACC_CODE = event.getAccCode();
        Constants.ACCOUNT_CUR_BAL = event.getCurBal();
        Constants.BRANCH_CODE = event.getBranchCode();
        Constants.ACCOUNT_NUMBER = event.getAcno();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void GetPayeeListOBA() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetPayeeList");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgPayeeType", "OBA");
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                payeeListPojoList = new
                                        ArrayList<PayeeListPojo>();
                                progress.hideDialog();
                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        PayeeListPojo payeeListPojo = new PayeeListPojo();
                                        payeeListPojo.setBene_Ac_No(jObj.getString("Bene_Ac_No"));
                                        payeeListPojo.setBene_Name(jObj.getString("Bene_Name"));
                                        payeeListPojo.setBene_Code(jObj.getString("Bene_Code"));
                                        payeeListPojo.setBene_Bank(jObj.getString("Bene_Bank"));
                                        payeeListPojo.setBene_Brach(jObj.getString("Bene_Brach"));
                                        payeeListPojo.setBeneficiary_ID(jObj.getString("Beneficiary_ID"));
                                        payeeListPojo.setIFSCCODE(jObj.getString("IFSCCODE"));
                                        payeeListPojoList.add(payeeListPojo);
                                    }

                                    Bundle bundle = new Bundle();
                                    bundle.putString("PAYEE_TYPE", "IMPS");
                                    bundle.putString("PAYEE_LIST", new Gson().toJson(payeeListPojoList));
                                    Constants.TransmitFragment(new PayeeListFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, bundle);

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
