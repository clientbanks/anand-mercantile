package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.amcblanand.android.R;
import com.amcblanand.android.adapter.FeedbackCategoryAdapter;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.FeedbackPojo;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 8/8/2017.
 */

public class FeedbackFragment extends Fragment implements Validator.ValidationListener, View.OnClickListener {

    private static final String TAG = Constants.TAG + FeedbackFragment.class.getSimpleName();

    @BindView(R.id.sp_feedback_category_list)
    Spinner spFeedbackCategoryList;

    @NotEmpty
    @BindView(R.id.et_feedback_subject)
    EditText etFeedbackSubject;

    @NotEmpty
    @BindView(R.id.et_feedback_message)
    EditText etFeedbackMessage;
    @BindView(R.id.btn_feedback)
    Button btnFeedback;
    Unbinder unbinder;

    private Progress progress;
    private Validator validator;
    private DecryptData decryptData;
    private String feedbackData = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Feedback Form");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);
        unbinder = ButterKnife.bind(this, view);

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        validator = new Validator(this);
        validator.setValidationListener(this);
        btnFeedback.setOnClickListener(this);

        progress.createDialog(false);
        progress.dialogMessage("Please wait..");
        progress.showDialog();
        BindFeedbackSubject();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_feedback:
                if (feedbackData.equals("") || feedbackData.isEmpty()) {
                    Alert.ShowAlert(getActivity(), "Please select category.");
                } else {
                    validator.validate();
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        progress.createDialog(false);
        progress.dialogMessage("Please wait. . ");
        progress.showDialog();
        sendFeedBack();
    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((TextView) (EditText) view).setError(message);
            } else if (view instanceof Spinner) {
                ((TextView) ((Spinner) view).getSelectedView()).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void sendFeedBack() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "Feedback");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgEmailID", Constants.EMAIL_ID);
            reqParamJsonObject.put("ArgSubject", feedbackData);
            reqParamJsonObject.put("ArgMessage", etFeedbackMessage.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                    etFeedbackMessage.setText("");
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void BindFeedbackSubject() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "BindFeedbackSubject");
            JSONObject reqParamJsonObject = new JSONObject();
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);
                                progress.hideDialog();

                                List<FeedbackPojo> feedbackPojoList = new ArrayList<FeedbackPojo>();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        FeedbackPojo feedbackPojo = new FeedbackPojo();
                                        feedbackPojo.setSRNO(jObj.getString("SRNO"));
                                        feedbackPojo.setSUBJECT(jObj.getString("SUBJECT"));
                                        feedbackPojoList.add(feedbackPojo);
                                    }

                                    spFeedbackCategoryList.setAdapter(new FeedbackCategoryAdapter(getActivity(),
                                            feedbackPojoList));
                                    spFeedbackCategoryList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                            FeedbackPojo feedbackPojo = (FeedbackPojo) adapterView.getItemAtPosition(i);
                                            feedbackData = feedbackPojo.getSUBJECT();
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> adapterView) {
                                            Toast.makeText(getActivity(), "Please select category.", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
