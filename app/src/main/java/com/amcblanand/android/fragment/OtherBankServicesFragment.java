package com.amcblanand.android.fragment;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amcblanand.android.R;
import com.amcblanand.android.adapter.OtherServicesAdapter;
import com.amcblanand.android.model.OtherServiceModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class OtherBankServicesFragment extends Fragment {


    @BindView(R.id.rv_other_bank_services)
    RecyclerView rvOtherBankServices;
    Unbinder unbinder;


    @NonNull
    private List<OtherServiceModel> otherServiceModelList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_other_bank_services, container, false);
        unbinder = ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Other Services");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);
        otherServiceModelList = new ArrayList<>();

        addData();

        rvOtherBankServices.hasFixedSize();
        rvOtherBankServices.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rvOtherBankServices.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        rvOtherBankServices.setItemAnimator(new DefaultItemAnimator());
        rvOtherBankServices.setAdapter(new OtherServicesAdapter(getActivity(), otherServiceModelList));

        return view;
    }

    private void addData() {

        OtherServiceModel otherServiceModel = new OtherServiceModel("Deposit Rate", R.drawable.ic_dds_final, R.color.service_one);
        otherServiceModelList.add(otherServiceModel);

        otherServiceModel = new OtherServiceModel("Loan Rate", R.drawable.icon_rupee_blue1, R.color.service_two);
        otherServiceModelList.add(otherServiceModel);

        otherServiceModel = new OtherServiceModel("EMI Cal", R.drawable.deposit_icon, R.color.service_three);
        otherServiceModelList.add(otherServiceModel);

            otherServiceModel = new OtherServiceModel("FDs Cal", R.drawable.ic_fd_final, R.color.service_four);
        otherServiceModelList.add(otherServiceModel);

        otherServiceModel = new OtherServiceModel("Contact Us", R.drawable.ic_contact_us, R.color.service_five);
        otherServiceModelList.add(otherServiceModel);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
