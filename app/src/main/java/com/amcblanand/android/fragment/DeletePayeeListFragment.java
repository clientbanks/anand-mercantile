package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.amcblanand.android.R;
import com.amcblanand.android.adapter.DeletePayeeAdapter;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.PayeeListPojo;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 8/29/2017.
 */

public class DeletePayeeListFragment extends Fragment {

    private static final String TAG = Constants.TAG + DeletePayeeListFragment.class.getSimpleName();

    @BindView(R.id.rv_delete_payee_list)
    RecyclerView rvDeletePayeeList;
    Unbinder unbinder;

    private Progress progress;
    private DecryptData decryptData;

    private String payee = "";
    private List<PayeeListPojo> payeeListPojoList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_delete_payee_list, container, false);
        unbinder = ButterKnife.bind(this, view);

        payee = getArguments().getString("payee_type");

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        progress.createDialog(false);
        progress.dialogMessage("Please wait. . ");
        progress.showDialog();
        GetPayeeList();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void GetPayeeList() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetPayeeList");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgPayeeType", payee);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                payeeListPojoList = new
                                        ArrayList<PayeeListPojo>();
                                progress.hideDialog();
                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        PayeeListPojo payeeListPojo = new PayeeListPojo();
                                        payeeListPojo.setBene_Ac_No(jObj.getString("Bene_Ac_No"));
                                        payeeListPojo.setBene_Name(jObj.getString("Bene_Name"));
                                        payeeListPojo.setBene_Code(jObj.getString("Bene_Code"));
                                        payeeListPojo.setBene_Bank(jObj.getString("Bene_Bank"));
                                        payeeListPojo.setBene_Brach(jObj.getString("Bene_Brach"));
                                        payeeListPojo.setBeneficiary_ID(jObj.getString("Beneficiary_ID"));
                                        payeeListPojo.setIFSCCODE(jObj.getString("IFSCCODE"));
                                        payeeListPojoList.add(payeeListPojo);
                                    }

                                    rvDeletePayeeList.hasFixedSize();
                                    rvDeletePayeeList.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    rvDeletePayeeList.setAdapter(new DeletePayeeAdapter(getActivity(), payeeListPojoList, payee));

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

//    @Subscribe
//    public void onDeletePayee(DeletePayeeCallback deletePayeeCallback) {
//        if (deletePayeeCallback.getStatus().equals("Y")) {
//            progress.createDialog(false);
//            progress.dialogMessage("Please wait. . ");
//            progress.showDialog();
//            GetPayeeList();
//        }
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        EventBus.getDefault().register(this);
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        EventBus.getDefault().unregister(this);
//    }
}
