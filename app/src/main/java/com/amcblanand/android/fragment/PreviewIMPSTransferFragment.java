package com.amcblanand.android.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.amcblanand.android.R;
import com.amcblanand.android.activity.PINVerificationActivity;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 8/18/2017.
 */

public class PreviewIMPSTransferFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = Constants.TAG + PreviewIMPSTransferFragment.class.getSimpleName();

    Unbinder unbinder;

    @BindView(R.id.btn_imps_preview_submit)
    Button btnImpsPreviewSubmit;
    @BindView(R.id.et_from_acc_imps)
    EditText etFromAccImps;
    @BindView(R.id.et_mobile_number_preview)
    EditText etMobileNumberPreview;
    @BindView(R.id.et_mmid_preview)
    EditText etMmidPreview;
    @BindView(R.id.et_amount_imps)
    EditText etAmountImps;
    @BindView(R.id.et_remarks_imps)
    EditText etRemarksImps;
    private String fromAcc = "", imps_type = "", param1 = "", param2 = "",
            amount = "", remarks = "";

    private Progress progress;
    private DecryptData decryptData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preview_imps_transfer, container, false);
        unbinder = ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Preview IMPS Transfer");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        fromAcc = getArguments().getString("fromAcc");
        param1 = getArguments().getString("mobile");
        param2 = getArguments().getString("mmid");
        amount = getArguments().getString("amount");
        remarks = getArguments().getString("remarks");

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        etFromAccImps.setText(fromAcc);
        etMobileNumberPreview.setText(param1);
        etMmidPreview.setText(param2);
        etAmountImps.setText(amount);
        etRemarksImps.setText(remarks);

        btnImpsPreviewSubmit.setOnClickListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_imps_preview_submit:
                progress.createDialog(false);
                progress.dialogMessage("Please wait. . ");
                progress.showDialog();
                Check_IMPS_Active();
                break;
            default:
                break;
        }
    }

    private void Check_IMPS_Active() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "Check_IMPS_Active");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.createDialog(false);
                                    progress.dialogMessage("Please wait. . ");
                                    progress.showDialog();
                                    CheckBalance();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void P2PFundTransfer() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "P2PFundTransfer");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgBeneMobile", etMobileNumberPreview.getText().toString().trim());
            reqParamJsonObject.put("ArgBeneMMID", etMmidPreview.getText().toString().trim());
            reqParamJsonObject.put("ArgAmount", etAmountImps.getText().toString().trim());
            reqParamJsonObject.put("ArgRemark", etRemarksImps.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    String jsonXML = jsonObject.getString("ResponseJSON");
                                    JSONObject xml = new JSONObject(jsonXML);
                                    String xmlData = xml.getString("XML");
                                    JSONObject data = new JSONObject(xmlData);
                                    String ActCode = data.getString("ActCode");
                                    String TransRefNo = data.getString("TransRefNo");

                                    if (ActCode.equals("000")) {
                                        Alert.ShowAlert(getActivity(), "Transaction successfully done. Your TransRefNo is : " + TransRefNo);
                                        etFromAccImps.setText("");
                                        etMobileNumberPreview.setText("");
                                        etMmidPreview.setText("");
                                        etAmountImps.setText("");
                                        etRemarksImps.setText("");
                                    } else {
                                        Alert.ShowAlert(getActivity(), "Transaction fail");
                                    }

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Constants.PIN_VERIFY == 1) {
            Constants.PIN_VERIFY = 0;
            progress.createDialog(false);
            progress.dialogMessage("Please wait..");
            progress.showDialog();
            P2PFundTransfer();
        }
    }

    private void SendOTPCode() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "SendOTPCode");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("Argacccode", Constants.ACC_CODE);
            reqParamJsonObject.put("Argbranchcode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("Argactyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    startActivityForResult(new Intent(getActivity(), PINVerificationActivity.class), 200);
                                    Toast.makeText(getActivity(), jsonObject.getString("ResponseMessage"), Toast.LENGTH_SHORT).show();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void CheckBalance() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "CheckBalance");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("Argacccode", Constants.ACC_CODE);
            reqParamJsonObject.put("Argbranchcode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("Argactyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAmount", etAmountImps.getText().toString().trim());
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.createDialog(false);
                                    progress.dialogMessage("Sending OTP Code. . ");
                                    progress.showDialog();
                                    SendOTPCode();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
