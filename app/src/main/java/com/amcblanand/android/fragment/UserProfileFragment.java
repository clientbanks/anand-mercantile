package com.amcblanand.android.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.zelory.compressor.Compressor;
import id.zelory.compressor.FileUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * Created by rakshit.sathwara on 8/11/2017.
 */

public class UserProfileFragment extends Fragment implements Validator.ValidationListener {

    private static final String TAG = Constants.TAG + UserProfileFragment.class.getSimpleName();


    @NotEmpty
    @BindView(R.id.et_user_name)
    EditText etUserName;

    @NotEmpty
    @BindView(R.id.et_mobile_number)
    EditText etMobileNumber;

    @BindView(R.id.et_email_address)
    EditText etEmailAddress;
    @BindView(R.id.btn_update_info)
    Button btnUpdateInfo;
    Unbinder unbinder;
    @BindView(R.id.iv_user_img)
    ImageView ivUserImg;
    @BindView(R.id.et_pan_no)
    EditText etPanNo;
    @BindView(R.id.et_address)
    EditText etAddress;

    private DecryptData decryptData;
    private Progress progress;
    private File actualImage;
    private File compressedImage;
    private Bitmap bitmap = null;
    private String photo;
    private Validator validator;
    Uri tempUri;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        unbinder = ButterKnife.bind(this, view);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Profile");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        decryptData = new DecryptData();
        progress = new Progress(getActivity());

        validator = new Validator(this);
        validator.setValidationListener(this);

        ivUserImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeImageDialog();
            }
        });

        progress.createDialog(false);
        progress.dialogMessage("Please wait..");
        progress.showDialog();
        GetEmailID();

        btnUpdateInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if (bitmap == null) {
//                    if (Constants.USER_PHOTO.equals("") || Constants.USER_PHOTO.isEmpty()) {
//                        Toast.makeText(getActivity(), "Please select profile.", Toast.LENGTH_SHORT).show();
//                    } else {
                validator.validate();
//                    }
//                } else {
//                    validator.validate();
//                }
            }
        });

        etUserName.setText(Constants.USER_NAME);
        etUserName.setEnabled(false);

        etAddress.setText(Constants.USER_ADDRESS);
        etPanNo.setText(Constants.USER_PAN_NO);

        etEmailAddress.setText(Constants.EMAIL_ID);
        etMobileNumber.setText(Constants.MOBILE_NUMBER);

        if (Constants.USER_PHOTO.equals("") || Constants.USER_PHOTO.isEmpty()) {
            byte[] imageByte = Base64.decode(Constants.USER_ICON, Base64.DEFAULT);
            Bitmap decodeImage = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
            ivUserImg.setImageBitmap(decodeImage);
        } else {
            byte[] imageByte = Base64.decode(Constants.USER_PHOTO, Base64.DEFAULT);
            Bitmap decodeImage = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
            ivUserImg.setImageBitmap(decodeImage);
//            ivUserImg.setImageBitmap(null);
        }


        return view;
    }

    /*
     * image Picker
     * */
    public void takeImageDialog() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 10);
                } else if (items[item].equals("Choose from Library")) {

                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            20);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    public void customCompressImage() {
        if (actualImage == null) {
            showError("Please choose an image!");
        } else {
            // Compress image using RxJava in background thread with custom Compressor
            new Compressor.Builder(getActivity())
                    .setMaxWidth(200)
                    .setMaxHeight(200)
                    .setQuality(70)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .build()
                    .compressToFileAsObservable(actualImage)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<File>() {
                        @Override
                        public void call(File file) {
                            compressedImage = file;
                            setCompressedImage();
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            showError(throwable.getMessage());
                        }
                    });
        }
    }

    private void setCompressedImage() {
        ivUserImg.setImageBitmap(BitmapFactory.decodeFile(compressedImage.getAbsolutePath()));
        bitmap = BitmapFactory.decodeFile(compressedImage.getAbsolutePath());
    }

    public void showError(String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 20 && resultCode == RESULT_OK) {
            if (data == null) {
                showError("Failed to open picture!");
                return;
            }
            try {
                Log.e("IMAGE SELECT", "onActivityResult: " + "IN  idProof 1");
                actualImage = FileUtil.from(getActivity(), data.getData());
                ivUserImg.setImageBitmap(BitmapFactory.decodeFile(actualImage.getAbsolutePath()));

                Log.e("SELECT-IMAGE", "onActivityResult: " + String.format("Size : %s", getReadableFileSize(actualImage.length())));

                customCompressImage();


            } catch (IOException e) {
                showError("Failed to read picture data!");
                e.printStackTrace();
            }
        } else if (requestCode == 10 && resultCode == RESULT_OK) {

            Log.e("IN camera", "onActivityResult: " + "DONE");

            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            Log.e("IMAGE SELECT", "onActivityResult: " + "IN 1 Camera");
            tempUri = getImageUri(getActivity(), thumbnail);
            Log.e("URi", "onActivityResult: " + tempUri);
            actualImage = new File(getRealPathFromURI(tempUri));
            Log.e("FILE path", "onActivityResult: " + actualImage);
            ivUserImg.setImageBitmap(thumbnail);
            customCompressImage();
            Log.e("CAMERA", "onActivityResult: " + String.format("Size : %s", getReadableFileSize(actualImage.length())));

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void UpdateProfileDetails() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "UpdateProfileDetails");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            if (bitmap == null) {
                reqParamJsonObject.put("ArgPic", Constants.USER_PHOTO);
            } else {
                reqParamJsonObject.put("ArgPic", Constants.encodeTobase64(bitmap));
            }
            reqParamJsonObject.put("ArgEmail", etEmailAddress.getText().toString().trim());
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);
                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {

                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }


    @Override
    public void onValidationSucceeded() {
        progress.createDialog(false);
        progress.dialogMessage("Please wait. . ");
        progress.showDialog();
        UpdateProfileDetails();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((TextView) (EditText) view).setError(message);
            } else if (view instanceof Spinner) {
                ((TextView) ((Spinner) view).getSelectedView()).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public String getReadableFileSize(long size) {
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    private void GetEmailID() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetEmailID");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);
                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    String EMAIL = " ";
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        EMAIL = jObj.getString("EMAIL");
                                    }
                                    etEmailAddress.setText(EMAIL);

                                } else {

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
