package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.amcblanand.android.R;
import com.amcblanand.android.adapter.MiniStatementAdapter;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.MiniStatement;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 8/19/2017.
 */

public class MiniStatementFragment extends Fragment {
    private static final String TAG = Constants.TAG + MiniStatementFragment.class.getSimpleName();
    @BindView(R.id.rv_mini_statement)
    RecyclerView rvMiniStatement;
    Unbinder unbinder;

    private Progress progress;
    private DecryptData decryptData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mini_statement, container, false);
        unbinder = ButterKnife.bind(this, view);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("IMPS Mini Statement");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        progress.createDialog(false);
        progress.dialogMessage("Please wait..");
        progress.showDialog();
        Check_IMPS_Active();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void Check_IMPS_Active() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "Check_IMPS_Active");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.createDialog(false);
                                    progress.dialogMessage("Please wait..");
                                    progress.showDialog();
                                    MiniStatement();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void MiniStatement() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "MiniStatement");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                List<MiniStatement> miniStatementList = new ArrayList<MiniStatement>();

                                if (jsonObject.getString("Status").equals("1")) {
                                    String jsonXML = jsonObject.getString("ResponseJSON");
                                    JSONObject xml = new JSONObject(jsonXML);
                                    String xmlData = xml.getString("XML");
                                    JSONObject data = new JSONObject(xmlData);
                                    JSONArray jsonArray = data.getJSONArray("Record");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject recordObject = jsonArray.getJSONObject(i);
                                        MiniStatement miniStatement = new MiniStatement();
                                        miniStatement.setBeneName(recordObject.getString("BeneName"));
                                        miniStatement.setStatus(recordObject.getString("Status"));
                                        miniStatement.setTranAmount(recordObject.getString("TranAmount"));
                                        miniStatement.setTranIndicator(recordObject.getString("TranIndicator"));
                                        miniStatement.setTranDateTime(recordObject.getString("TranDateTime"));
                                        miniStatement.setTranType(recordObject.getString("TranType"));
                                        miniStatement.setTransRefNo(recordObject.getString("TransRefNo"));
                                        miniStatementList.add(miniStatement);
                                    }

                                    rvMiniStatement.hasFixedSize();
                                    rvMiniStatement.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    rvMiniStatement.setAdapter(new MiniStatementAdapter(getActivity(), miniStatementList));

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
