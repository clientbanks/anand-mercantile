package com.amcblanand.android.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.BannerListModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 11/14/2017.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {

    Unbinder unbinder;
    @BindView(R.id.guideline_main_line)
    Guideline guidelineMainLine;
    @BindView(R.id.guideline_slider_notice)
    Guideline guidelineSliderNotice;
    @BindView(R.id.guideline_margin_left)
    Guideline guidelineMarginLeft;
    @BindView(R.id.guideline_margin_right)
    Guideline guidelineMarginRight;
    @BindView(R.id.guideline_dash_one)
    Guideline guidelineDashOne;
    @BindView(R.id.guideline_dash_two)
    Guideline guidelineDashTwo;
    @BindView(R.id.guideline_horizontal_dash_one)
    Guideline guidelineHorizontalDashOne;
    @BindView(R.id.guideline_horizontal_dash_two)
    Guideline guidelineHorizontalDashTwo;
    @BindView(R.id.linear_accounts)
    LinearLayout linearAccounts;
    @BindView(R.id.button2)
    LinearLayout button2;
    @BindView(R.id.linear_fund_transfer)
    LinearLayout linearFundTransfer;
    @BindView(R.id.button3)
    LinearLayout button3;
    @BindView(R.id.linear_mmid)
    LinearLayout linearMmid;
    @BindView(R.id.button4)
    LinearLayout button4;
    @BindView(R.id.linear_branch_locator)
    LinearLayout linearBranchLocator;
    @BindView(R.id.button5)
    LinearLayout button5;
    @BindView(R.id.linear_bhim_upi)
    LinearLayout linearBhimUpi;
    @BindView(R.id.button6)
    LinearLayout button6;
    @BindView(R.id.linear_bill_payment)
    LinearLayout linearBillPayment;
    @BindView(R.id.button7)
    LinearLayout button7;
    @BindView(R.id.lienar_card_services)
    LinearLayout lienarCardServices;
    @BindView(R.id.button8)
    LinearLayout button8;
    @BindView(R.id.linear_manage_payee)
    LinearLayout linearManagePayee;
    @BindView(R.id.button9)
    LinearLayout button9;
    @BindView(R.id.linear_service)
    LinearLayout linearService;
    @BindView(R.id.button10)
    LinearLayout button10;
    @BindView(R.id.iv_user_profile)
    CircleImageView ivUserProfile;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.tv_cust_id)
    TextView tvCustId;

    private Fragment fragment = null;

    private Progress progress;
    private DecryptData decryptData;

    private List<BannerListModel> bannerListModelList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_design, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("A M C B L");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(R.mipmap.ic_launcher_round);
        unbinder = ButterKnife.bind(this, view);

        linearAccounts.setOnClickListener(this);
        linearMmid.setOnClickListener(this);
        linearFundTransfer.setOnClickListener(this);
        linearService.setOnClickListener(this);
        linearBranchLocator.setOnClickListener(this);
        linearBhimUpi.setOnClickListener(this);
        linearBillPayment.setOnClickListener(this);
        lienarCardServices.setOnClickListener(this);
        linearManagePayee.setOnClickListener(this);

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

//        getBanner();
        GetProfileDetails();

        tvCustId.setText("ID : - " + Constants.CUST_ID);

        /*if (Constants.NeedPasswordReset) {
            AlertDialog dialog = new AlertDialog.Builder(getContext())
                    .setMessage(Constants.DialogeText)
                    .setNegativeButton("Close App", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().finish();
                        }
                    })
                    .setPositiveButton("Change password", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            clearBackStack();
                            FullScreenPasswordChange screenPasswordChange = new FullScreenPasswordChange();
                            screenPasswordChange.show(getActivity().getSupportFragmentManager(), null);
                        }
                    })
                    .setCancelable(false)
                    .create();
            dialog.show();
        }*/

        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        clearBackStack();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linear_accounts:
                Constants.TransmitFragment(new AccountsFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                fragment = new AccountsFragment();
//                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                ft.replace(R.id.frame_container, fragment);
//                ft.addToBackStack(null);
//                ft.commit();
                break;

            case R.id.linear_mmid:
                if (Constants.IMPS_ACTIVE.equals("Y")) {
                    Constants.TransmitFragment(new MyMMIDFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                    fragment = new MyMMIDFragment();
//                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                    fragmentTransaction.replace(R.id.frame_container, fragment);
//                    fragmentTransaction.addToBackStack(null);
//                    fragmentTransaction.commit();
                } else {
                    Alert.ShowAlert(getActivity(), "IMPS not active.");
                }
                break;

            case R.id.linear_fund_transfer:
                Constants.TransmitFragment(new FundTransferFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                fragment = new FundTransferFragment();
//                FragmentTransaction beginTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                beginTransaction.replace(R.id.frame_container, fragment);
//                beginTransaction.addToBackStack(null);
//                beginTransaction.commit();
                break;

            case R.id.linear_service:
                Constants.TransmitFragment(new ServiceFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                fragment = new ServiceFragment();
//                FragmentTransaction serviceTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                serviceTransaction.replace(R.id.frame_container, fragment);
//                serviceTransaction.addToBackStack(null);
//                serviceTransaction.commit();
                break;

            case R.id.linear_branch_locator:
                Constants.TransmitFragment(new ContactUsFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                fragment = new ContactUsFragment();
//                FragmentTransaction contactTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                contactTransaction.replace(R.id.frame_container, fragment);
//                contactTransaction.addToBackStack(null);
//                contactTransaction.commit();
                break;

            case R.id.linear_bhim_upi:
                Toast.makeText(getActivity(), "Coming soon.", Toast.LENGTH_SHORT).show();
                break;

            case R.id.linear_bill_payment:
                if (Constants.IMPS_ACTIVE.equals("Y")) {
                    Constants.TransmitFragment(new IMPSMoneyTransferFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
                } else {
                    Toast.makeText(getActivity(), "Coming soon.", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.lienar_card_services:
                Constants.TransmitFragment(new DebitCardRequestFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
                break;

            case R.id.linear_manage_payee:
                Constants.TransmitFragment(new ManagePayeeFragment(), getActivity().getSupportFragmentManager(), R.id.frame_container, true, null);
//                fragment = new ManagePayeeFragment();
//                FragmentTransaction managePayeeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                managePayeeTransaction.replace(R.id.frame_container, fragment);
//                managePayeeTransaction.addToBackStack(null);
//                managePayeeTransaction.commit();
                break;
            default:
                Toast.makeText(getActivity(), "Please Select", Toast.LENGTH_SHORT).show();
        }
    }


    private void clearBackStack() {
        FragmentManager manager = getFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }


    private void GetProfileDetails() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetProfileDetails");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, "Request");


            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, "Response");

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        Constants.USER_NAME = jObj.getString("UName");
                                        Constants.EMAIL_ID = jObj.getString("Email");
                                        Constants.USER_PHOTO = jObj.getString("PHOTO");
                                        Constants.USER_ADDRESS = jObj.getString("Address");
                                        Constants.USER_PAN_NO = jObj.getString("Pan");
                                    }
                                    if (Constants.USER_PHOTO.equals("") || Constants.USER_PHOTO.isEmpty()) {
                                        ivUserProfile.setBackgroundResource(R.drawable.ic_user_icon);
                                    } else {
                                        byte[] imageByte = Base64.decode(Constants.USER_PHOTO, Base64.DEFAULT);
                                        Bitmap decodeImage = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
                                        ivUserProfile.setImageBitmap(decodeImage);
                                    }
                                    tvUserName.setText(Constants.USER_NAME);
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
