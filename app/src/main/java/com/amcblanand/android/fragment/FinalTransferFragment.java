package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 1/8/2018.
 */

public class FinalTransferFragment extends Fragment implements Validator.ValidationListener {

    private static final String TAG = Constants.TAG + FinalTransferFragment.class.getSimpleName();

    @BindView(R.id.et_nick_name)
    EditText etNickName;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_to_acc)
    EditText etToAcc;
    @BindView(R.id.et_from)
    EditText etFrom;
    @BindView(R.id.et_bank_ifsc_code)
    EditText etBankIfscCode;
    @BindView(R.id.et_amount)
    EditText etAmount;
    @BindView(R.id.et_remarks)
    EditText etRemarks;
    @BindView(R.id.et_otp_verification)
    EditText etOtpVerification;

    @NotEmpty
    @Length(min = 7, message = "7 digit OTP required.")
    @BindView(R.id.btn_verify_otp)
    Button btnVerifyOtp;
    Unbinder unbinder;
    @BindView(R.id.et_charges)
    EditText etCharges;
    @BindView(R.id.txt_charges)
    TextInputLayout txtCharges;
    @BindView(R.id.tv_resend_otp)
    TextView tvResendOtp;

    private Progress progress;
    private DecryptData decryptData;
    private Validator validator;

    private String NICK_NAME = "", NAME = "", TO_ACCOUNT = "", FROM_ACCOUNT = "", IFSC = "", AMOUNT = "",
            REMARKS = "", TRANSFER_TYPE = "", PAYEE_TYPE = "", CHARGE_AMOUNT = "", BENEFICIARY_ID = "", PAY_TYPE = "", OTP_TYPE = "";
    private String chargeAmount = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_final_transfer, container, false);
        unbinder = ButterKnife.bind(this, view);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Verify Transaction");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        NICK_NAME = getArguments().getString("NICK_NAME");
        NAME = getArguments().getString("NAME");
        TO_ACCOUNT = getArguments().getString("TO_ACCOUNT");
        FROM_ACCOUNT = getArguments().getString("FROM_ACCOUNT");
        IFSC = getArguments().getString("IFSC");
        AMOUNT = getArguments().getString("AMOUNT");
        REMARKS = getArguments().getString("REMARKS");
        TRANSFER_TYPE = getArguments().getString("TRANSFER_TYPE");
        PAYEE_TYPE = getArguments().getString("PAYEE_TYPE");
        CHARGE_AMOUNT = getArguments().getString("CHARGE_AMOUNT");
        BENEFICIARY_ID = getArguments().getString("BENEFICIARY_ID");
        PAY_TYPE = getArguments().getString("PAY_TYPE");
        OTP_TYPE = getArguments().getString("OTP_TYPE");

        progress = new Progress(getActivity());
        decryptData = new DecryptData();
        validator = new Validator(this);
        validator.setValidationListener(this);

        etNickName.setText(NICK_NAME);
        etName.setText(NAME);
        etToAcc.setText(TO_ACCOUNT);
        etFrom.setText(FROM_ACCOUNT);
        etBankIfscCode.setText(IFSC);
        etAmount.setText(AMOUNT);
        etRemarks.setText(REMARKS);

        if (PAYEE_TYPE.equals("OBA")) {
            CalcOnlineTransactionCharge();
        } else {
            txtCharges.setVisibility(View.GONE);
            etCharges.setText("");
        }

        btnVerifyOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });

        tvResendOtp.setOnClickListener(view1 -> {
            progress.createDialog(false);
            progress.dialogMessage("Please wait..");
            progress.showDialog();
            SendOTPCode();
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    /*
     * <pre>
     *     ExternalTransfer
     * </pre>
     *
     * */
    private void ExternalTransfer() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "ExternalTransfer");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgPayeeType", PAYEE_TYPE);
            reqParamJsonObject.put("ArgAmount", etAmount.getText().toString().trim());
            reqParamJsonObject.put("ArgBeneId", BENEFICIARY_ID);
            reqParamJsonObject.put("ArgRemarks", etRemarks.getText().toString().trim());
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgChargeAmt", chargeAmount);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqParamJsonObject.put("ArgPaytype", PAY_TYPE);
            reqParamJsonObject.put("ArgOTP", etOtpVerification.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                    Fragment fragment = new HomeFragment();
                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.frame_container, fragment);
                                    fragmentTransaction.addToBackStack(null);
                                    fragmentTransaction.commit();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }


    /*
     * <pre>
     *     InternalTransfer
     * </pre>
     *
     * */
    private void InternalTransfer() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "InternalTransfer");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgPayeeType", PAYEE_TYPE);
            reqParamJsonObject.put("ArgAmount", etAmount.getText().toString().trim());
            reqParamJsonObject.put("ArgFromAccountId", etFrom.getText().toString().trim());
            reqParamJsonObject.put("ArgToEcsacccode", etToAcc.getText().toString().trim());
            reqParamJsonObject.put("ArgTerminalIp", "");
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgSystemIp", "");
            reqParamJsonObject.put("ArgBeneId", BENEFICIARY_ID);
            reqParamJsonObject.put("ArgOTP", etOtpVerification.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                    Fragment fragment = new HomeFragment();
                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.frame_container, fragment);
                                    fragmentTransaction.addToBackStack(null);
                                    fragmentTransaction.commit();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }


    /*
     * <pre>
     *     P2AFundTransfer - IMPS
     * </pre>
     *
     * */
    private void P2AFundTransfer() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "P2AFundTransfer");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgBeneAccNo", etToAcc.getText().toString().trim());
            reqParamJsonObject.put("ArgBeneIFSC", etBankIfscCode.getText().toString().trim());
            reqParamJsonObject.put("ArgAmount", etAmount.getText().toString().trim());
            reqParamJsonObject.put("ArgRemark", etRemarks.getText().toString().trim());
            reqParamJsonObject.put("ArgOTP", etOtpVerification.getText().toString().trim());
            reqParamJsonObject.put("ArgBeneId", BENEFICIARY_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    String responseJson = jsonObject.getString("ResponseJSON");
                                    JSONObject obj = new JSONObject(responseJson);
                                    if (obj.getString("RespCode").equals("00")) {
                                        Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                        Fragment fragment = new HomeFragment();
                                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.frame_container, fragment);
                                        fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();
                                    } else {
                                        Alert.ShowAlert(getActivity(), "Transaction fail");
                                    }
                                    /*String jsonXML = jsonObject.getString("ResponseJSON");
                                    JSONObject xml = new JSONObject(jsonXML);
                                    String xmlData = xml.getString("XML");
                                    JSONObject data = new JSONObject(xmlData);
                                    String ActCode = data.getString("ActCode");
                                    String TransRefNo = data.getString("TransRefNo");

                                    if (ActCode.equals("000")) {
                                        Alert.ShowAlert(getActivity(), "Your Transaction is Successfully Processed with reference no :- " + TransRefNo);
                                        Fragment fragment = new HomeFragment();
                                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.frame_container, fragment);
                                        fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();
                                    } else {
                                        Alert.ShowAlert(getActivity(), "Transaction fail");
                                    }*/
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    @Override
    public void onValidationSucceeded() {
        progress.createDialog(false);
        progress.dialogMessage("Verify OTP..");
        progress.showDialog();
        if (TRANSFER_TYPE.equals("INT")) {
            CheckOTPCodeOpt(TRANSFER_TYPE);
//            InternalTransfer();
        } else if (TRANSFER_TYPE.equals("IMPS")) {
            CheckOTPCodeOpt(TRANSFER_TYPE);
        } else if (TRANSFER_TYPE.equals("NEFT")) {
            CheckOTPCodeOpt(TRANSFER_TYPE);
        } else {
            CheckOTPCodeOpt(TRANSFER_TYPE);
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void CheckOTPCodeOpt(String TRANSFER_TYPE) {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "CheckOTPCodeOpt");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgOtpCode", etOtpVerification.getText().toString().trim());
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);

            if (TRANSFER_TYPE.equals("INT")) {
                reqParamJsonObject.put("ArgSource", "IT");
            } else if (TRANSFER_TYPE.equals("NEFT")) {
                reqParamJsonObject.put("ArgSource", "ET");
            } else {
                reqParamJsonObject.put("ArgSource", "P2A");
            }


            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.createDialog(false);
                                    progress.dialogMessage("Please wait..");
                                    progress.showDialog();
                                    if (TRANSFER_TYPE.equals("INT")) {
                                        InternalTransfer();
                                    } else if (TRANSFER_TYPE.equals("IMPS")) {
                                        P2AFundTransfer();
                                    } else if (TRANSFER_TYPE.equals("NEFT")) {
                                        ExternalTransfer();
                                    } else {
                                        ExternalTransfer();
                                    }
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }


    private void CalcOnlineTransactionCharge() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "CalcOnlineTransactionCharge");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgAmount", AMOUNT);
            reqParamJsonObject.put("ArgBeneIFSC", IFSC);
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgPaytype", PAY_TYPE);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        chargeAmount = jObj.getString("CHARGE_AMT");
                                    }
                                    txtCharges.setVisibility(View.VISIBLE);
                                    etCharges.setText("\u20B9 " + chargeAmount);
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void SendOTPCode() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "SendOTPCode");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("Argacccode", Constants.ACC_CODE);
            reqParamJsonObject.put("Argbranchcode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("Argactyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgSource", OTP_TYPE);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    Toast.makeText(getActivity(), jsonObject.getString("ResponseMessage"), Toast.LENGTH_SHORT).show();
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
