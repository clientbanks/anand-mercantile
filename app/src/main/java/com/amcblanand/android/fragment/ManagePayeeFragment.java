package com.amcblanand.android.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.amcblanand.android.R;
import com.amcblanand.android.adapter.ManagePayeeListAdapter;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.callbackmodel.AccountListCallback;
import com.amcblanand.android.dialogs.AccountListDialog;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.AccountListModel;
import com.amcblanand.android.model.PayeeListPojo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 11/16/2017.
 */

public class ManagePayeeFragment extends Fragment /*implements View.OnClickListener */ {

    private static final String TAG = Constants.TAG + ManagePayeeFragment.class.getSimpleName();

    @BindView(R.id.et_account_list)
    EditText etAccountList;
    @BindView(R.id.etx_account_list)
    TextInputLayout etxAccountList;
    @BindView(R.id.tv_yba)
    TextView tvYba;
    @BindView(R.id.switch_change_type)
    Switch switchChangeType;
    @BindView(R.id.tv_oba)
    TextView tvOba;
    @BindView(R.id.linear_switch)
    LinearLayout linearSwitch;

    Unbinder unbinder;
    @BindView(R.id.btn_add_payee)
    FloatingActionButton btnAddPayee;
    @BindView(R.id.rv_payee_list_manage_payee)
    RecyclerView rvPayeeListManagePayee;
    @BindView(R.id.layout_yba)
    LinearLayout layoutYba;
    @BindView(R.id.layout_oba)
    LinearLayout layoutOba;
    @BindView(R.id.layout_relative)
    RelativeLayout layoutRelative;

    private Progress progress;
    private DecryptData decryptData;

    private List<AccountListModel> accountListModelList = new ArrayList<>();
    private AccountListDialog accountListDialog;

    private Fragment fragment = null;
    private List<PayeeListPojo> payeeListPojoList;

    private boolean fabExpanded = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_payee, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Manage Payee");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);
        unbinder = ButterKnife.bind(this, view);
//        btnAddPayee.setOnClickListener(this);
//        btnDeletePayee.setOnClickListener(this);

        progress = new Progress(getActivity());
        decryptData = new DecryptData();

//        progress.createDialog(false);
//        progress.dialogMessage("Getting account list . . ");
//        progress.showDialog();
        getAccountList();


        switchChangeType.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                tvYba.setTextSize(18);
                tvOba.setTextSize(14);
                tvYba.setTypeface(null, Typeface.BOLD);
                progress.createDialog(false);
                progress.dialogMessage("Please wait..");
                progress.showDialog();
                GetPayeeList();

            } else {
                tvOba.setTextSize(18);
                tvYba.setTextSize(14);
                tvOba.setTypeface(null, Typeface.BOLD);
                progress.createDialog(false);
                progress.dialogMessage("Please wait..");
                progress.showDialog();
                GetPayeeList();
            }
        });

        etAccountList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                linearSwitch.setVisibility(View.GONE);
//                switchChangeType.setChecked(false);
//                rvPayeeListManagePayee.setVisibility(View.GONE);
                if (accountListModelList.size() < 0) {
                    Toast.makeText(getActivity(), "Please wait..", Toast.LENGTH_SHORT).show();
                } else {
                    accountListDialog = new AccountListDialog(getActivity(), accountListModelList);
                    accountListDialog.show();
                }
            }
        });

        btnAddPayee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fabExpanded == true) {
                    closeSubMenusFab();
                } else {
                    openSubMenusFab();
                }
            }
        });

        closeSubMenusFab();

        return view;
    }


    //closes FAB submenus
    private void closeSubMenusFab() {
        layoutOba.setVisibility(View.INVISIBLE);
        layoutYba.setVisibility(View.INVISIBLE);
        layoutRelative.setVisibility(View.INVISIBLE);
        btnAddPayee.setImageResource(R.drawable.ic_add_circle_outline);
        fabExpanded = false;
    }

    //Opens FAB submenus
    private void openSubMenusFab() {
        layoutOba.setVisibility(View.VISIBLE);
        layoutYba.setVisibility(View.VISIBLE);
        layoutRelative.setVisibility(View.VISIBLE);
        btnAddPayee.setImageResource(R.drawable.ic_close);
        fabExpanded = true;

        layoutOba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etAccountList.getText().toString().trim().isEmpty() || etAccountList.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please select account.", Toast.LENGTH_SHORT).show();
                } else {
                    switchChangeType.setChecked(true);
                    etAccountList.setText("");
//                    switchChangeType.setEnabled(false);
//                    switchChangeType.setVisibility(View.GONE);
                    rvPayeeListManagePayee.setVisibility(View.GONE);
                    Fragment fragment = new AddBeneficiaryFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("payee_type", "OBA");
                    fragment.setArguments(bundle);
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_container, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });


        layoutYba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etAccountList.getText().toString().trim().isEmpty() || etAccountList.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please select account.", Toast.LENGTH_SHORT).show();
                } else {
                    switchChangeType.setChecked(true);
                    etAccountList.setText("");
//                    switchChangeType.setEnabled(false);
//                    switchChangeType.setVisibility(View.GONE);
                    rvPayeeListManagePayee.setVisibility(View.GONE);
                    Fragment fragment = new AddBeneficiaryFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("payee_type", "YBA");
                    fragment.setArguments(bundle);
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_container, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.btn_add_payee:
//                if (etAccountList.getText().toString().trim().equals("") || etAccountList.getText().toString().trim().isEmpty()) {
//                    Toast.makeText(getActivity(), "Select account.", Toast.LENGTH_SHORT).show();
//                } else {
//                    AddPayeeDialog addPayeeDialog = new AddPayeeDialog(getActivity(), "P");
//                    addPayeeDialog.show();
//                }
//                break;
//
//            case R.id.btn_delete_payee:
//                if (etAccountList.getText().toString().trim().equals("") || etAccountList.getText().toString().trim().isEmpty()) {
//                    Toast.makeText(getActivity(), "Select account.", Toast.LENGTH_SHORT).show();
//                } else {
//                    AddPayeeDialog deletePayeeDialog = new AddPayeeDialog(getActivity(), "D");
//                    deletePayeeDialog.show();
//                }
//                break;
//
//            default:
//                Toast.makeText(getActivity(), "Select any.", Toast.LENGTH_SHORT).show();
//                break;
//        }
//    }

    private void getAccountList() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetAccountList");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

//                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        AccountListModel accountListPojo = new AccountListModel();
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        accountListPojo.setBankId(jObj.getString("BankId"));
                                        accountListPojo.setAccCode(jObj.getString("AccCode"));
                                        accountListPojo.setAcno(jObj.getString("Acno"));
                                        accountListPojo.setActyp(jObj.getString("Actyp"));
                                        accountListPojo.setBranchCode(jObj.getString("BranchCode"));
                                        accountListPojo.setCurBal(jObj.getString("CurBal"));
                                        accountListPojo.setEmail(jObj.getString("Email"));
                                        accountListPojo.setGender(jObj.getString("Gender"));
                                        accountListPojo.setName(jObj.getString("Name"));
                                        accountListModelList.add(accountListPojo);
                                    }

                                   /* if (switchChangeType.isChecked()) {
                                        tvYba.setTextSize(18);
                                        tvOba.setTextSize(14);
                                        tvYba.setTypeface(null, Typeface.BOLD);
                                        progress.createDialog(false);
                                        progress.dialogMessage("Please wait..");
                                        progress.showDialog();
                                        GetPayeeList();
                                    } else {
                                        tvOba.setTextSize(18);
                                        tvYba.setTextSize(14);
                                        tvOba.setTypeface(null, Typeface.BOLD);
                                        progress.createDialog(false);
                                        progress.dialogMessage("Please wait..");
                                        progress.showDialog();
                                        GetPayeeList();
                                    }*/

//                                    if (switchChangeType.isChecked()) {
//                                        tvYba.setTextSize(18);
//                                        tvOba.setTextSize(14);
//                                        tvYba.setTypeface(null, Typeface.BOLD);
//                                        progress.createDialog(false);
//                                        progress.dialogMessage("Please wait..");
//                                        progress.showDialog();
//                                        GetPayeeList();
//
//                                    } else {
//                                        tvOba.setTextSize(18);
//                                        tvYba.setTextSize(14);
//                                        tvOba.setTypeface(null, Typeface.BOLD);
//                                        progress.createDialog(false);
//                                        progress.dialogMessage("Please wait..");
//                                        progress.showDialog();
//                                        GetPayeeList();
//                                    }

                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
//                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
//                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
//            progress.hideDialog();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AccountListCallback event) {
        accountListDialog.dismiss();
        etAccountList.setText(event.getAcno());

        Constants.ACCOUNT_BANK_ID = event.getBankId();
        Constants.ACTYP = event.getActyp();
        Constants.ACC_CODE = event.getAccCode();
        Constants.ACCOUNT_CUR_BAL = event.getCurBal();
        Constants.BRANCH_CODE = event.getBranchCode();
        Constants.ACCOUNT_NUMBER = event.getAcno();

//        linearSwitch.setVisibility(View.VISIBLE);
//        if (switchChangeType.isChecked()) {
//            tvYba.setTextSize(18);
//            tvOba.setTextSize(14);
//            tvYba.setTypeface(null, Typeface.BOLD);
//            progress.createDialog(false);
//            progress.dialogMessage("Please wait..");
//            progress.showDialog();
//            GetPayeeList();
//        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnDelete(com.amcblanand.android.callback.DeletePayeeCallback deletePayeeCallback) {
        if (deletePayeeCallback.getStatus().equals("Y")) {
            Handler mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progress.createDialog(false);
                    progress.dialogMessage("Please wait. . ");
                    progress.showDialog();
                    GetPayeeList();
                }
            }, 1000);

        }
    }

//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onDeletePayee(DeletePayeeCallback deletePayeeCallback) {
//        if (deletePayeeCallback.getStatus().equals("Y")) {
//            progress.createDialog(false);
//            progress.dialogMessage("Please wait. . ");
//            progress.showDialog();
//            GetPayeeList();
//        }
//    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void GetPayeeList() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetPayeeList");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);

            if (switchChangeType.isChecked()) {
                reqParamJsonObject.put("ArgPayeeType", "YBA");
            } else {
                reqParamJsonObject.put("ArgPayeeType", "OBA");
            }

            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                payeeListPojoList = new
                                        ArrayList<PayeeListPojo>();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        PayeeListPojo payeeListPojo = new PayeeListPojo();
                                        payeeListPojo.setBene_Ac_No(jObj.getString("Bene_Ac_No"));
                                        payeeListPojo.setBene_Name(jObj.getString("Bene_Name"));
                                        payeeListPojo.setBene_Code(jObj.getString("Bene_Code"));
                                        payeeListPojo.setBene_Bank(jObj.getString("Bene_Bank"));
                                        payeeListPojo.setBene_Brach(jObj.getString("Bene_Brach"));
                                        payeeListPojo.setBeneficiary_ID(jObj.getString("Beneficiary_ID"));
                                        payeeListPojo.setIFSCCODE(jObj.getString("IFSCCODE"));
                                        payeeListPojo.setBeneEmailID(jObj.getString("Bene_Email"));
                                        payeeListPojo.setBeneMobileNumber(jObj.getString("Bene_Mobile"));
                                        payeeListPojoList.add(payeeListPojo);
                                    }
                                    rvPayeeListManagePayee.setVisibility(View.VISIBLE);
                                    rvPayeeListManagePayee.hasFixedSize();
                                    rvPayeeListManagePayee.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    rvPayeeListManagePayee.setAdapter(new ManagePayeeListAdapter(getActivity(), payeeListPojoList/*, payeeType*/));
                                    progress.hideDialog();
                                } else {
                                    rvPayeeListManagePayee.setVisibility(View.GONE);
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
