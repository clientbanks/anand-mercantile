package com.amcblanand.android.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;


import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 8/31/2017.
 */

public class StopChequePaymentFragment extends Fragment implements Validator.ValidationListener {

    private static final String TAG = Constants.TAG + StopChequePaymentFragment.class.getSimpleName();


    @BindView(R.id.btn_check)
    Button btnCheck;
    Unbinder unbinder;

    //    @NotEmpty
//    @Length(min = 6, message = "6 digit required.")
    @BindView(R.id.et_cheque_no)
    TextInputEditText etChequeNo;

    @NotEmpty
    @BindView(R.id.et_reason)
    TextInputEditText etReason;

    private Progress progress;
    private DecryptData decryptData;
    private Validator validator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stop_cheque_payment, container, false);
        unbinder = ButterKnife.bind(this, view);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Stop Cheque Payment");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(null);

        validator = new Validator(this);
        validator.setValidationListener(this);
        progress = new Progress(getActivity());
        decryptData = new DecryptData();

        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etChequeNo.getText().toString().trim().equals("")) {
                    etChequeNo.setError("Please enter cheque no.");
                } else {
                    validator.validate();
                }
            }
        });

        return view;
    }

    @Override
    public void onValidationSucceeded() {
        progress.createDialog(false);
        progress.dialogMessage("Please wait..");
        progress.showDialog();
        StopChequePayment();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((TextView) (EditText) view).setError(message);
            } else if (view instanceof Spinner) {
                ((TextView) ((Spinner) view).getSelectedView()).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void StopChequePayment() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "StopChequePayment");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgChqNo", etChequeNo.getText().toString().trim());
            reqParamJsonObject.put("ArgReason", etReason.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(getActivity(), dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(getActivity()) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);
                                progress.hideDialog();
                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                if (jsonObject.getString("Status").equals("1")) {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                } else {
                                    Alert.ShowAlert(getActivity(), jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Alert.ShowAlert(getActivity(), "Something wrong!");
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
            Alert.ShowAlert(getActivity(), "Something wrong!");
        }
    }
}
