package com.amcblanand.android.extra;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;


    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static boolean isInternetConnencted(Context context) {
        // boolean isRunning = true;
        boolean retVal = false;
        int conn = NetworkUtil.getConnectivityStatus(context);
        if (conn == NetworkUtil.TYPE_WIFI) {
            retVal = true;
        } else if (conn == NetworkUtil.TYPE_MOBILE) {
            retVal = true;
        } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
            retVal = false;
        }
     /*   while (isRunning) {

            if (!retVal) {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message mesg) {
                        throw new RuntimeException();
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
                builder.setTitle(context.getApplicationInfo().loadLabel(context.getPackageManager()))
                        .setMessage("Please check your internet connection.")
                        .setIcon(R.drawable.ic_favicon_small)
                        .setCancelable(false)
                        .setPositiveButton("Retry",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        handler.sendMessage(handler.obtainMessage());
                                    }
                                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                try {
                    Looper.loop();
                } catch (RuntimeException e2) {
                }

            } else {
                isRunning = false;
            }
        }*/
        return retVal;

    }
}