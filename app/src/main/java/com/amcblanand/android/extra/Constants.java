package com.amcblanand.android.extra;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

/**
 * Created by rakshit.sathwara on 11/10/2017.
 */

public class Constants {
    public static final String TAG = "SB";
    public static final String NOTIFICATION_SERVICE = "notification";
    public static final String SHARED_PREF = "firebase";
    public static final String USER_ICON = "iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAB2lJREFUeJztnVuIlVUUgL85zTiOo4OmaWqNKXaxvJCUQQqWZbeHgl66UFYKIRH0UHSPivDBIHrpgtCdiq5aaHQlodQwpCK7eSXTUrPSsWbM0ZnpYY0gJ8dzzr78a5/zrw/Wy3D2/tdea83+/31bGwzDMAzDMAzDMAzDMIxcUKetQAbUAeOBacAkYBwwEhgMNPb+Zj+wG9gObAbWAl8Cm4CejPU1AlAAZgGLgG2IE11kK/A0MJN8/LNUPQOA24CNuDu9L9kA3Ao0ZdYao2yOAW4GdhDe8cXyGzAX6WWMBJgArCa+44tlJXBKBu0zjsI8YB/ZO/+QtANzorfS+B/1wJPoOb5YHkdeQ0YGNAJL0Hd6sbwB9IvYbgMx8DL0nd2XvAM0RGt9zikAr6Lv5FLyEjZnEIWH0XduuXJ/JBvklsvQd2ol0g3MjmKJHDIM2Im+UyuV7cCxEeyRO15E35mu8kwEe+SKaeg70Ue6ganBrZIjPkLfib7yXnCr5ISp6DsvlEwKbJtgpLyidYu2AgFJti2pTlg0Ab8DA7UVCcQeYATQqa1IMan2ALOpHeeDbD+bpa3EkUg1AC7SViACSbYp1QCYoa1ABKZrK3AkUvwGaEQ2WtTa+non0Awc1FbkcFLsAcZTe84HWcoeq61EMSkGQKu2AhFJrm0pBsBx2gpEZJi2AsWkGACDtBWISIu2AsWkGAD12gpEJLk9gykGQJe2AhE5oK1AMSkGQLu2AhH5R1uBYlIMgD+0FYhIcm1LMQB+0VYgIlu1FSgmxZnAAUhXmaJuPnQhbUtqRTDFHqADWKetRAS+JzHnQ5oBAHLyttZIsk2pBsDH2gpEIMk2pfqebUF2BDWW+mGV0AEMJ8Ehbqo9wF7koGWtsJgEnZ86M9HfzRtKktwMkjp1SKo2bef5ykrSfdUmz0XoO9BXzg9ulZyxFH0nusriCPbIHa3A3+g7s1JpA06IYI9ccj36Dq1UroliiRyzCH2nlitPRLJBrukHfIK+c0vJB1iiqGi0kPbQcBW1daQtSQYjhtZ2drF8RoKbPmuVZuBd9J1+SN7CsohnTgFJHdeNnuO7kJRwqa6plCSVKcrhwBTgZOQ2jxZkB+0rwNclyp4HPEf2x642ATcBn5f43VnIkLAemRvYgdw98A2wK6aCqTMJeBT4kaP/l71A6dNCzcAjZJM5vB14kNJd/nDg5RJ1/QAsBM4oUVfNUAdcjHwwVWL0XcC1lO6xRgKPIcvJoR3fhgTsiDLaeB3wZ4X1LwcuKFF3VTMWGSP7OGEpcGIZzxqIdM8fIvvwXJ+3H3gfuAHpZUrRimQF82njMmBMGc+qKq4i3Hz+XiTpUrkfXs3IquIDwOvIPMI2pCvv6pX23r+tBl5DPuwuRHbxlkMBuU8oVBvbgCvLfHby3EP47rgHWEEa6demIGv+Mdp4R4btiMJ9xDHMIekCnkXuAsya8cDzxB+GVm0QzCGuYYoD4U1k80XMMXkBeTW83fvMrNp3dcQ2ReFM4F+yM9DhshkZEk4mzDxHHdLNLwB+VmpTB5FedzEmgpqAr4DTItRdKVuQ/fgrgDXAekof0W4ATkUmcGYgH5DljDxisxY4GxmZBCNGACwA7o1QbwgOIgc0tyN3Be/r/XsTMASZR2gl3SRVDyHT38EIHQDjkJm95DJh1Aj7kd5pS6gKQ0f6U8j70ohDPXILSbBDMyF7gInIe8qISzdwOoFOUIccMt0VsC6jbwrAnaEqC9UDjEQye9Ryhq+U6ES2nHsvJ4fqAeZizs+SfsgClTehJkrWIZs5jOz4jgCTQyF6gMmY8zWYiHwMehEiAK4IUIfhxuW+FYQIgEsD1GG4cYlvBb7fAC3IlGrV7oqtcg4g5yQ6XCvwddz0AHUY7jQA5/pU4Ou8Wrzbp9pQDYBpnuUNf7x84PMNUIckP7Yr0nXZCRzvWtinBxiNOT8FRlD6rEKf+ATARI+yRlicTxb5BMAEj7JGWJy33/kEgE3/poOzL3wC4CSPskZYnE9G+wTAGI+yRlicL6T0CYBRHmWNsIx2Leg6D9BAgrdf5JhuxCfdlRZ07QGGOJYz4lDAMUGVawBYNqz0yDQAavl+32rFKT+hawCUmzjByI5MA6C/YzkjHk73K7kGgJ39Sw8nn7gGQKqnZ/OMk09sO1ft4DSn4xoANgmUHk6JI1wDoM2xnBGPPS6FXAPgV8dyRjycfOK6FlBAEjaWkz3TiM9uYCiSUKoiXHuAbiQRlJEGa3BwPviNAj71KGuEZblrQZ8AWOJR1giLsy98AuBb7DWQAl8AP7kW9gmAHiR/vqHLQs2HF5AU6xrpU03k4g31a38mopcXOM/SQRrpeAG4EX2D5E2Su5v4bvSNkhe5vUyfZM58JCGztoFqVTqBeWV7Q4npwEb0jVVrsh44pwI/qNKEXBezB33DVbv8hbxeq3Ib3iDkhq+V6F7xWm3ShVxyMZ/It5FnOYYcirwepiIXLo1CDpj0z1iPlOhBhtC7keXcDcjs6irkP98wDMMwDMMwDMMwDMMwwvAfy+U+r3pF1RwAAAAASUVORK5CYII=";
    public static String IMPS_ACTIVE = "";
    public static String API_BASE_URL = "";
    public static String PUBLIC_KEY = "";
    public static String PRIVATE_KEY = "";
    public static String SECRET_KEY = "";
    public static String AUTH_KEY = "";
    public static String CONNECTION_ID = "";
    public static String BANK_ID = "";
    public static String VERSION_NUMBER = "";
    public static String DEVICE_ID = "";
    public static String DEVICE_TYPE = "";
    public static String APP_VERSION_NUMBER = "";
    public static String IP = "";
    public static String MOBILE_NUMBER = "";
    public static String SESSION = "";
    public static String CHECK_NR_TIME = "";
    public static String NEFT_START_TIME = "";
    public static String NEFT_END_TIME = "";
    public static String RTGS_START_TIME = "";
    public static String RTGS_END_TIME = "";
    public static String CUST_ID = "";

    /*
     * User Profile
     * */
    public static String SecurityQuestionExist = "";
    public static String REMOVE_REGISTRATION_KEY = "";
    public static String TERMS_CONDITION = "";
    public static String USER_NAME = "";
    public static String EMAIL_ID = "";
    public static String USER_PHOTO = "";
    public static String USER_ADDRESS = "";
    public static String USER_PAN_NO = "";
    public static String CURRENT_BALANCE = "";
    /*
     * Account Details
     * */
    public static String ACCOUNT_BANK_ID = "";
    public static String ACTYP = "";
    public static String ACC_CODE = "";
    public static String ACCOUNT_CUR_BAL = "";
    public static String BRANCH_CODE = "";
    public static String ACCOUNT_NUMBER = "";
    public static int RTGS = 0;
    public static int IMPS = 0;
    public static int INTERNAL = 0;
    public static int NEFT = 0;
    public static int PIN_VERIFY = 0;
    public static String TRANSACTION_TYPE = "";
    public static String FCM_KEY = "";

    /*
     * V3 CHANGES
     * */
    public static String SECURITY_QUESTIONS_EXITS = "";
    public static String SECURITY_QUESTIONS_VERIFIED = "";
    public static String IS_LOGIN = "";
    public static String SOURCE_FROM = "";

    public static String BENE_ID = "";
    public static String UserUnBlockTime = "";
    public static boolean IsUserBlock = false;
    public static String DEVICE_NAME = "";
    public static Boolean NeedPasswordReset = false;
    public static String DialogeText = "";

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    public static String encodeTobase64(Bitmap image) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);

    }


    public static void TransmitFragment(Fragment fragment, FragmentManager fragmentManager, int fragmentID, boolean isBackStack, Bundle bundle) {

        if (bundle == null) {
            if (!isBackStack) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(fragmentID, fragment);
                fragmentTransaction.commit();
            } else {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(fragmentID, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        } else {
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(fragmentID, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }
}
