package com.amcblanand.android.extra;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by rakshit.sathwara on 7/26/2017.
 */

public class Progress {
    private ProgressDialog progressDialog;
    private Context context;

    public Progress(Context context) {
        this.context = context;
    }

    public void createDialog(boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(cancelable);
    }

    public void dialogMessage(String msg) {
        progressDialog.setMessage(msg);
    }

    public void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    public void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
