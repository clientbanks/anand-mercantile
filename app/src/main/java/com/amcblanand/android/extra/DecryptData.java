package com.amcblanand.android.extra;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * Created by rakshit.sathwara on 7/19/2017.
 */

public class DecryptData {
    private static final String APPTAG = Constants.TAG + DecryptData.class.getSimpleName();
    CryptLib cryptLib;
    private String responseData = "", publicKey;

    public String parseResponse(String data) {
        String finalStringResponse = "";
        try {
            cryptLib = new CryptLib();
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new InputSource(new StringReader(data)));
            Element element = doc.getDocumentElement();
            element.normalize();
            NodeList nList = doc.getElementsByTagName("anyType");
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                responseData = node.getTextContent().substring(1, node.getTextContent().length() - 1);
            }
            publicKey = CryptLib.SHA256(Constants.PUBLIC_KEY, 32);
            String firstDec = cryptLib.decrypt(responseData, publicKey, Constants.SECRET_KEY);
            finalStringResponse = cryptLib.decrypt(firstDec, publicKey, Constants.PRIVATE_KEY);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return finalStringResponse;
    }


    public String parseResponseRemoveRegistration(String data) {
        String finalStringResponse = "";
        try {
            cryptLib = new CryptLib();
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new InputSource(new StringReader(data)));
            Element element = doc.getDocumentElement();
            element.normalize();
            NodeList nList = doc.getElementsByTagName("anyType");
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                responseData = node.getTextContent().substring(1, node.getTextContent().length() - 1);
            }
            publicKey = CryptLib.SHA256(Constants.REMOVE_REGISTRATION_KEY, 32);
            String firstDec = cryptLib.decrypt(responseData, publicKey, Constants.SECRET_KEY);
            finalStringResponse = cryptLib.decrypt(firstDec, publicKey, Constants.PRIVATE_KEY);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return finalStringResponse;
    }

}
