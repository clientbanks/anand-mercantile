package com.amcblanand.android.extra;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.multidex.MultiDex;
import android.util.Log;

import com.amcblanand.android.BuildConfig;

import timber.log.Timber;


/**
 * Created by rakshit.sathwara on 11/10/2017.
 */

public class AMCBLBank extends Application {
	public static final String TAG = Constants.TAG + AMCBLBank.class.getName();
	private static AMCBLBank mInstance;

	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
		if (BuildConfig.DEBUG) {
			Timber.plant(new Timber.DebugTree());
		} else {
			Timber.plant(new CrashReportingTree());
		}
	}

	public static synchronized AMCBLBank getInstance() {
		return mInstance;
	}

	public static boolean hasNetwork() {
		return mInstance.checkIfHasNetwork();
	}

	public boolean checkIfHasNetwork() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		return networkInfo != null && networkInfo.isConnected();
	}

	private static class CrashReportingTree extends Timber.Tree {
		@Override
		protected void log(int priority, String tag, String message, Throwable t) {
			if (priority == Log.VERBOSE || priority == Log.DEBUG) {
				return;
			}

			FakeCrashLibrary.log(priority, tag, message);

			if (t != null) {
				if (priority == Log.ERROR) {
					FakeCrashLibrary.logError(t);
				} else if (priority == Log.WARN) {
					FakeCrashLibrary.logWarning(t);
				}
			}
		}
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

}
