package com.amcblanand.android.extra;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.amcblanand.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomToast extends Toast {
    @BindView(R.id.tv_toast)
    TextView tvToast;

    /**
     * Construct an empty Toast object.  You must call {@link #setView} before you
     * can call {@link #show}.
     *
     * @param context The context to use.  Usually your {@link Application}
     *                or {@link Activity} object.
     */
    public CustomToast(Context context) {
        super(context);
    }

    public CustomToast(Context context, Activity activity, String message) {
        super(context);
        LayoutInflater layoutInflater = activity.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.custom_toast_layout, null);
        ButterKnife.bind(this, view);


        tvToast.setText(message);
        setGravity(Gravity.BOTTOM, 0, 10);
        setDuration(Toast.LENGTH_SHORT);
        setView(view);
    }
}
