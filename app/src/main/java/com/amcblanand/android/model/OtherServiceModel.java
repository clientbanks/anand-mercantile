package com.amcblanand.android.model;

public class OtherServiceModel {

    private String serviceName;
    private int serviceImg;
    private int serviceColor;

    public OtherServiceModel(String serviceName, int serviceImg, int serviceColor) {
        this.serviceName = serviceName;
        this.serviceImg = serviceImg;
        this.serviceColor = serviceColor;
    }


    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getServiceImg() {
        return serviceImg;
    }

    public void setServiceImg(int serviceImg) {
        this.serviceImg = serviceImg;
    }

    public int getServiceColor() {
        return serviceColor;
    }

    public void setServiceColor(int serviceColor) {
        this.serviceColor = serviceColor;
    }
}
