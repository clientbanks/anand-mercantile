package com.amcblanand.android.model;

/**
 * Created by rakshit.sathwara on 8/21/2017.
 */

public class MiniStatement {
    String BeneName, Status, TranAmount, TranIndicator, TranDateTime, TransRefNo, TranType;

    public String getBeneName() {
        return BeneName;
    }

    public void setBeneName(String beneName) {
        BeneName = beneName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getTranAmount() {
        return TranAmount;
    }

    public void setTranAmount(String tranAmount) {
        TranAmount = tranAmount;
    }

    public String getTranIndicator() {
        return TranIndicator;
    }

    public void setTranIndicator(String tranIndicator) {
        TranIndicator = tranIndicator;
    }

    public String getTranDateTime() {
        return TranDateTime;
    }

    public void setTranDateTime(String tranDateTime) {
        TranDateTime = tranDateTime;
    }

    public String getTranType() {
        return TranType;
    }

    public void setTranType(String tranType) {
        TranType = tranType;
    }

    public String getTransRefNo() {
        return TransRefNo;
    }

    public void setTransRefNo(String transRefNo) {
        TransRefNo = transRefNo;
    }
}
