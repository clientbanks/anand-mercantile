package com.amcblanand.android.model;

/**
 * Created by rakshit.sathwara on 1/25/2018.
 */

public class BannerListModel {

    private String PATH;

    public String getPATH() {
        return PATH;
    }

    public void setPATH(String PATH) {
        this.PATH = PATH;
    }
}
