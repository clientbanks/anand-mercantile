package com.amcblanand.android.model;

/**
 * Created by rakshit.sathwara on 8/31/2017.
 */

public class FeedbackPojo {
    private String SRNO, SUBJECT;

    public String getSRNO() {
        return SRNO;
    }

    public void setSRNO(String SRNO) {
        this.SRNO = SRNO;
    }

    public String getSUBJECT() {
        return SUBJECT;
    }

    public void setSUBJECT(String SUBJECT) {
        this.SUBJECT = SUBJECT;
    }
}
