package com.amcblanand.android.model;


/**
 * Created by rakshit.sathwara on 8/1/2017.
 */

public class PayeeListPojo {
    String Bene_Ac_No, Bene_Name, Bene_Code, Bene_Bank, Bene_Brach, Beneficiary_ID, NAME, ACDESC, BRANCH_NAME, BANK_NAME, STATE_BRANCH_NAME,
            IFSCCODE, Bene_Status, beneEmailID, beneMobileNumber;

    public String getBene_Ac_No() {
        return Bene_Ac_No;
    }

    public void setBene_Ac_No(String bene_Ac_No) {
        Bene_Ac_No = bene_Ac_No;
    }

    public String getBene_Name() {
        return Bene_Name;
    }

    public void setBene_Name(String bene_Name) {
        Bene_Name = bene_Name;
    }

    public String getBene_Code() {
        return Bene_Code;
    }

    public void setBene_Code(String bene_Code) {
        Bene_Code = bene_Code;
    }

    public String getBene_Bank() {
        return Bene_Bank;
    }

    public void setBene_Bank(String bene_Bank) {
        Bene_Bank = bene_Bank;
    }

    public String getBene_Brach() {
        return Bene_Brach;
    }

    public void setBene_Brach(String bene_Brach) {
        Bene_Brach = bene_Brach;
    }

    public String getBeneficiary_ID() {
        return Beneficiary_ID;
    }

    public void setBeneficiary_ID(String beneficiary_ID) {
        Beneficiary_ID = beneficiary_ID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getACDESC() {
        return ACDESC;
    }

    public void setACDESC(String ACDESC) {
        this.ACDESC = ACDESC;
    }

    public String getBRANCH_NAME() {
        return BRANCH_NAME;
    }

    public void setBRANCH_NAME(String BRANCH_NAME) {
        this.BRANCH_NAME = BRANCH_NAME;
    }

    public String getBANK_NAME() {
        return BANK_NAME;
    }

    public void setBANK_NAME(String BANK_NAME) {
        this.BANK_NAME = BANK_NAME;
    }

    public String getSTATE_BRANCH_NAME() {
        return STATE_BRANCH_NAME;
    }

    public void setSTATE_BRANCH_NAME(String STATE_BRANCH_NAME) {
        this.STATE_BRANCH_NAME = STATE_BRANCH_NAME;
    }

    public String getIFSCCODE() {
        return IFSCCODE;
    }

    public void setIFSCCODE(String IFSCCODE) {
        this.IFSCCODE = IFSCCODE;
    }

    public String getBene_Status() {
        return Bene_Status;
    }

    public void setBene_Status(String bene_Status) {
        Bene_Status = bene_Status;
    }

    public String getBeneEmailID() {
        return beneEmailID;
    }

    public void setBeneEmailID(String beneEmailID) {
        this.beneEmailID = beneEmailID;
    }

    public String getBeneMobileNumber() {
        return beneMobileNumber;
    }

    public void setBeneMobileNumber(String beneMobileNumber) {
        this.beneMobileNumber = beneMobileNumber;
    }
}
