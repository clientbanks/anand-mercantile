package com.amcblanand.android.model;

public class HistoryModel {
    private String TDT, TDATE, PAYMENT_TYPE, BENE_DETAILS, AMOUNT;

    public String getTDT() {
        return TDT;
    }

    public void setTDT(String TDT) {
        this.TDT = TDT;
    }

    public String getTDATE() {
        return TDATE;
    }

    public void setTDATE(String TDATE) {
        this.TDATE = TDATE;
    }

    public String getPAYMENT_TYPE() {
        return PAYMENT_TYPE;
    }

    public void setPAYMENT_TYPE(String PAYMENT_TYPE) {
        this.PAYMENT_TYPE = PAYMENT_TYPE;
    }

    public String getBENE_DETAILS() {
        return BENE_DETAILS;
    }

    public void setBENE_DETAILS(String BENE_DETAILS) {
        this.BENE_DETAILS = BENE_DETAILS;
    }

    public String getAMOUNT() {
        return AMOUNT;
    }

    public void setAMOUNT(String AMOUNT) {
        this.AMOUNT = AMOUNT;
    }
}
