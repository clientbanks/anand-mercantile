package com.amcblanand.android.model;

/**
 * Created by amit.singh on 12/31/2017.
 */

public class ChequebookStatusPojo {
    private String USER_ID, ECS_ACC_CODE, REQ_DATE, REQ_TIME, CHQ_BK_LEAF, STATUS;

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getECS_ACC_CODE() {
        return ECS_ACC_CODE;
    }

    public void setECS_ACC_CODE(String ECS_ACC_CODE) {
        this.ECS_ACC_CODE = ECS_ACC_CODE;
    }

    public String getREQ_DATE() {
        return REQ_DATE;
    }

    public void setREQ_DATE(String REQ_DATE) {
        this.REQ_DATE = REQ_DATE;
    }

    public String getREQ_TIME() {
        return REQ_TIME;
    }

    public void setREQ_TIME(String REQ_TIME) {
        this.REQ_TIME = REQ_TIME;
    }

    public String getCHQ_BK_LEAF() {
        return CHQ_BK_LEAF;
    }

    public void setCHQ_BK_LEAF(String CHQ_BK_LEAF) {
        this.CHQ_BK_LEAF = CHQ_BK_LEAF;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
