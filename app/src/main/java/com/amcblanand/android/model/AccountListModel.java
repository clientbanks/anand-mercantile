package com.amcblanand.android.model;

/**
 * Created by rakshit.sathwara on 7/29/2017.
 */

public class AccountListModel {
    String BankId, BranchCode, Actyp, AccCode, CurBal, Name, Gender, Acno, Email;

    public String getBankId() {
        return BankId;
    }

    public void setBankId(String bankId) {
        BankId = bankId;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(String branchCode) {
        BranchCode = branchCode;
    }

    public String getActyp() {
        return Actyp;
    }

    public void setActyp(String actyp) {
        Actyp = actyp;
    }

    public String getAccCode() {
        return AccCode;
    }

    public void setAccCode(String accCode) {
        AccCode = accCode;
    }

    public String getCurBal() {
        return CurBal;
    }

    public void setCurBal(String curBal) {
        CurBal = curBal;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getAcno() {
        return Acno;
    }

    public void setAcno(String acno) {
        Acno = acno;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}
