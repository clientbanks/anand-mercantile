package com.amcblanand.android.model;

/**
 * Created by rakshit.sathwara on 9/1/2017.
 */

public class ContactusPojo {
    private String BANK_NAME, BRANCH_NAME, BR_ADD1, BR_ADD2, BR_ADD3, BR_CITY, BR_PIN, BR_TEL_NO, IFSC_CODE, FAX, EMAIL_ID, WEB_URL, BAL_ENQ_NO, ATM_BLOCK_NO;

    public String getBANK_NAME() {
        return BANK_NAME;
    }

    public void setBANK_NAME(String BANK_NAME) {
        this.BANK_NAME = BANK_NAME;
    }

    public String getBRANCH_NAME() {
        return BRANCH_NAME;
    }

    public void setBRANCH_NAME(String BRANCH_NAME) {
        this.BRANCH_NAME = BRANCH_NAME;
    }

    public String getBR_ADD1() {
        return BR_ADD1;
    }

    public void setBR_ADD1(String BR_ADD1) {
        this.BR_ADD1 = BR_ADD1;
    }

    public String getBR_ADD2() {
        return BR_ADD2;
    }

    public void setBR_ADD2(String BR_ADD2) {
        this.BR_ADD2 = BR_ADD2;
    }

    public String getBR_ADD3() {
        return BR_ADD3;
    }

    public void setBR_ADD3(String BR_ADD3) {
        this.BR_ADD3 = BR_ADD3;
    }

    public String getBR_CITY() {
        return BR_CITY;
    }

    public void setBR_CITY(String BR_CITY) {
        this.BR_CITY = BR_CITY;
    }

    public String getBR_PIN() {
        return BR_PIN;
    }

    public void setBR_PIN(String BR_PIN) {
        this.BR_PIN = BR_PIN;
    }

    public String getBR_TEL_NO() {
        return BR_TEL_NO;
    }

    public void setBR_TEL_NO(String BR_TEL_NO) {
        this.BR_TEL_NO = BR_TEL_NO;
    }

    public String getIFSC_CODE() {
        return IFSC_CODE;
    }

    public void setIFSC_CODE(String IFSC_CODE) {
        this.IFSC_CODE = IFSC_CODE;
    }

    public String getFAX() {
        return FAX;
    }

    public void setFAX(String FAX) {
        this.FAX = FAX;
    }

    public String getEMAIL_ID() {
        return EMAIL_ID;
    }

    public void setEMAIL_ID(String EMAIL_ID) {
        this.EMAIL_ID = EMAIL_ID;
    }

    public String getWEB_URL() {
        return WEB_URL;
    }

    public void setWEB_URL(String WEB_URL) {
        this.WEB_URL = WEB_URL;
    }

    public String getBAL_ENQ_NO() {
        return BAL_ENQ_NO;
    }

    public void setBAL_ENQ_NO(String BAL_ENQ_NO) {
        this.BAL_ENQ_NO = BAL_ENQ_NO;
    }

    public String getATM_BLOCK_NO() {
        return ATM_BLOCK_NO;
    }

    public void setATM_BLOCK_NO(String ATM_BLOCK_NO) {
        this.ATM_BLOCK_NO = ATM_BLOCK_NO;
    }
}
