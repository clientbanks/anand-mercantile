package com.amcblanand.android.model;

/**
 * Created by rakshit.sathwara on 8/1/2017.
 */

public class BankListPojo {
    String Text, Value;

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
