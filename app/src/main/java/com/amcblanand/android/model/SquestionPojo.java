package com.amcblanand.android.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amit.singh on 07-03-2018.
 */

public class SquestionPojo {


    /**
     * Status : 1
     * ResponseJSON : [{"SRNO":1,"QUESTION":"In which city were you born?","GROUP_CD":1},{"SRNO":2,"QUESTION":"What is the name of your first school?","GROUP_CD":1},{"SRNO":3,"QUESTION":"What was your childhood nickname?","GROUP_CD":1},{"SRNO":4,"QUESTION":"What is the name of your favorite childhood friend?","GROUP_CD":1},{"SRNO":5,"QUESTION":"What was your favorite place to visit as a child?","GROUP_CD":1},{"SRNO":6,"QUESTION":"Who was your childhood hero?","GROUP_CD":1},{"SRNO":7,"QUESTION":"What is the name of your favorite Movie?","GROUP_CD":1},{"SRNO":8,"QUESTION":"Who is your favorite actor?","GROUP_CD":1},{"SRNO":9,"QUESTION":"What is the name of your favorite Singer?","GROUP_CD":1},{"SRNO":10,"QUESTION":"In which town was your first job?","GROUP_CD":1},{"SRNO":11,"QUESTION":"What was the name of the company where you had your first job?","GROUP_CD":2},{"SRNO":12,"QUESTION":"What was your designation for your first job?","GROUP_CD":2},{"SRNO":13,"QUESTION":"In which city were your father born?","GROUP_CD":2},{"SRNO":14,"QUESTION":"In which city were your mother born?","GROUP_CD":2},{"SRNO":15,"QUESTION":"In which city were your grandfather born?","GROUP_CD":2},{"SRNO":16,"QUESTION":"What is your father maiden name?","GROUP_CD":2},{"SRNO":17,"QUESTION":"What is your mother maiden name?","GROUP_CD":2},{"SRNO":18,"QUESTION":"What is your grandmother (on your father side) maiden name?","GROUP_CD":2},{"SRNO":19,"QUESTION":"What is your favorite web browser?","GROUP_CD":2},{"SRNO":20,"QUESTION":"What is the name of your favorite web search engine?","GROUP_CD":2},{"SRNO":21,"QUESTION":"What is the name of your favorite social media website?","GROUP_CD":3},{"SRNO":22,"QUESTION":"What is the name of your favorite sports?","GROUP_CD":3},{"SRNO":23,"QUESTION":"What is the name of your favorite sports team?","GROUP_CD":3},{"SRNO":24,"QUESTION":"Who is your favorite sports person?","GROUP_CD":3},{"SRNO":25,"QUESTION":"What is the name of your favorite color?","GROUP_CD":3},{"SRNO":26,"QUESTION":"What is the name of your favorite fruit?","GROUP_CD":3},{"SRNO":27,"QUESTION":"What is the name of your favorite chocolate?","GROUP_CD":3},{"SRNO":28,"QUESTION":"What was the name of your first mobile?","GROUP_CD":3},{"SRNO":29,"QUESTION":"What was the name of your first car?","GROUP_CD":3},{"SRNO":30,"QUESTION":"What was the name of your first bike?","GROUP_CD":3}]
     * ResponseMessage :
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("ResponseMessage")
    private String ResponseMessage;
    @SerializedName("ResponseJSON")
    private List<ResponseJSONBean> ResponseJSON;

    public static SquestionPojo objectFromData(String str) {

        return new Gson().fromJson(str, SquestionPojo.class);
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String ResponseMessage) {
        this.ResponseMessage = ResponseMessage;
    }

    public List<ResponseJSONBean> getResponseJSON() {
        return ResponseJSON;
    }

    public void setResponseJSON(List<ResponseJSONBean> ResponseJSON) {
        this.ResponseJSON = ResponseJSON;
    }

    public static class ResponseJSONBean {
        /**
         * SRNO : 1
         * QUESTION : In which city were you born?
         * GROUP_CD : 1
         */

        @SerializedName("SRNO")
        private int SRNO;
        @SerializedName("QUESTION")
        private String QUESTION;
        @SerializedName("GROUP_CD")
        private int GROUPCD;


        public static ResponseJSONBean objectFromData(String str) {

            return new Gson().fromJson(str, ResponseJSONBean.class);
        }

        public int getSRNO() {
            return SRNO;
        }

        public void setSRNO(int SRNO) {
            this.SRNO = SRNO;
        }

        public String getQUESTION() {
            return QUESTION;
        }

        public void setQUESTION(String QUESTION) {
            this.QUESTION = QUESTION;
        }

        public int getGROUPCD() {
            return GROUPCD;
        }

        public void setGROUPCD(int GROUPCD) {
            this.GROUPCD = GROUPCD;
        }

    }
}
