package com.amcblanand.android.model;

/**
 * Created by amit.singh on 07-03-2018.
 */

public class SQuestions {

    private int SRNO;
    private String QUESTION;

    private String isError;

    public SQuestions(int SRNO, String QUESTION, String isError) {
        this.SRNO = SRNO;
        this.QUESTION = QUESTION;
        this.isError = isError;
    }

    public int getSRNO() {
        return SRNO;
    }

    public void setSRNO(int SRNO) {
        this.SRNO = SRNO;
    }

    public String getQUESTION() {
        return QUESTION;
    }

    public void setQUESTION(String QUESTION) {
        this.QUESTION = QUESTION;
    }

    public String getIsError() {
        return isError;
    }

    public void setIsError(String isError) {
        this.isError = isError;
    }
}
