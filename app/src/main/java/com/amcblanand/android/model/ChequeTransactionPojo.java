package com.amcblanand.android.model;

/**
 * Created by rakshit.sathwara on 7/31/2017.
 */

public class ChequeTransactionPojo {

    public String Trandate, Descrip, Amount;

    public String getTrandate() {
        return Trandate;
    }

    public void setTrandate(String trandate) {
        Trandate = trandate;
    }

    public String getDescrip() {
        return Descrip;
    }

    public void setDescrip(String descrip) {
        Descrip = descrip;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }
}
