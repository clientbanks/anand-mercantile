package com.amcblanand.android.model;

/**
 * Created by amit.singh on 12/1/2017.
 */

public class DebitCardPojo {
    private String ATM_YN, ATM_CARD_SRNO, ATM_ISSUE_DT, ATM_EXPIRE_DT;

    public String getATM_YN() {
        return ATM_YN;
    }

    public void setATM_YN(String ATM_YN) {
        this.ATM_YN = ATM_YN;
    }

    public String getATM_CARD_SRNO() {
        return ATM_CARD_SRNO;
    }

    public void setATM_CARD_SRNO(String ATM_CARD_SRNO) {
        this.ATM_CARD_SRNO = ATM_CARD_SRNO;
    }

    public String getATM_ISSUE_DT() {
        return ATM_ISSUE_DT;
    }

    public void setATM_ISSUE_DT(String ATM_ISSUE_DT) {
        this.ATM_ISSUE_DT = ATM_ISSUE_DT;
    }

    public String getATM_EXPIRE_DT() {
        return ATM_EXPIRE_DT;
    }

    public void setATM_EXPIRE_DT(String ATM_EXPIRE_DT) {
        this.ATM_EXPIRE_DT = ATM_EXPIRE_DT;
    }
}
