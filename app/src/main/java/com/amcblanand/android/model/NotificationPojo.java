package com.amcblanand.android.model;

/**
 * Created by amit.singh on 16-09-2017.
 */

public class NotificationPojo {

    private String Remittance,Beneficiary,Amount,Tdate,Ttime,Time,Trantype;

    public String getRemittance() {
        return Remittance;
    }

    public void setRemittance(String remittance) {
        Remittance = remittance;
    }

    public String getBeneficiary() {
        return Beneficiary;
    }

    public void setBeneficiary(String beneficiary) {
        Beneficiary = beneficiary;
    }

    public String getTdate() {
        return Tdate;
    }

    public void setTdate(String tdate) {
        Tdate = tdate;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getTtime() {
        return Ttime;
    }

    public void setTtime(String ttime) {
        Ttime = ttime;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getTrantype() {
        return Trantype;
    }

    public void setTrantype(String trantype) {
        Trantype = trantype;
    }
}
