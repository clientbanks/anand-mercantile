package com.amcblanand.android.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.fragment.AccountsFragment;
import com.amcblanand.android.fragment.ChangePINFragment;
import com.amcblanand.android.fragment.ContactUsFragment;
import com.amcblanand.android.fragment.DebitCardRequestFragment;
import com.amcblanand.android.fragment.FeedbackFragment;
import com.amcblanand.android.fragment.FundTransferFragment;
import com.amcblanand.android.fragment.HomeFragment;
import com.amcblanand.android.fragment.IMPSMoneyTransferFragment;
import com.amcblanand.android.fragment.ManagePayeeFragment;
import com.amcblanand.android.fragment.MyMMIDFragment;
import com.amcblanand.android.fragment.NotificationFragment;
import com.amcblanand.android.fragment.ServiceFragment;
import com.amcblanand.android.fragment.UserProfileFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = Constants.TAG + DashboardActivity.class.getSimpleName();

    private TextView tvUserName, tvUserEmailID;
    private ImageView imageView;
    Toolbar toolbar;
    private DecryptData decryptData;
    private Progress progress;
    private Fragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                DashboardActivity.this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(DashboardActivity.this);
        tvUserName = navigationView.findViewById(R.id.tv_user_name_header);
        tvUserEmailID = navigationView.findViewById(R.id.tv_user_email_id_header);
        imageView = navigationView.findViewById(R.id.imageView);

        navigationView.setItemIconTintList(null);

        long pmillis = new java.util.Date().getTime();
        Constants.SESSION = pmillis + "";
        checkTimeout();

        decryptData = new DecryptData();
        progress = new Progress(DashboardActivity.this);

//            Constants.TransmitFragment(new HomeFragment(), getSupportFragmentManager(), R.id.frame_container, false, null);
        fragment = new HomeFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_container, fragment);
        ft.commit();

        progress.createDialog(false);
        progress.dialogMessage("Please wait..");
        progress.showDialog();
        GetProfileDetails();

//        TedPermission.with(DashboardActivity.this)
//                .setPermissionListener(permissionListener)
//                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
//                .setPermissions(Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.CAMERA)
//                .check();

    }
//
//    PermissionListener permissionListener = new PermissionListener() {
//        @Override
//        public void onPermissionGranted() {
//
//        }
//
//        @Override
//        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
//
//        }
//    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fm = getSupportFragmentManager();
            if (fm.getBackStackEntryCount() > 0) {
                fm.popBackStack();
            } else {
                final Dialog dialog = new Dialog(DashboardActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setCancelable(true);
                dialog.setContentView(R.layout.logout_dialog);
                dialog.show();

                Button declineButton = (Button) dialog.findViewById(R.id.btn_ok);
                // if decline button is clicked, close the custom dialog
                declineButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Close dialog
                        dialog.dismiss();
                        finish();
                        int pid = android.os.Process.myPid();
                        android.os.Process.killProcess(pid);
                        System.exit(0);

                    }
                });

                Button cancel = (Button) dialog.findViewById(R.id.btn_cancel);
                // if decline button is clicked, close the custom dialog
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Close dialog
                        dialog.dismiss();
                    }
                });
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            fragment = new NotificationFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame_container, fragment);
            ft.addToBackStack(null);
            ft.commit();
            return true;
        } else if (id == R.id.action_logout) {

            final Dialog dialog = new Dialog(DashboardActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setCancelable(true);
            dialog.setContentView(R.layout.logout_dialog);
            dialog.show();

            Button declineButton = (Button) dialog.findViewById(R.id.btn_ok);
            // if decline button is clicked, close the custom dialog
            declineButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Close dialog
                    dialog.dismiss();
                    finish();
                    int pid = android.os.Process.myPid();
                    android.os.Process.killProcess(pid);
                    System.exit(0);

                }
            });

            Button cancel = (Button) dialog.findViewById(R.id.btn_cancel);
            // if decline button is clicked, close the custom dialog
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Close dialog
                    dialog.dismiss();
                }
            });

//            finish();
//            Intent i = new Intent(this, SplashActivity.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(i);
//            int pid = android.os.Process.myPid();
//            android.os.Process.killProcess(pid);
//            System.exit(0);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Constants.TransmitFragment(new HomeFragment(), getSupportFragmentManager(), R.id.frame_container, true, null);
//            fragment = new HomeFragment();
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.frame_container, fragment);
//            ft.addToBackStack(null);
//            ft.commit();
        } else if (id == R.id.nav_accounts) {
            Constants.TransmitFragment(new AccountsFragment(), getSupportFragmentManager(), R.id.frame_container, true, null);
//            fragment = new AccountsFragment();
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.frame_container, fragment);
//            ft.addToBackStack(null);
//            ft.commit();
        } else if (id == R.id.nav_fund_transfer) {
            Constants.TransmitFragment(new FundTransferFragment(), getSupportFragmentManager(), R.id.frame_container, true, null);
//            fragment = new FundTransferFragment();
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.frame_container, fragment);
//            ft.addToBackStack(null);
//            ft.commit();
        } else if (id == R.id.nav_my_mmid) {
            Constants.TransmitFragment(new MyMMIDFragment(), getSupportFragmentManager(), R.id.frame_container, true, null);
//            fragment = new MyMMIDFragment();
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.frame_container, fragment);
//            ft.addToBackStack(null);
//            ft.commit();
        } else if (id == R.id.nav_branch_locator) {
            Constants.TransmitFragment(new ContactUsFragment(), getSupportFragmentManager(), R.id.frame_container, true, null);
//            fragment = new ContactUsFragment();
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.frame_container, fragment);
//            ft.addToBackStack(null);
//            ft.commit();
        } else if (id == R.id.nav_bhim_upi) {
            Toast.makeText(this, "Coming soon.", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_bill_payment) {

            if (Constants.IMPS_ACTIVE.equals("Y")) {
                Constants.TransmitFragment(new IMPSMoneyTransferFragment(), getSupportFragmentManager(), R.id.frame_container, true, null);
            } else {
                Toast.makeText(DashboardActivity.this, "Coming soon.", Toast.LENGTH_SHORT).show();
            }


        } else if (id == R.id.nav_card_service) {
            Constants.TransmitFragment(new DebitCardRequestFragment(), getSupportFragmentManager(), R.id.frame_container, true, null);
        } else if (id == R.id.nav_manage_payee) {
            Constants.TransmitFragment(new ManagePayeeFragment(), getSupportFragmentManager(), R.id.frame_container, true, null);
//            fragment = new ManagePayeeFragment();
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.frame_container, fragment);
//            ft.addToBackStack(null);
//            ft.commit();
        } else if (id == R.id.nav_services) {
            Constants.TransmitFragment(new ServiceFragment(), getSupportFragmentManager(), R.id.frame_container, true, null);
//            fragment = new ServiceFragment();
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.frame_container, fragment);
//            ft.addToBackStack(null);
//            ft.commit();
        } else if (id == R.id.nav_remove_registration) {
            AlertDialog.Builder builder = new AlertDialog.Builder(DashboardActivity.this);
            builder.setTitle("Do you want to Remove Account?");
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    progress.createDialog(false);
                    progress.dialogMessage("Please wait..");
                    progress.showDialog();
                    RemoveRegistration();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else if (id == R.id.nav_change_pin) {
            Constants.TransmitFragment(new ChangePINFragment(), getSupportFragmentManager(), R.id.frame_container, true, null);
//            fragment = new ChangePINFragment();
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.frame_container, fragment);
//            ft.addToBackStack(null);
//            ft.commit();
        } else if (id == R.id.nav_feedback_form) {
            Constants.TransmitFragment(new FeedbackFragment(), getSupportFragmentManager(), R.id.frame_container, true, null);
//            fragment = new FeedbackFragment();
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.frame_container, fragment);
//            ft.addToBackStack(null);
//            ft.commit();
        } else if (id == R.id.nav_profile) {
            Constants.TransmitFragment(new UserProfileFragment(), getSupportFragmentManager(), R.id.frame_container, true, null);
//            fragment = new UserProfileFragment();
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.frame_container, fragment);
//            ft.addToBackStack(null);
//            ft.commit();
        } else if (id == R.id.nav_logout) {

            final Dialog dialog = new Dialog(DashboardActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setCancelable(true);
            dialog.setContentView(R.layout.logout_dialog);
            dialog.show();

            Button declineButton = (Button) dialog.findViewById(R.id.btn_ok);
            // if decline button is clicked, close the custom dialog
            declineButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Close dialog
                    dialog.dismiss();
                    finish();
                    int pid = android.os.Process.myPid();
                    android.os.Process.killProcess(pid);
                    System.exit(0);

                }
            });

            Button cancel = (Button) dialog.findViewById(R.id.btn_cancel);
            // if decline button is clicked, close the custom dialog
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Close dialog
                    dialog.dismiss();
                }
            });
//            finish();
//            Intent i = new Intent(this, SplashActivity.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(i);
//            int pid = android.os.Process.myPid();
//            android.os.Process.killProcess(pid);
//            System.exit(0);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void GetProfileDetails() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetProfileDetails");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);


            API.getInstance().encryptRequest(DashboardActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(DashboardActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);
                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        Constants.USER_NAME = jObj.getString("UName");
                                        Constants.EMAIL_ID = jObj.getString("Email");
                                        Constants.USER_PHOTO = jObj.getString("PHOTO");
                                    }
                                    if (Constants.USER_PHOTO.equals("") || Constants.USER_PHOTO.isEmpty()) {
//                                        ivUserProfile.setBackgroundResource(R.drawable.ic_user_icon);
                                    } else {
                                        byte[] imageByte = Base64.decode(Constants.USER_PHOTO, Base64.DEFAULT);
                                        Bitmap decodeImage = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
                                        imageView.setImageBitmap(decodeImage);
                                    }
                                    tvUserName.setText(Constants.USER_NAME);
                                    tvUserEmailID.setText(Constants.EMAIL_ID);
                                } else {
                                    Alert.ShowAlert(DashboardActivity.this, jsonObject.getString("ResponseMessage"));
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void RemoveRegistration() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "RemoveRegistration");

            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);

            String dataValue = reqJsonObject.toString();
            Timber.d(TAG + " - " + dataValue);

            API.getInstance().encryptRequest(DashboardActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(DashboardActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);

                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);

                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();
                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {

                                    progress.hideDialog();
                                    Intent i = new Intent(DashboardActivity.this, SplashActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    int pid = android.os.Process.myPid();
                                    android.os.Process.killProcess(pid);
                                    System.exit(0);
                                } else {
                                    progress.hideDialog();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Toast.makeText(DashboardActivity.this, "Something wrong! Please try after sometimes.", Toast.LENGTH_SHORT).show();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    void checkTimeout() {
        long cmillis = new java.util.Date().getTime();
        long pmillis = new java.util.Date().getTime();
        if (!Constants.SESSION.equals("")) {
            pmillis = Long.valueOf(Constants.SESSION);
        }

        long diff = cmillis - pmillis;
        Timber.e(diff + " TIME");
        if (diff > 1000000) {
            Constants.SESSION = "";
            finish();
            Intent i = new Intent(this, SplashActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);
            System.exit(0);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Timber.e("ON RESUME");
        checkTimeout();
    }

}
