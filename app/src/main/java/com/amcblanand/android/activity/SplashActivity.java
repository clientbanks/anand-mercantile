package com.amcblanand.android.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amcblanand.android.BuildConfig;
import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.NetworkUtil;
import com.amcblanand.android.extra.Progress;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = Constants.TAG + SplashActivity.class.getSimpleName();

    static {
        System.loadLibrary("native-lib");
    }

    @BindView(R.id.iv_splash)
    ImageView ivSplash;
    @BindView(R.id.tv_version)
    TextView tvVersion;

    private DecryptData decryptData;
    PermissionListener permissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {

            if (BuildConfig.DEBUG) {
                Constants.API_BASE_URL = stagingURLFromJNI();
                Constants.CONNECTION_ID = stagingConnectionIDFromJNI();
            } else {
                Constants.API_BASE_URL = liveURLFromJNI();
                Constants.CONNECTION_ID = connectionIDFromJNI();
            }

            Constants.PUBLIC_KEY = publicKeyFromJNI();
            Constants.PRIVATE_KEY = privateKeyFromJNI();
            Constants.SECRET_KEY = secretKeyFromJNI();
            Constants.AUTH_KEY = authKeyFromJNI();

            Constants.VERSION_NUMBER = apiVersionNumberFromJNI();
            Constants.BANK_ID = Constants.CONNECTION_ID;
            Constants.DEVICE_TYPE = deviceTypeFromJNI();
            Constants.APP_VERSION_NUMBER = appVersionNumberFromJNI();
            Constants.IP = Constants.getIPAddress(true);

            Constants.REMOVE_REGISTRATION_KEY = publicKeyFromJNI();

            decryptData = new DecryptData();
            final RotateAnimation rotate_ANIMATION = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate_ANIMATION.setDuration(2000);
            rotate_ANIMATION.setRepeatCount(Animation.INFINITE);
            rotate_ANIMATION.setInterpolator(new LinearInterpolator());
            ivSplash.startAnimation(rotate_ANIMATION);

            String deviceName = Constants.getDeviceName();
            Timber.e("DEVICE NAME -> " + deviceName);
            if (deviceName.equals("") || deviceName == null) {
                Constants.DEVICE_NAME = "NOT AVAILABLE";
            } else {
                Constants.DEVICE_NAME = deviceName;
            }

            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                tvVersion.setText("v" + Constants.VERSION_NUMBER + "." + pInfo.versionCode + "." + Constants.APP_VERSION_NUMBER);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                tvVersion.setText("v" + Constants.VERSION_NUMBER + "." + pInfo.versionCode + "." + Constants.APP_VERSION_NUMBER);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if (telephonyManager != null) {
                    Constants.DEVICE_ID = telephonyManager.getDeviceId();
                    if (Constants.DEVICE_ID.equals("") || Constants.DEVICE_ID.isEmpty()) {
                        Constants.DEVICE_ID = telephonyManager.getSimSerialNumber();
                        if (!Constants.DEVICE_ID.equals("") || !Constants.DEVICE_ID.isEmpty()) {
                            if (NetworkUtil.isInternetConnencted(SplashActivity.this)) {
                                GetLoginPageMobile();
                            } else {
                                Alert.showAlertWithFinishNew(SplashActivity.this, "Please check your internet connection.");
                            }
                        }
                    } else {
                        Constants.DEVICE_ID = telephonyManager.getDeviceId();
                        if (!Constants.DEVICE_ID.equals("") || !Constants.DEVICE_ID.isEmpty()) {
                            if (NetworkUtil.isInternetConnencted(SplashActivity.this)) {
//                                GetIMEICounter();
                                GetLoginPageMobile();
                            } else {
                                Alert.showAlertWithFinishNew(SplashActivity.this, "Please check your internet connection.");
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            Toast.makeText(SplashActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
        }
    };
    private Progress progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        progress = new Progress(SplashActivity.this);

        TedPermission.with(SplashActivity.this)
                .setPermissionListener(permissionListener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.CAMERA)
                .check();
    }

    public native String liveURLFromJNI();

    public native String stagingURLFromJNI();

    public native String publicKeyFromJNI();

    public native String privateKeyFromJNI();

    public native String secretKeyFromJNI();

    public native String authKeyFromJNI();

    public native String connectionIDFromJNI();

    public native String stagingConnectionIDFromJNI();

    public native String apiVersionNumberFromJNI();

    public native String deviceTypeFromJNI();

    public native String appVersionNumberFromJNI();

    private void GetLoginPageMobile() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetLoginPageMobile");

            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgVersion", Constants.APP_VERSION_NUMBER);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);

            String dataValue = reqJsonObject.toString();
            Timber.d(dataValue, "SplashActivity");

            API.getInstance().encryptRequest(SplashActivity.this, dataValue, Constants.PUBLIC_KEY, Constants.CONNECTION_ID,
                    Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_TYPE, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(SplashActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                if (response.code() == 200) {
                                    String responseString = response.body().string();
                                    String responseDataValue = decryptData.parseResponse(responseString);
                                    Timber.e(responseDataValue, "response");
                                    JSONObject jsonObject = new JSONObject(responseDataValue);
                                    if (jsonObject.getString("Status").equals("1")) {
                                        JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                        String IP = "";
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jObj = jsonArray.getJSONObject(i);
                                            IP = jObj.getString("IP");
                                            Constants.IMPS_ACTIVE = jObj.getString("IMPS_ACTIVE");
                                        }

                                        try {
                                            if (android.os.Build.VERSION.SDK_INT > 9) {
                                                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                                StrictMode.setThreadPolicy(policy);
                                            }
                                            URL oracle = new URL(IP);
                                            BufferedReader in = new BufferedReader(
                                                    new InputStreamReader(oracle.openStream()));
                                            String all = "", inputLine;
                                            while ((inputLine = in.readLine()) != null) {
                                                all += inputLine;
                                            }
                                            Constants.IP = all.toString();
                                            if (Constants.IP.equals(" ") || Constants.IP.isEmpty()) {
                                                Alert.ShowAlert(SplashActivity.this, "Something wrong.Please try after sometimes.");
                                            } else {
                                                finish();
                                                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                                            }

                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                            Alert.ShowAlert(SplashActivity.this, "Something wrong.Please try after sometimes.");
                                        }
                                    } else if (jsonObject.getString("Status").equals("2")) {
                                        startActivity(new Intent(SplashActivity.this, UpdateApplication.class));
                                    } else {
                                        Toast.makeText(SplashActivity.this, jsonObject.getString("ResponseMessage"), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(SplashActivity.this, "Check internet.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(SplashActivity.this, "Something wrong! Please try after sometimes.", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            Toast.makeText(SplashActivity.this, "Something wrong! Please try after sometimes.", Toast.LENGTH_SHORT).show();
                        }
                    }
            );

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(SplashActivity.this, "Something wrong! Please try after sometimes.", Toast.LENGTH_SHORT).show();
        }
    }
}
