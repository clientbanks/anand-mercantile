package com.amcblanand.android.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class PINVerificationActivity extends AppCompatActivity {

    private static final String TAG = Constants.TAG + PINVerificationActivity.class.getSimpleName();

    @BindView(R.id.et_verify_otp)
    EditText etVerifyOtp;
    @BindView(R.id.btn_verify_otp)
    Button btnVerifyOtp;

    private DecryptData decryptData;
    private Progress progress;

    private String ArgSource = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinverification);
        ButterKnife.bind(this);

        ArgSource = getIntent().getStringExtra("ArgSource");

        decryptData = new DecryptData();
        progress = new Progress(PINVerificationActivity.this);

        btnVerifyOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etVerifyOtp.getText().toString().trim().equals("") || etVerifyOtp.getText().toString().trim().isEmpty()) {
                    Toast.makeText(PINVerificationActivity.this, "Please enter otp.", Toast.LENGTH_SHORT).show();
                } else {
                    progress.createDialog(false);
                    progress.dialogMessage("Please wait. . ");
                    progress.showDialog();
                    CheckOTPCodeOpt();
                }
            }
        });

    }


    private void CheckOTPCodeOpt() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "CheckOTPCodeOpt");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgOtpCode", etVerifyOtp.getText().toString().trim());
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgBrCode", Constants.BRANCH_CODE);
            reqParamJsonObject.put("ArgActyp", Constants.ACTYP);
            reqParamJsonObject.put("ArgAccCode", Constants.ACC_CODE);
            reqParamJsonObject.put("ArgSource", ArgSource);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(PINVerificationActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(PINVerificationActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    Constants.PIN_VERIFY = 1;
                                    finish();
                                } else {
                                    Alert.ShowAlert(PINVerificationActivity.this, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
