package com.amcblanand.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.SquestionPojo;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class RegisterActivity extends AppCompatActivity implements Validator.ValidationListener {

    private static final String TAG = Constants.TAG + RegisterActivity.class.getSimpleName();

    @NotEmpty
    @Length(min = 9, message = "9 digit required.")
    @BindView(R.id.et_mobile_register)
    EditText etMobileRegister;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.check_terms)
    AppCompatCheckBox checkTerms;
    @BindView(R.id.tv_terms_condition)
    AppCompatTextView tvTermsCondition;
    @BindView(R.id.linear_terms_condition)
    LinearLayoutCompat linearTermsCondition;
    private Validator validator;
    private DecryptData decryptData;
    private Progress progress;

    private String mobile = "", localSession = "";
    private String type = "";
    private String fcmDeviceToken = "", termsAndCondition = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

//        mobile = getIntent().getStringExtra("MOBILE");
//        localSession = getIntent().getStringExtra("SESSION");

        type = getIntent().getStringExtra("type");

        fcmDeviceToken = PreferenceManager.getDefaultSharedPreferences(RegisterActivity.this)
                .getString(Constants.FCM_KEY, null);
        Timber.e("FCM -> " + fcmDeviceToken);

        validator = new Validator(this);
        validator.setValidationListener(this);
        progress = new Progress(this);
        decryptData = new DecryptData();

        if (type.equals("NU")) {
            btnRegister.setText("Register");
            linearTermsCondition.setVisibility(View.VISIBLE);
        } else {
            btnRegister.setText("Forgot Password");
            linearTermsCondition.setVisibility(View.GONE);
        }

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });


        tvTermsCondition.setOnClickListener(tvTermsConditionView -> {
            Intent intent = new Intent(RegisterActivity.this, OtherBankServices.class);
            intent.putExtra("URL", "http://www.amcblanand.com/terms-conditions.php");
            startActivity(intent);
        });

        checkTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    // TODO: 04-07-2018 CALL SERVICE
                    termsAndCondition = "Y";
                } else {
                    termsAndCondition = "N";
                }
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        if (type.equals("NU")) {

            if (termsAndCondition.equals("Y")) {
                progress.createDialog(false);
                progress.dialogMessage("Register. . .");
                progress.showDialog();
                FirstTimeRegistration();
            } else {
                Alert.ShowAlert(RegisterActivity.this, "Please select terms & condition.");
            }


        } else {
            // TODO: 21-05-2018 CHNAGES FOR FORGOT PASSWORD
            progress.createDialog(false);
            progress.dialogMessage("Please wait..");
            progress.showDialog();
            CheckRegistration();
        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(RegisterActivity.this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((TextView) (EditText) view).setError(message);
            } else if (view instanceof Spinner) {
                ((TextView) ((Spinner) view).getSelectedView()).setError(message);
            } else {
                Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void updateUserTc(String mobileNumber) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("function", "UpdateUserTC");

            JSONObject reqParamObject = new JSONObject();
            reqParamObject.put("ArgBankId", Constants.BANK_ID);
            reqParamObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamObject.put("ArgMobile", mobileNumber);
            reqParamObject.put("ArgClientID", etMobileRegister.getText().toString().trim());
            jsonObject.putOpt("parameter", reqParamObject);

            String dataValue = jsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(RegisterActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(RegisterActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                if (jsonObject.getString("Status").equals("1")) {

                                    Toast.makeText(RegisterActivity.this, "OTP sent.", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(RegisterActivity.this, OTPVerificationActivity.class);
                                    intent.putExtra("number", mobileNumber);
                                    intent.putExtra("cust_id", etMobileRegister.getText().toString().trim());
                                    startActivity(intent);
                                    finish();

                                } else {
                                    progress.hideDialog();
                                    Toast.makeText(RegisterActivity.this, jsonObject.getString("ResponseMessage"), Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void FirstTimeRegistration() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "FirstTimeRegistration");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", etMobileRegister.getText().toString().trim());
            reqParamJsonObject.put("ArgDeviceType", Constants.DEVICE_TYPE);
            reqParamJsonObject.put("ArgSessionKey", "");

            if (fcmDeviceToken == null || fcmDeviceToken.equals("")) {
                reqParamJsonObject.put("ArgFCMKey", "");
            } else {
                reqParamJsonObject.put("ArgFCMKey", fcmDeviceToken);
            }

            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(RegisterActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(RegisterActivity.this) {

                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    String mobileNumber = "";
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        mobileNumber = object.getString("MOBILE");
                                    }
                                    progress.createDialog(false);
                                    progress.dialogMessage("Please wait. . .");
                                    progress.showDialog();
                                    GetRegisterBankDetails(mobileNumber);
                                } else {
                                    Alert.ShowAlert(RegisterActivity.this, jsonObject.getString("ResponseMessage"));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void GetRegisterBankDetails(String mobileNumber) {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetRegisterBankDetails");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", mobileNumber);
            reqParamJsonObject.put("ArgDeviceType", Constants.DEVICE_TYPE);
            reqParamJsonObject.put("ArgSessionKey", "");
            reqParamJsonObject.put("ArgNetBankPass", "");
            if (fcmDeviceToken == null || fcmDeviceToken.equals("")) {
                reqParamJsonObject.put("ArgFCMKey", "");
            } else {
                reqParamJsonObject.put("ArgFCMKey", fcmDeviceToken);
            }
            reqParamJsonObject.put("ArgClientID", etMobileRegister.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(RegisterActivity.this, dataValue, "M}^C?xa]gG[7qQ3=.5Wkb~",
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(RegisterActivity.this) {

                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                if (jsonObject.getString("Status").equals("1")) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        Constants.SECURITY_QUESTIONS_EXITS = object.getString("SecurityQuestionExist");
                                    }

                                    progress.createDialog(false);
                                    progress.dialogMessage("Sending OTP. . .");
                                    progress.showDialog();
                                    updateUserTc(mobileNumber);

                                } else {
                                    progress.hideDialog();
                                    Toast.makeText(RegisterActivity.this, jsonObject.getString("ResponseMessage"), Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void ForgetPasswordOTP() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "ForgetPasswordOTP");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", etMobileRegister.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, "Request");

            API.getInstance().encryptRequest(RegisterActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(RegisterActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, "Response");

                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                progress.hideDialog();

                                if (jsonObject.getString("Status").equals("1")) {
                                    startActivity(new Intent(RegisterActivity.this, ForgotPINVerificationActivity.class));
                                    finish();
                                } else {
                                    Alert.ShowAlert(RegisterActivity.this, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Toast.makeText(RegisterActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
            Toast.makeText(RegisterActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }
    }


    private void GerSecurityQuestions() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetSecurityQuestion");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", etMobileRegister.getText().toString().trim());
            reqParamJsonObject.put("ArgType", "F");

            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, "Request");

            API.getInstance().encryptRequest(RegisterActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(RegisterActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, "Response");


                                SquestionPojo squestionPojo = SquestionPojo.objectFromData(responseDataValue);

                                progress.hideDialog();

                                if (squestionPojo.getStatus() == 1) {

                                    Intent intent1 = new Intent(RegisterActivity.this, VerifySecurityQuestionActivity.class);
                                    intent1.putExtra("responseDataValue", responseDataValue);
                                    intent1.putExtra("ArgType", "F");
                                    intent1.putExtra("cust_id", etMobileRegister.getText().toString().trim());
                                    intent1.putExtra("pwd", "");
                                    startActivity(intent1);
                                    finish();

                                } else {
                                    Alert.ShowAlert(RegisterActivity.this, squestionPojo.getResponseMessage());
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Toast.makeText(RegisterActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
            Toast.makeText(RegisterActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    private void CheckRegistration() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "CheckRegistration");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", etMobileRegister.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.d(dataValue, "request");

            API.getInstance().encryptRequest(RegisterActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(RegisterActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseData = response.body().string();
                                String responseDataValue = decryptData.parseResponse(responseData);
                                Timber.d(responseDataValue, "Response");
                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                if (jsonObject.getString("Status").equals("1")) {
                                    String isPin = "";
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        Constants.MOBILE_NUMBER = jObj.getString("MOBILE");
                                        isPin = jObj.getString("IS_PIN");
                                        Constants.SECURITY_QUESTIONS_EXITS = jObj.getString("SECURITYQUESTIONEXIST");
                                        Constants.SECURITY_QUESTIONS_VERIFIED = jObj.getString("SECURITYQUESTIONVERIFIED");
                                    }
                                    progress.hideDialog();
                                    Constants.CUST_ID = etMobileRegister.getText().toString().trim();
                                    if (Constants.SECURITY_QUESTIONS_EXITS.equalsIgnoreCase("Y")) {
                                        progress.createDialog(false);
                                        progress.dialogMessage("Please wait..");
                                        progress.showDialog();
                                        GerSecurityQuestions();
                                    } else {
                                        progress.createDialog(false);
                                        progress.dialogMessage("Please wait..");
                                        progress.showDialog();
                                        ForgetPasswordOTP();
                                    }
                                } else {
                                    progress.hideDialog();
                                    Intent intent = new Intent(RegisterActivity.this, RegisterActivity.class);
                                    intent.putExtra("type", "NU");
                                    startActivity(intent);
                                    Alert.ShowAlert(RegisterActivity.this, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                Alert.ShowAlert(RegisterActivity.this, "Please try after sometimes");
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Alert.ShowAlert(RegisterActivity.this, "Check internet");
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

}
