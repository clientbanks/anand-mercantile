package com.amcblanand.android.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Process;
import android.preference.PreferenceManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.base.BaseActivity;
import com.amcblanand.android.dialogs.FullScreenPasswordChange;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.AsteriskPasswordTransformationMethod;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class LoginActivity extends BaseActivity {


    private static final String TAG = Constants.TAG + LoginActivity.class.getSimpleName();
    @BindView(R.id.et_cust_id)
    TextInputEditText etCustId;
    @BindView(R.id.txt_cust_id)
    TextInputLayout txtCustId;
    @BindView(R.id.et_cust_pwd)
    TextInputEditText etCustPwd;
    @BindView(R.id.txt_pwd)
    TextInputLayout txtPwd;
    @BindView(R.id.tv_forgot_pin)
    AppCompatTextView tvForgotPin;
    @BindView(R.id.tv_register)
    AppCompatTextView tvRegister;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.linear_deposit)
    LinearLayout linearDeposit;
    @BindView(R.id.linear_loan)
    LinearLayout linearLoan;
    @BindView(R.id.linear_emi)
    LinearLayout linearEmi;
    @BindView(R.id.linear_fd)
    LinearLayout linearFd;
    @BindView(R.id.linear_contact)
    LinearLayout linearContact;


    private Context context;
    private DecryptData decryptData;
    private Progress progress;

    private String password = "";
    private String fcmDeviceToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        fcmDeviceToken = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this)
                .getString(Constants.FCM_KEY, null);

        context = this;
        decryptData = new DecryptData();
        progress = new Progress(context);

        if (Constants.IsUserBlock) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getApplicationInfo().loadLabel(context.getPackageManager()))
                    .setIcon(R.mipmap.ic_launcher)
                    .setMessage(Constants.UserUnBlockTime)
                    .setCancelable(false);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

        etCustPwd.setTransformationMethod(new AsteriskPasswordTransformationMethod());

        etCustId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                txtCustId.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etCustPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                txtPwd.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                intent.putExtra("type", "NU");
                startActivity(intent);
            }
        });

        tvForgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                intent.putExtra("type", "FP");
                startActivity(intent);
            }
        });


    }


    private String getFcm() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.SHARED_PREF, 0);
        return pref.getString("regId", "");

    }

    @OnClick(R.id.btn_login)
    public void onViewClicked() {
        String custID = etCustId.getText().toString().trim();
        String pwd = etCustPwd.getText().toString().trim();

        if (custID.equals(" ") || custID.isEmpty()) {
            txtCustId.setError("This field required.");
        } else if (custID.length() < 9) {
            txtCustId.setError("9 digit required.");
        } else if (pwd.equals(" ") || pwd.isEmpty()) {
            txtPwd.setError("This field required.");
        } else if (pwd.length() < 6) {
            txtPwd.setError("6 digit required.");
        } else {
            hideKeyboard();
            progress.createDialog(false);
            progress.dialogMessage("Please wait..");
            progress.showDialog();
            CheckRegistration();
        }
    }

    private void CheckRegistration() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "CheckRegistration");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", etCustId.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.d(dataValue, "request");

            API.getInstance().encryptRequest(LoginActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(LoginActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseData = response.body().string();
                                String responseDataValue = decryptData.parseResponse(responseData);
                                Timber.e(responseDataValue, "Response");
                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                if (jsonObject.getString("Status").equals("1")) {
                                    String isPin = "";
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        Constants.MOBILE_NUMBER = jObj.getString("MOBILE");
                                        Constants.TERMS_CONDITION = jObj.getString("TC_ACCEPT");
                                        isPin = jObj.getString("IS_PIN");

                                        if (jObj.getString("USER_BLOCK").equals("Y")) {
                                            Constants.IsUserBlock = true;
                                            Constants.UserUnBlockTime = jObj.getString("LOGIN_UNBLOCK_TIME");
                                        } else {
                                            Constants.IsUserBlock = false;
                                            Constants.UserUnBlockTime = "";
                                        }

                                        Constants.SECURITY_QUESTIONS_EXITS = jObj.getString("SECURITYQUESTIONEXIST");
                                        Constants.SECURITY_QUESTIONS_VERIFIED = jObj.getString("SECURITYQUESTIONVERIFIED");
                                    }
                                    progress.hideDialog();
                                    Constants.CUST_ID = etCustId.getText().toString().trim();
                                    if (isPin.equalsIgnoreCase("n")) {
                                        Constants.CUST_ID = etCustId.getText().toString().trim();
                                        Intent intent = new Intent(LoginActivity.this, CreatePasswordActivity.class);
                                        intent.putExtra("number", Constants.MOBILE_NUMBER);
                                        intent.putExtra("cust_id", Constants.CUST_ID);
                                        startActivity(intent);
                                    } else {
                                        Constants.IS_LOGIN = "Y";
                                        progress.createDialog(false);
                                        progress.dialogMessage("Please wait..");
                                        progress.showDialog();
                                        LoginWithPIN();
//										if (Constants.SECURITY_QUESTIONS_EXITS.equalsIgnoreCase("N")) {
//											progress.createDialog(false);
//											progress.dialogMessage("Please wait..");
//											progress.showDialog();
//											getSecurityQueAL();
//										} else {
//											if (Constants.SECURITY_QUESTIONS_VERIFIED.equalsIgnoreCase("y")) {
//												progress.createDialog(false);
//												progress.dialogMessage("Please wait..");
//												progress.showDialog();
//												LoginWithPIN();
//											} else {
//												progress.createDialog(false);
//												progress.dialogMessage("Please wait..");
//												progress.showDialog();
//												getSecurityQueAL();
//											}
//										}
                                    }
                                } else {
                                    progress.hideDialog();
                                    Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                                    intent.putExtra("type", "NU");
                                    startActivity(intent);
                                    Alert.ShowAlert(LoginActivity.this, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                Alert.ShowAlert(LoginActivity.this, "Please try after sometimes");
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Alert.ShowAlert(LoginActivity.this, "Check internet");
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }


    private void LoginWithPIN() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "LoginWithPIN");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgPIN", etCustPwd.getText().toString().trim());
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgFCMKey", fcmDeviceToken);
            reqParamJsonObject.put("ArgClientID", etCustId.getText().toString().trim());
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, "Request");

            API.getInstance().encryptRequest(LoginActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(LoginActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, "Response");
                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                if (jsonObject.getString("Status").equals("1")) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        Constants.PUBLIC_KEY = jObj.getString("SessionKey");
                                        Constants.IMPS_ACTIVE = jObj.getString("IMPSActive");
                                        Constants.CHECK_NR_TIME = jObj.getString("CHECK_NR_TIME");
                                        Constants.NEFT_START_TIME = jObj.getString("NEFT_START_TIME");
                                        Constants.NEFT_END_TIME = jObj.getString("NEFT_END_TIME");
                                        Constants.RTGS_START_TIME = jObj.getString("RTGS_START_TIME");
                                        Constants.RTGS_END_TIME = jObj.getString("RTGS_END_TIME");
                                        Constants.SECURITY_QUESTIONS_EXITS = jObj.getString("SecurityQuestionExist");
                                        Constants.NeedPasswordReset = false;
                                    }
                                    progress.hideDialog();
                                    Constants.CUST_ID = etCustId.getText().toString().trim();
                                    finish();
                                    startActivity(new Intent(LoginActivity.this, LoginOTPVerificationActivity.class));
                                } else if (jsonObject.getString("Status").equals("3")) {
                                    progress.hideDialog();
                                    Constants.CUST_ID = etCustId.getText().toString().trim();
                                    JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jObj = jsonArray.getJSONObject(i);
                                        Constants.PUBLIC_KEY = jObj.getString("SessionKey");
                                        Constants.IMPS_ACTIVE = jObj.getString("IMPSActive");
                                        Constants.CHECK_NR_TIME = jObj.getString("CHECK_NR_TIME");
                                        Constants.NEFT_START_TIME = jObj.getString("NEFT_START_TIME");
                                        Constants.NEFT_END_TIME = jObj.getString("NEFT_END_TIME");
                                        Constants.RTGS_START_TIME = jObj.getString("RTGS_START_TIME");
                                        Constants.RTGS_END_TIME = jObj.getString("RTGS_END_TIME");
                                        Constants.SECURITY_QUESTIONS_EXITS = jObj.getString("SecurityQuestionExist");
                                        Constants.NeedPasswordReset = true;
                                    }

//                                    if (Constants.NeedPasswordReset) {
                                    androidx.appcompat.app.AlertDialog dialog = new androidx.appcompat.app.AlertDialog.Builder(LoginActivity.this)
                                            .setMessage(jsonObject.getString("ResponseMessage"))
                                            .setNegativeButton("Close App", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    finish();
                                                }
                                            })
                                            .setPositiveButton("Change password", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    FullScreenPasswordChange screenPasswordChange = new FullScreenPasswordChange();
                                                    screenPasswordChange.show(getSupportFragmentManager(), null);
                                                }
                                            })
                                            .setCancelable(false)
                                            .create();
                                    dialog.show();
//                                    }


//                                    Constants.CUST_ID = etCustId.getText().toString().trim();
//                                    finish();
//                                    startActivity(new Intent(LoginActivity.this, LoginOTPVerificationActivity.class));
                                } else if (jsonObject.getString("Status").equals("4")) {
                                    progress.hideDialog();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setTitle(context.getApplicationInfo().loadLabel(context.getPackageManager()))
                                            .setIcon(R.mipmap.ic_launcher)
                                            .setMessage(jsonObject.getString("ResponseMessage"))
                                            .setCancelable(false);
                                    AlertDialog alertDialog = builder.create();
                                    alertDialog.show();
                                } else {
                                    progress.hideDialog();
                                    Alert.ShowAlert(LoginActivity.this, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Toast.makeText(LoginActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
            Toast.makeText(LoginActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setCancelable(true);
        dialog.setContentView(R.layout.logout_dialog_login);
        dialog.show();

        Button declineButton = (Button) dialog.findViewById(R.id.btn_ok);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                int pid = Process.myPid();
                Process.killProcess(pid);
                System.exit(0);

            }
        });

        Button cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

    }


    @OnClick({R.id.linear_deposit, R.id.linear_loan, R.id.linear_emi, R.id.linear_fd, R.id.linear_contact})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linear_deposit:
                Intent intent = new Intent(LoginActivity.this, OtherBankServices.class);
                intent.putExtra("URL", "http://amcblanand.com/deposit_rates.php");
                startActivity(intent);
                break;
            case R.id.linear_loan:
                Intent intent1 = new Intent(LoginActivity.this, OtherBankServices.class);
                intent1.putExtra("URL", "http://amcblanand.com/loan_rates.php");
                startActivity(intent1);
                break;
            case R.id.linear_emi:
                Intent intent2 = new Intent(LoginActivity.this, OtherBankServices.class);
                intent2.putExtra("URL", "http://amcblanand.com/emicalculator.php");
                startActivity(intent2);
                break;
            case R.id.linear_fd:
                Intent intent3 = new Intent(LoginActivity.this, OtherBankServices.class);
                intent3.putExtra("URL", "http://amcblanand.com/fdcalc.php");
                startActivity(intent3);
                break;
            case R.id.linear_contact:
                Intent intent4 = new Intent(LoginActivity.this, OtherBankServices.class);
                intent4.putExtra("URL", "http://amcblanand.com/contactus.php");
                startActivity(intent4);
                break;
        }
    }
}
