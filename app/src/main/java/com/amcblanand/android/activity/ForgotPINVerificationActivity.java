package com.amcblanand.android.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class ForgotPINVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = Constants.TAG + ForgotPINVerificationActivity.class.getSimpleName();

    @BindView(R.id.et_number_otp)
    EditText etNumberOtp;
    @BindView(R.id.et_otp)
    EditText etOtp;
    @BindView(R.id.btn_resend_otp)
    Button btnResendOtp;

    private String cust_id = "", number = "";

    private DecryptData decryptData;
    private Progress progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pinverification);
        ButterKnife.bind(this);

        cust_id = getIntent().getStringExtra("cust_id");
        number = getIntent().getStringExtra("number");

        progress = new Progress(ForgotPINVerificationActivity.this);

        decryptData = new DecryptData();

        etNumberOtp.setText(Constants.MOBILE_NUMBER);

        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 7) {
                    progress.createDialog(false);
                    progress.dialogMessage("Verifying OTP");
                    progress.showDialog();
                    EnterTheOTP();
                } else {
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnResendOtp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_resend_otp:
                progress.createDialog(false);
                progress.dialogMessage("Sending OTP again");
                progress.showDialog();
                resendOTP();
                break;
        }
    }

    private void resendOTP() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "ForgetPasswordOTP");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", cust_id);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(ForgotPINVerificationActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(ForgotPINVerificationActivity.this) {

                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                if (jsonObject.getString("Status").equals("1")) {
                                    Toast.makeText(ForgotPINVerificationActivity.this, "OTP sent.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Alert.ShowAlert(ForgotPINVerificationActivity.this, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                                Alert.ShowAlert(ForgotPINVerificationActivity.this, "Something wrong!");
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Alert.ShowAlert(ForgotPINVerificationActivity.this, "Something wrong!");
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
            Alert.ShowAlert(ForgotPINVerificationActivity.this, "Something wrong!");
        }
    }

    private void EnterTheOTP() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "EnterTheOTP");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", number);
            reqParamJsonObject.put("ArgOTP", etOtp.getText().toString().trim());
            reqParamJsonObject.put("ArgSource", "FP");
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(ForgotPINVerificationActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(ForgotPINVerificationActivity.this) {

                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    Intent intent = new Intent(ForgotPINVerificationActivity.this, CreatePasswordActivity.class);
                                    intent.putExtra("number", number);
                                    intent.putExtra("cust_id", cust_id);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Alert.ShowAlert(ForgotPINVerificationActivity.this, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }
}
