package com.amcblanand.android.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.amcblanand.android.R;
import com.amcblanand.android.adapter.SecQueAdapter;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.SQuestions;
import com.amcblanand.android.model.SquestionPojo;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class SecurityQuestionActivity extends AppCompatActivity {

	@BindView(R.id.rv_security_que)
	RecyclerView rvSecurityQue;
	@BindView(R.id.btn_set_question)
	Button btnSetQuestion;

	private Progress progress;
	private DecryptData decryptData;
	private SquestionPojo squestionPojo;

	private String ArgQueAns = "";

	private List<SquestionPojo.ResponseJSONBean> responseJSONBeans = new ArrayList<>();
	private SecQueAdapter secQueAdapter;
	public static Map<String, String> stringStringMap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_security_question);
		ButterKnife.bind(this);

		decryptData = new DecryptData();
		progress = new Progress(SecurityQuestionActivity.this);

		if (getIntent().getExtras() != null) {
			squestionPojo = SquestionPojo.objectFromData(getIntent().getExtras().getString("responseDataValue"));
			responseJSONBeans = squestionPojo.getResponseJSON();
		}

		stringStringMap = new HashMap<>();

		Map<String, List<SQuestions>> stringSQuestionsMap = new HashMap<>();

		for (int i = 0; i < (responseJSONBeans.size() / 10); i++) {

			List<SQuestions> sQuestions = new ArrayList<>();

			for (int k = 0; k < responseJSONBeans.size(); k++) {

				if (responseJSONBeans.get(k).getGROUPCD() == i + 1) {
					sQuestions.add(new SQuestions(responseJSONBeans.get(k).getSRNO(), responseJSONBeans.get(k).getQUESTION(), "T"));
				}


			}

			stringSQuestionsMap.put("" + i, sQuestions);

		}

		secQueAdapter = new SecQueAdapter(SecurityQuestionActivity.this, stringSQuestionsMap);
		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SecurityQuestionActivity.this);
		rvSecurityQue.setLayoutManager(mLayoutManager);
//        rvSecurityQue.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
//        rvSecurityQue.setItemAnimator(new DefaultItemAnimator());
		rvSecurityQue.setAdapter(secQueAdapter);

		btnSetQuestion.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if ((responseJSONBeans.size() / 10) == stringStringMap.size()) {

					for (int i = 0; i < stringStringMap.size(); i++) {

						if (i == 0) {
							ArgQueAns = stringStringMap.get("" + i) + "|";
						} else if (i == stringStringMap.size() - 1) {
							ArgQueAns = ArgQueAns + stringStringMap.get("" + i);
						} else {
							ArgQueAns = ArgQueAns + stringStringMap.get("" + i) + "|";
						}
					}
					progress.createDialog(false);
					progress.dialogMessage("Please wait ...");
					progress.showDialog();
					SetSecurityQuestion();
				} else {
					Toast.makeText(SecurityQuestionActivity.this, "Please Enter All Answer", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	private void SetSecurityQuestion() {
		try {
			JSONObject reqJsonObject = new JSONObject();

			if (Constants.IS_LOGIN.equalsIgnoreCase("")) {
				reqJsonObject.put("function", "SetSecurityQuestion");
			} else {
				reqJsonObject.put("function", "SetSecurityQuestionAL");
			}

			JSONObject reqParamJsonObject = new JSONObject();
			reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
			reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
			reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
			reqParamJsonObject.put("ArgQueAns", ArgQueAns);
			reqJsonObject.putOpt("parameter", reqParamJsonObject);
			String dataValue = reqJsonObject.toString();
			Timber.e(dataValue, "Request");

			API.getInstance().encryptRequest(SecurityQuestionActivity.this, dataValue, Constants.PUBLIC_KEY,
					Constants.CONNECTION_ID, Constants.AUTH_KEY,
					Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
					new RetrofitCallbacks<ResponseBody>(SecurityQuestionActivity.this) {
						@Override
						public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
							super.onResponse(call, response);
							try {
								String responseString = response.body().string();
								String responseDataValue = decryptData.parseResponse(responseString);
								Timber.e(responseDataValue, "Response");

								JSONObject jsonObject = new JSONObject(responseDataValue);

								progress.hideDialog();

								if (jsonObject.getString("Status").equals("1")) {
									// go to mpin
									if (Constants.IS_LOGIN.equalsIgnoreCase("y")) {
										startActivity(new Intent(SecurityQuestionActivity.this, DashboardActivity.class));
										finish();
									} else {
										startActivity(new Intent(SecurityQuestionActivity.this, LoginActivity.class));
										finish();
									}

								} else {
									Alert.ShowAlert(SecurityQuestionActivity.this, jsonObject.getString("ResponseMessage"));
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						@Override
						public void onFailure(Call<ResponseBody> call, Throwable t) {
							super.onFailure(call, t);
							Toast.makeText(SecurityQuestionActivity.this, "Something wrong!", Toast.LENGTH_SHORT).show();
						}
					});

		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this, "Something wrong!", Toast.LENGTH_SHORT).show();
		}
	}
}
