package com.amcblanand.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class OTPVerificationActivity extends AppCompatActivity {

    private static final String TAG = Constants.TAG + OTPVerificationActivity.class.getSimpleName();

    @BindView(R.id.et_number_otp)
    EditText etNumberOtp;
    @BindView(R.id.et_otp)
    EditText etOtp;
    @BindView(R.id.btn_resend_otp)
    Button btnResendOtp;
    @BindView(R.id.btn_verify)
    Button btnVerify;

    private DecryptData decryptData;
    private String number = "", cust_id = "";
    private Progress progress;
    private String fcmDeviceToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverification);
        ButterKnife.bind(this);

        progress = new Progress(OTPVerificationActivity.this);

        decryptData = new DecryptData();
        Intent intent = getIntent();
        number = intent.getStringExtra("number");
        cust_id = intent.getStringExtra("cust_id");

        fcmDeviceToken = PreferenceManager.getDefaultSharedPreferences(OTPVerificationActivity.this)
                .getString(Constants.FCM_KEY, null);

        Constants.MOBILE_NUMBER = number;
        Constants.CUST_ID = cust_id;

        if (!number.isEmpty()) {
            etNumberOtp.setText(number);
            etNumberOtp.setEnabled(false);
            etNumberOtp.setFocusable(false);
        } else {
            etNumberOtp.setText("");
            etNumberOtp.setEnabled(true);
            etNumberOtp.setFocusable(true);
        }

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etOtp.getText().toString().trim().length() < 7) {
                    Toast.makeText(OTPVerificationActivity.this, "7 digit required.", Toast.LENGTH_SHORT).show();
                } else {
                    progress.createDialog(false);
                    progress.dialogMessage("Verifying OTP");
                    progress.showDialog();
                    EnterTheOTP();
                }
            }
        });


        /*etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 7 && !etNumberOtp.getText().toString().trim().isEmpty()) {
                    progress.createDialog(false);
                    progress.dialogMessage("Verifying OTP");
                    progress.showDialog();
                    EnterTheOTP();
                } else {
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });*/

        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.createDialog(false);
                progress.dialogMessage("Sending OTP again");
                progress.showDialog();
                resendOTP();
            }
        });
    }

    private void EnterTheOTP() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "EnterTheOTP");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", number);
            reqParamJsonObject.put("ArgOTP", etOtp.getText().toString().trim());
            reqParamJsonObject.put("ArgSource", "NR");
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(OTPVerificationActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(OTPVerificationActivity.this) {

                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    Intent intent = new Intent(OTPVerificationActivity.this, CreatePasswordActivity.class);
                                    intent.putExtra("number", number);
                                    intent.putExtra("cust_id", cust_id);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Alert.ShowAlert(OTPVerificationActivity.this, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void resendOTP() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetRegisterBankDetails");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", number);
            reqParamJsonObject.put("ArgDeviceType", Constants.DEVICE_TYPE);
            reqParamJsonObject.put("ArgSessionKey", "");
            reqParamJsonObject.put("ArgNetBankPass", "");
            reqParamJsonObject.put("ArgFCMKey", fcmDeviceToken);
            reqParamJsonObject.put("ArgClientID", cust_id);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(OTPVerificationActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(OTPVerificationActivity.this) {

                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                if (jsonObject.getString("Status").equals("1")) {
                                    Toast.makeText(OTPVerificationActivity.this, "OTP sent.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Alert.ShowAlert(OTPVerificationActivity.this, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                                Alert.ShowAlert(OTPVerificationActivity.this, "Something wrong!");
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Alert.ShowAlert(OTPVerificationActivity.this, "Something wrong!");
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
            Alert.ShowAlert(OTPVerificationActivity.this, "Something wrong!");
        }
    }
}
