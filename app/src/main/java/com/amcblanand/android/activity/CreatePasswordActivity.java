package com.amcblanand.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.SquestionPojo;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class CreatePasswordActivity extends AppCompatActivity implements Validator.ValidationListener {

    private static final String TAG = Constants.TAG + CreatePasswordActivity.class.getSimpleName();

    @NotEmpty
    @Password
    @Length(min = 6, message = "6 digit required.")
    @BindView(R.id.et_pin)
    EditText etPin;

    @NotEmpty
    @Length(min = 6, message = "6 digit required.")
    @ConfirmPassword
    @BindView(R.id.et_confirm_pin)
    EditText etConfirmPin;

    @BindView(R.id.btn_create_pin)
    Button btnCreatePin;

    private DecryptData decryptData;
    private Progress progress;
    private Validator validator;

    private String number = "", cust_id = "";
    private String fcmDeviceToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_password);
        ButterKnife.bind(this);

        validator = new Validator(this);
        validator.setValidationListener(this);
        progress = new Progress(this);

        decryptData = new DecryptData();

        fcmDeviceToken = PreferenceManager.getDefaultSharedPreferences(CreatePasswordActivity.this)
                .getString(Constants.FCM_KEY, null);

        number = getIntent().getStringExtra("number");
        cust_id = getIntent().getStringExtra("cust_id");

        btnCreatePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        progress.createDialog(false);
        progress.dialogMessage("Creating PIN. . .");
        progress.showDialog();
        FirstTimeEnterPIN();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(CreatePasswordActivity.this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((TextView) (EditText) view).setError(message);
            } else if (view instanceof Spinner) {
                ((TextView) ((Spinner) view).getSelectedView()).setError(message);
            } else {
                Toast.makeText(CreatePasswordActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void FirstTimeEnterPIN() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "FirstTimeEnterPIN");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgMobile", number);
            reqParamJsonObject.put("ArgDeviceType", Constants.DEVICE_TYPE);
            reqParamJsonObject.put("ArgPIN", etPin.getText().toString().trim());
            reqParamJsonObject.put("ArgSessionKey", "");
            reqParamJsonObject.put("ArgFCMKey", fcmDeviceToken);
            reqParamJsonObject.put("ArgClientID", cust_id);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, TAG);

            API.getInstance().encryptRequest(CreatePasswordActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(CreatePasswordActivity.this) {

                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, TAG);
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, TAG);

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                if (jsonObject.getString("Status").equals("1")) {

                                    if (Constants.SECURITY_QUESTIONS_EXITS.equalsIgnoreCase("n")) {
                                        progress.createDialog(false);
                                        progress.dialogMessage("Please wait..");
                                        progress.showDialog();
                                        getSecurityQuestion();
                                    } else {
                                        startActivity(new Intent(CreatePasswordActivity.this, LoginActivity.class));
                                        finish();
                                    }
                                } else {
                                    Alert.ShowAlert(CreatePasswordActivity.this, "Please try after sometimes.");
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void getSecurityQuestion() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetSecurityQuestion");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqParamJsonObject.put("ArgType", "N");
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, "Request");

            API.getInstance().encryptRequest(CreatePasswordActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(CreatePasswordActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, "Response");

                                SquestionPojo squestionPojo = SquestionPojo.objectFromData(responseDataValue);

                                if (squestionPojo.getStatus() == 1) {
                                    progress.hideDialog();
                                    Intent intent = new Intent(CreatePasswordActivity.this, SecurityQuestionActivity.class);
                                    intent.putExtra("responseDataValue", responseDataValue);
                                    intent.putExtra("ArgType", "N");
                                    startActivity(intent);
                                    finish();
                                } else {
                                    progress.hideDialog();
                                    Alert.ShowAlert(CreatePasswordActivity.this, squestionPojo.getResponseMessage());
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Toast.makeText(CreatePasswordActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception e) {
            progress.hideDialog();
            e.printStackTrace();
            Alert.ShowAlert(CreatePasswordActivity.this, "Something wrong!");
        }
    }

}
