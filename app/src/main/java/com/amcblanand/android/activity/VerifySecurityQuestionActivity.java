package com.amcblanand.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.amcblanand.android.R;
import com.amcblanand.android.adapter.VerifySecurityQuestionAdapter;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.SQuestions;
import com.amcblanand.android.model.SquestionPojo;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class VerifySecurityQuestionActivity extends AppCompatActivity {

	@BindView(R.id.rv_security_ans)
	RecyclerView rvSecurityAns;
	@BindView(R.id.btn_set_answer)
	Button btnSetAnswer;

	private SquestionPojo squestionPojo;

	//    private Validator validator;
	private DecryptData decryptData;
	private Progress progress;

	private String ArgQueAns = "";

	private List<SquestionPojo.ResponseJSONBean> responseJSONBeans = new ArrayList<>();
	private VerifySecurityQuestionAdapter verifySecurityQuestionAdapter;

	public static Map<String, String> stringStringForgotMap;
	List<SQuestions> sQuestions;

	private String fcmDeviceToken = "";
	private String custId, password;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_verify_security_question);
		ButterKnife.bind(this);

		fcmDeviceToken = PreferenceManager.getDefaultSharedPreferences(VerifySecurityQuestionActivity.this)
				.getString(Constants.FCM_KEY, null);

		decryptData = new DecryptData();
		progress = new Progress(VerifySecurityQuestionActivity.this);


		if (getIntent().getExtras() != null) {
			squestionPojo = SquestionPojo.objectFromData(getIntent().getExtras().getString("responseDataValue"));
			responseJSONBeans = squestionPojo.getResponseJSON();
			custId = getIntent().getExtras().getString("cust_id");
			password = getIntent().getExtras().getString("pwd");
		}

		stringStringForgotMap = new HashMap<>();

		sQuestions = new ArrayList<>();

		for (int i = 0; i < responseJSONBeans.size(); i++) {


			for (int k = 0; k < responseJSONBeans.size(); k++) {

				if (responseJSONBeans.get(k).getGROUPCD() == i + 1) {
					sQuestions.add(new SQuestions(responseJSONBeans.get(k).getSRNO(), responseJSONBeans.get(k).getQUESTION(), "T"));
				}

			}
		}


		verifySecurityQuestionAdapter = new VerifySecurityQuestionAdapter(VerifySecurityQuestionActivity.this, sQuestions);
		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(VerifySecurityQuestionActivity.this);
		rvSecurityAns.setLayoutManager(mLayoutManager);
		rvSecurityAns.setAdapter(verifySecurityQuestionAdapter);

		btnSetAnswer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if ((responseJSONBeans.size()) == stringStringForgotMap.size()) {

					for (int i = 0; i < stringStringForgotMap.size(); i++) {

						if (i == 0) {
							ArgQueAns = stringStringForgotMap.get("" + i) + "|";
						} else if (i == stringStringForgotMap.size() - 1) {
							ArgQueAns = ArgQueAns + stringStringForgotMap.get("" + i);
						} else {
							ArgQueAns = ArgQueAns + stringStringForgotMap.get("" + i) + "|";
						}
					}

					progress.createDialog(false);
					progress.dialogMessage("Please wait ...");
					progress.showDialog();
					VerifySecurityQuestion();

				} else {
					Toast.makeText(VerifySecurityQuestionActivity.this, "Please Enter All Answer", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	private void VerifySecurityQuestion() {
		try {
			JSONObject reqJsonObject = new JSONObject();

			if (Constants.IS_LOGIN.equalsIgnoreCase("")) {
				reqJsonObject.put("function", "VerifySecurityQuestion");
			} else {
				reqJsonObject.put("function", "VerifySecurityQuestionAL");
			}

			JSONObject reqParamJsonObject = new JSONObject();
			reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
			reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
			reqParamJsonObject.put("ArgClientID", custId);
//            reqParamJsonObject.put("ArgType", "F");
			reqParamJsonObject.put("ArgType", getIntent().getExtras().getString("ArgType"));


			reqParamJsonObject.put("ArgQueAns", ArgQueAns);
			reqJsonObject.putOpt("parameter", reqParamJsonObject);
			String dataValue = reqJsonObject.toString();
			Timber.e(dataValue, "Request");


			API.getInstance().encryptRequest(VerifySecurityQuestionActivity.this, dataValue, Constants.PUBLIC_KEY,
					Constants.CONNECTION_ID, Constants.AUTH_KEY,
					Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
					new RetrofitCallbacks<ResponseBody>(VerifySecurityQuestionActivity.this) {
						@Override
						public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
							super.onResponse(call, response);
							try {
								String responseString = response.body().string();
								String responseDataValue = decryptData.parseResponse(responseString);
								Timber.e(responseDataValue, "Response");

								JSONObject jsonObject = new JSONObject(responseDataValue);

								progress.hideDialog();

								if (jsonObject.getString("Status").equals("1")) {


									if (getIntent().getExtras().getString("ArgType").equalsIgnoreCase("E")) {

										if (Constants.IS_LOGIN.equalsIgnoreCase("")) {
											startActivity(new Intent(VerifySecurityQuestionActivity.this, VerifySecurityQuestionActivity.class));
											finish();
										} else {
											startActivity(new Intent(VerifySecurityQuestionActivity.this, DashboardActivity.class));
											finish();
										}


									} else if (getIntent().getExtras().getString("ArgType").equalsIgnoreCase("F")) {
										progress.createDialog(false);
										progress.dialogMessage("Please wait ...");
										progress.showDialog();
										ForgetPasswordOTP();
									} else {
										Toast.makeText(VerifySecurityQuestionActivity.this, "No ArgType Found", Toast.LENGTH_SHORT).show();
									}


								} else {
									JSONArray jsonArray = jsonObject.getJSONArray("ResponseJSON");
									String ANS_STATUS = "";
									for (int i = 0; i < jsonArray.length(); i++) {
										JSONObject jObj = jsonArray.getJSONObject(i);
										ANS_STATUS = jObj.getString("ANS_STATUS");
									}

									String[] value = ANS_STATUS.split("~");
									for (int i = 0; i < value.length; i++) {
										sQuestions.get(i).setIsError(value[i]);
									}
									verifySecurityQuestionAdapter = new VerifySecurityQuestionAdapter(VerifySecurityQuestionActivity.this, sQuestions);
									rvSecurityAns.setAdapter(verifySecurityQuestionAdapter);
									verifySecurityQuestionAdapter.notifyDataSetChanged();

									// if secuerty did not matches
									Alert.ShowAlert(VerifySecurityQuestionActivity.this, jsonObject.getString("ResponseMessage"));
								}

							} catch (Exception e) {
								e.printStackTrace();
								progress.hideDialog();
							}
						}

						@Override
						public void onFailure(Call<ResponseBody> call, Throwable t) {
							super.onFailure(call, t);
							progress.hideDialog();
							Toast.makeText(VerifySecurityQuestionActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
						}
					});

		} catch (Exception e) {
			e.printStackTrace();
			progress.hideDialog();
			Toast.makeText(VerifySecurityQuestionActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
		}
	}

	private void ForgetPasswordOTP() {
		try {
			JSONObject reqJsonObject = new JSONObject();
			reqJsonObject.put("function", "ForgetPasswordOTP");
			JSONObject reqParamJsonObject = new JSONObject();
			reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
			reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
			reqParamJsonObject.put("ArgClientID", custId);
			reqJsonObject.putOpt("parameter", reqParamJsonObject);
			String dataValue = reqJsonObject.toString();
			Timber.e(dataValue, "Request");

			API.getInstance().encryptRequest(VerifySecurityQuestionActivity.this, dataValue, Constants.PUBLIC_KEY,
					Constants.CONNECTION_ID, Constants.AUTH_KEY,
					Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
					new RetrofitCallbacks<ResponseBody>(VerifySecurityQuestionActivity.this) {
						@Override
						public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
							super.onResponse(call, response);
							try {
								String responseString = response.body().string();
								String responseDataValue = decryptData.parseResponse(responseString);
								Timber.e(responseDataValue, "Response");

								JSONObject jsonObject = new JSONObject(responseDataValue);

								progress.hideDialog();

								if (jsonObject.getString("Status").equals("1")) {
									Intent intent = new Intent(VerifySecurityQuestionActivity.this, ForgotPINVerificationActivity.class);
									intent.putExtra("cust_id", custId);
									intent.putExtra("number", Constants.MOBILE_NUMBER);
									startActivity(intent);
									finish();
								} else {
									Alert.ShowAlert(VerifySecurityQuestionActivity.this, jsonObject.getString("ResponseMessage"));
								}

							} catch (Exception e) {
								e.printStackTrace();
								progress.hideDialog();
							}
						}

						@Override
						public void onFailure(Call<ResponseBody> call, Throwable t) {
							super.onFailure(call, t);
							progress.hideDialog();
							Toast.makeText(VerifySecurityQuestionActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
						}
					});

		} catch (Exception e) {
			e.printStackTrace();
			progress.hideDialog();
			Toast.makeText(VerifySecurityQuestionActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
		}
	}

}
