package com.amcblanand.android.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.amcblanand.android.R;
import com.amcblanand.android.api.API;
import com.amcblanand.android.api.RetrofitCallbacks;
import com.amcblanand.android.base.BaseActivity;
import com.amcblanand.android.extra.Alert;
import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.DecryptData;
import com.amcblanand.android.extra.Progress;
import com.amcblanand.android.model.SquestionPojo;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class LoginOTPVerificationActivity extends BaseActivity {

    @BindView(R.id.et_otp)
    EditText etOtp;
    @BindView(R.id.btn_resend_otp)
    Button btnResendOtp;
    @BindView(R.id.btn_login_otp_verification)
    Button btnLoginOtpVerification;

    @BindView(R.id.check_terms)
    AppCompatCheckBox checkTerms;
    @BindView(R.id.tv_terms_condition)
    AppCompatTextView tvTermsCondition;
    @BindView(R.id.linear_terms_condition)
    LinearLayoutCompat linearTermsCondition;

    private DecryptData decryptData;
    private Progress progress;
    private String termsAndCondition = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_otpverification);
        ButterKnife.bind(this);

        progress = new Progress(LoginOTPVerificationActivity.this);

        decryptData = new DecryptData();
        Intent intent = getIntent();

        if (Constants.TERMS_CONDITION.equals("Y")) {
            linearTermsCondition.setVisibility(View.GONE);
        } else {
            linearTermsCondition.setVisibility(View.VISIBLE);
        }

        btnLoginOtpVerification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etOtp.getText().toString().trim().length() < 7) {
                    Toast.makeText(LoginOTPVerificationActivity.this, "7 digit required.", Toast.LENGTH_SHORT).show();
                } else {
                    hideKeyboard();
                    if (Constants.TERMS_CONDITION.equals("Y")) {
                        progress.createDialog(false);
                        progress.dialogMessage("Verifying OTP");
                        progress.showDialog();
                        checkOTPCode();
                    } else {
                        if (termsAndCondition.equals("Y")) {
                            progress.createDialog(false);
                            progress.dialogMessage("Please wait..");
                            progress.showDialog();
                            updateUserTc();
                        } else {
                            Alert.ShowAlert(LoginOTPVerificationActivity.this, "Please select terms & condition.");
                        }
                    }
                }
            }
        });


        tvTermsCondition.setOnClickListener(tvTermsConditionView -> {
            Intent urlIntent = new Intent(LoginOTPVerificationActivity.this, OtherBankServices.class);
            urlIntent.putExtra("URL", "http://www.amcblanand.com/terms-conditions.php");
            startActivity(urlIntent);
        });

        checkTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    // TODO: 04-07-2018 CALL SERVICE
                    termsAndCondition = "Y";
                } else {
                    termsAndCondition = "N";
                }
            }
        });

//        etOtp.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if (charSequence.length() == 7) {
//                    progress.createDialog(false);
//                    progress.dialogMessage("Verifying OTP");
//                    progress.showDialog();
//                    checkOTPCode();
//                } else {
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//            }
//        });

        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                progress.createDialog(false);
                progress.dialogMessage("Sending OTP again");
                progress.showDialog();
                resendOTP();
            }
        });
    }

    private void resendOTP() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "SendOTPCode");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("Argacccode", "");
            reqParamJsonObject.put("Argbranchcode", "");
            reqParamJsonObject.put("Argactyp", "");
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgSource", "WL");
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, "Request");

            API.getInstance().encryptRequest(LoginOTPVerificationActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(LoginOTPVerificationActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, "Response");

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    Toast.makeText(LoginOTPVerificationActivity.this, jsonObject.getString("ResponseMessage"), Toast.LENGTH_SHORT).show();
                                } else {
                                    Alert.ShowAlert(LoginOTPVerificationActivity.this, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void checkOTPCode() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "CheckOTPCodeOpt");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgOtpCode", etOtp.getText().toString().trim());
            reqParamJsonObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgSource", "WL");
            reqParamJsonObject.put("ArgBrCode", "");
            reqParamJsonObject.put("ArgActyp", "");
            reqParamJsonObject.put("ArgAccCode", "");
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, "Request");

            API.getInstance().encryptRequest(LoginOTPVerificationActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(LoginOTPVerificationActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, "Response");


                                JSONObject jsonObject = new JSONObject(responseDataValue);
                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.hideDialog();
                                    Toast.makeText(LoginOTPVerificationActivity.this, jsonObject.getString("ResponseMessage"),
                                            Toast.LENGTH_SHORT).show();

                                    if (Constants.SECURITY_QUESTIONS_EXITS.equalsIgnoreCase("N")) {
                                        progress.createDialog(false);
                                        progress.dialogMessage("Please wait..");
                                        progress.showDialog();
                                        getSecurityQueAL();
                                    } else {
                                        if (Constants.SECURITY_QUESTIONS_VERIFIED.equalsIgnoreCase("n")) {
                                            progress.createDialog(false);
                                            progress.dialogMessage("Please wait..");
                                            progress.showDialog();
                                            getSecurityQueAL();
                                        } else {
                                            finish();
                                            startActivity(new Intent(LoginOTPVerificationActivity.this, DashboardActivity.class));
                                        }
                                    }
                                } else {
                                    progress.hideDialog();
                                    Alert.ShowAlert(LoginOTPVerificationActivity.this, jsonObject.getString("ResponseMessage"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

    private void getSecurityQueAL() {
        try {
            JSONObject reqJsonObject = new JSONObject();
            reqJsonObject.put("function", "GetSecurityQuestionAL");
            JSONObject reqParamJsonObject = new JSONObject();
            reqParamJsonObject.put("ArgBankId", Constants.BANK_ID);
            reqParamJsonObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamJsonObject.put("ArgClientID", Constants.CUST_ID);
            if (Constants.SOURCE_FROM.equalsIgnoreCase("FP")) {
                reqParamJsonObject.put("ArgType", "F");
            } else {

                if (Constants.SECURITY_QUESTIONS_EXITS.equalsIgnoreCase("n")) {
                    reqParamJsonObject.put("ArgType", "EE");
                } else {
                    reqParamJsonObject.put("ArgType", "E");
                }
            }
            reqJsonObject.putOpt("parameter", reqParamJsonObject);
            String dataValue = reqJsonObject.toString();
            Timber.e(dataValue, "Request");

            API.getInstance().encryptRequest(LoginOTPVerificationActivity.this, dataValue, Constants.PUBLIC_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(LoginOTPVerificationActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                String responseDataValue = decryptData.parseResponse(responseString);
                                Timber.e(responseDataValue, "Response");


                                SquestionPojo squestionPojo = SquestionPojo.objectFromData(responseDataValue);

                                progress.hideDialog();

                                if (squestionPojo.getStatus() == 1) {

                                    Constants.IS_LOGIN = "Y";

                                    if (Constants.SOURCE_FROM.equalsIgnoreCase("FP")) {
                                        //when forgot password
                                        Intent intent1 = new Intent(LoginOTPVerificationActivity.this, VerifySecurityQuestionActivity.class);
                                        intent1.putExtra("responseDataValue", responseDataValue);
                                        intent1.putExtra("ArgType", "F");
                                        intent1.putExtra("cust_id", "");
                                        intent1.putExtra("pwd", "");
                                        startActivity(intent1);
                                        finish();
                                    } else {
                                        if (Constants.SECURITY_QUESTIONS_EXITS.equalsIgnoreCase("n")) {
                                            Intent intent1 = new Intent(LoginOTPVerificationActivity.this, SecurityQuestionActivity.class);
                                            intent1.putExtra("responseDataValue", responseDataValue);
                                            intent1.putExtra("ArgType", "EE");
                                            intent1.putExtra("cust_id", Constants.CUST_ID);
                                            intent1.putExtra("pwd", "");
                                            startActivity(intent1);
                                            finish();
                                        } else {
                                            Intent intent1 = new Intent(LoginOTPVerificationActivity.this, VerifySecurityQuestionActivity.class);
                                            intent1.putExtra("responseDataValue", responseDataValue);
                                            intent1.putExtra("ArgType", "E");
                                            intent1.putExtra("cust_id", Constants.CUST_ID);
                                            intent1.putExtra("pwd", "");
                                            startActivity(intent1);
                                            finish();
                                        }

                                    }


                                } else {
                                    Alert.ShowAlert(LoginOTPVerificationActivity.this, squestionPojo.getResponseMessage());
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                            Toast.makeText(LoginOTPVerificationActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
            Toast.makeText(LoginOTPVerificationActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }
    }


    private void updateUserTc() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("function", "UpdateUserTC");

            JSONObject reqParamObject = new JSONObject();
            reqParamObject.put("ArgBankId", Constants.BANK_ID);
            reqParamObject.put("ArgIMEI", Constants.DEVICE_ID);
            reqParamObject.put("ArgMobile", Constants.MOBILE_NUMBER);
            reqParamObject.put("ArgClientID", Constants.CUST_ID);
            jsonObject.putOpt("parameter", reqParamObject);

            String dataValue = jsonObject.toString();
            Timber.e(dataValue, "LOGIN");

            API.getInstance().encryptRequestForTC(LoginOTPVerificationActivity.this, dataValue, Constants.REMOVE_REGISTRATION_KEY,
                    Constants.CONNECTION_ID, Constants.AUTH_KEY,
                    Constants.VERSION_NUMBER, Constants.DEVICE_ID, Constants.IP, Constants.DEVICE_TYPE,
                    new RetrofitCallbacks<ResponseBody>(LoginOTPVerificationActivity.this) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            try {
                                String responseString = response.body().string();
                                Timber.e(responseString, "LOGIN");
                                String responseDataValue = decryptData.parseResponseRemoveRegistration(responseString);
                                Timber.e(responseDataValue, "LOGIN");

                                progress.hideDialog();

                                JSONObject jsonObject = new JSONObject(responseDataValue);

                                if (jsonObject.getString("Status").equals("1")) {
                                    progress.createDialog(false);
                                    progress.dialogMessage("Verifying OTP");
                                    progress.showDialog();
                                    checkOTPCode();

                                } else {
                                    progress.hideDialog();
                                    Toast.makeText(LoginOTPVerificationActivity.this, jsonObject.getString("ResponseMessage"), Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progress.hideDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                            progress.hideDialog();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            progress.hideDialog();
        }
    }

}
