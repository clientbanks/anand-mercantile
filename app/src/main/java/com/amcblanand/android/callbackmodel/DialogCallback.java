package com.amcblanand.android.callbackmodel;

/**
 * Created by rakshit.sathwara on 8/1/2017.
 */

public class DialogCallback {
    public String Text, Value, dialogData, type;

    public DialogCallback(String text, String value, String dialogData, String type) {
        Text = text;
        Value = value;
        this.dialogData = dialogData;
        this.type = type;
    }


    public String getText() {
        return Text;
    }

    public String getValue() {
        return Value;
    }

    public String getDialogData() {
        return dialogData;
    }

    public String getType() {
        return type;
    }
}
