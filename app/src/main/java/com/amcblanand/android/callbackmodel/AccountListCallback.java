package com.amcblanand.android.callbackmodel;

/**
 * Created by rakshit.sathwara on 11/14/2017.
 */

public class AccountListCallback {
    String BankId, BranchCode, Actyp, AccCode, CurBal, Name, Gender, Acno, Email;

    public AccountListCallback(String bankId, String branchCode, String actyp, String accCode, String curBal, String name, String gender, String acno, String email) {
        BankId = bankId;
        BranchCode = branchCode;
        Actyp = actyp;
        AccCode = accCode;
        CurBal = curBal;
        Name = name;
        Gender = gender;
        Acno = acno;
        Email = email;
    }

    public String getBankId() {
        return BankId;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public String getActyp() {
        return Actyp;
    }

    public String getAccCode() {
        return AccCode;
    }

    public String getCurBal() {
        return CurBal;
    }

    public String getName() {
        return Name;
    }

    public String getGender() {
        return Gender;
    }

    public String getAcno() {
        return Acno;
    }

    public String getEmail() {
        return Email;
    }
}
