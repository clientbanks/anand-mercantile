package com.amcblanand.android.callbackmodel;

public class DebitCardCallBack {

	String status;

	public DebitCardCallBack(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
}
