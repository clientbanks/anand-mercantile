package com.amcblanand.android.callbackmodel;

/**
 * Created by rakshit.sathwara on 10/6/2017.
 */

public class DeletePayeeCallback {
    public String status;

    public DeletePayeeCallback(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
