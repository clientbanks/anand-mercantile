package com.amcblanand.android.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by rakshit.sathwara on 2/28/2017.
 */

public interface ApiInterface {

    /*@POST("MasterService.asmx/DoWebRequest")
    Call<ResponseBody> getReqResponse(@Header("Content-Type") String contentType, @Header("PublicKey") String key, @Header("ConnectionId") String connectionId,
                                      @Header("Authorization") String auth, @Header("VersionNumber") String versionNo,
                                      @Header("DeviceID") String deviceId, @Header("IPv4") String ip, @Header("DeviceType") String deviceType,
                                      @Body String reqDataValue);*/

    @POST("MasterService.asmx/DoWebRequest")
    Call<ResponseBody> getReqResponse(@Header("Content-Type") String contentType, @Header("PublicKey") String key, @Header("ConnectionId") String connectionId,
                                      @Header("Authorization") String auth, @Header("VersionNumber") String versionNo,
                                      @Header("DeviceID") String deviceId, @Header("DeviceType") String deviceType, @Header("DEVICE_NAME") String deviceName,
                                      @Body String reqDataValue);
}
