package com.amcblanand.android.api;

import android.content.Context;
import androidx.annotation.CallSuper;
import android.widget.Toast;

import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rakshit.sathwara on 2/28/2017.
 */

public class RetrofitCallbacks<T> implements Callback<T> {

    //public static final String TAG = "RETRO-CALLBACK";
    final Context context;

    public RetrofitCallbacks(Context context) {
        this.context = context;
    }

    /**
     * Invoked for a received HTTP response.
     * <p>
     * Note: An HTTP response may still indicate an application-level failure such as a 404 or 500.
     * Call {@link Response#isSuccessful()} to determine if the response indicates success.
     *
     * @param call
     * @param response
     */
    @CallSuper
    @Override
    public void onResponse(Call<T> call, Response<T> response) {

        if (response.isSuccessful()) {

            if (response.code() != HttpURLConnection.HTTP_OK) {
//                System.out.println(response.errorBody().toString());
            } else if (response.code() == HttpURLConnection.HTTP_OK) {
//                System.out.println(response.body().toString());
            }
        }
    }

    /**
     * Invoked when a network exception occurred talking to the server or when an unexpected
     * exception occurred creating the request or processing the response.
     *
     * @param call
     * @param t
     */

    @CallSuper
    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (!call.isCanceled()) {
            if (!call.isCanceled()) {
                Toast.makeText(context, "Please Check Internet Connection.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
