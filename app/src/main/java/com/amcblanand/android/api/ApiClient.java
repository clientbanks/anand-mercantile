package com.amcblanand.android.api;

import android.util.Log;

import com.amcblanand.android.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.AMCBLBank;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rakshit.sathwara on 2/28/2017.
 */

public class ApiClient {

	private static final String TAG = "ApiClient";

	private static final String CACHE_CONTROL = "Cache-Control";

	private static Retrofit retrofit = null;

	public static Retrofit getClient() {

		OkHttpClient okHttpClient;

		if (BuildConfig.DEBUG) {
			HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
			httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


			okHttpClient = new OkHttpClient.Builder()
					.cache(provideCache()) // 20 MB
					.addNetworkInterceptor(provideCacheInterceptor())
					.addInterceptor(httpLoggingInterceptor)
					.addInterceptor(provideOfflineCacheInterceptor())
					.readTimeout(10, TimeUnit.MINUTES)
					.connectTimeout(10, TimeUnit.MINUTES)
					.build();
		} else {
			okHttpClient = new OkHttpClient.Builder()
					.cache(provideCache()) // 20 MB
					.addNetworkInterceptor(provideCacheInterceptor())
					.addInterceptor(provideOfflineCacheInterceptor())
					.readTimeout(10, TimeUnit.MINUTES)
					.connectTimeout(10, TimeUnit.MINUTES)
					.build();
		}


		Gson gson = new GsonBuilder()
				.setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
				.create();

		if (retrofit == null) {
			retrofit = new Retrofit.Builder()
					.client(okHttpClient)
					.baseUrl(Constants.API_BASE_URL)
					.addConverterFactory(GsonConverterFactory.create(gson))
					.build();
		}
		return retrofit;
	}

	private static Cache provideCache() {
		Cache cache = null;
		try {
			cache = new Cache(new File(AMCBLBank.getInstance().getCacheDir(), "http-cache"),
					10 * 1024 * 1024); // 10 MB
		} catch (Exception e) {
			Log.i(TAG, "Could not create Cache!");
		}
		return cache;
	}

	public static Interceptor provideCacheInterceptor() {
		return new Interceptor() {
			@Override
			public Response intercept(Chain chain) throws IOException {
				Response response = chain.proceed(chain.request());

				// re-write response header to force use of cache
				CacheControl cacheControl = new CacheControl.Builder()
						.maxAge(2, TimeUnit.MINUTES)
						.build();

				return response.newBuilder()
						.header(CACHE_CONTROL, cacheControl.toString())
						.build();
			}
		};
	}


	public static Interceptor provideOfflineCacheInterceptor() {
		return new Interceptor() {
			@Override
			public Response intercept(Chain chain) throws IOException {
				Request request = chain.request();

				if (!AMCBLBank.hasNetwork()) {
					CacheControl cacheControl = new CacheControl.Builder()
							.maxStale(7, TimeUnit.DAYS)
							.build();

					request = request.newBuilder()
							.cacheControl(cacheControl)
							.build();
				}
				return chain.proceed(request);
			}
		};
	}

}