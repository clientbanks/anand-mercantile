package com.amcblanand.android.api;

import android.content.Context;

import com.amcblanand.android.extra.Constants;
import com.amcblanand.android.extra.CryptLib;

import okhttp3.ResponseBody;
import retrofit2.Call;
import timber.log.Timber;

/**
 * Created by rakshit.sathwara on 2/28/2017.
 */

public class API {
    private static final String TAG = "API";

    private static API instance = new API();
    private ApiInterface apiService;

    private CryptLib cryptLib;

    private String responseValue;
    String publicKey, privateKey, secretKey;

    private API() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        try {
            cryptLib = new CryptLib();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static API getInstance() {
        return instance;
    }

    public void encryptRequest(final Context context, final String data, final String key, final String ConnectionId,
                               final String Authorization, final String VersionNumber, final String DeviceId,
                               final String IPv4, final String DeviceType, final RetrofitCallbacks<ResponseBody> callbacks) {
        try {
            publicKey = CryptLib.SHA256(Constants.PUBLIC_KEY, 32);

            Timber.e(TAG + " - publicKey - " + publicKey);
            Timber.e(TAG + " - privateKey - " + Constants.PRIVATE_KEY);
            Timber.e(TAG + " - secretKey - " + Constants.SECRET_KEY);

            privateKey = Constants.PRIVATE_KEY;
            secretKey = Constants.SECRET_KEY;

            Timber.e(TAG + " - data - " + data);

            String reqString = cryptLib.encrypt(data.replace("\n", ""), publicKey, privateKey);

            Timber.e(TAG + " - reqString first - " + reqString);

            String encryptedReqString = cryptLib.encrypt(reqString, publicKey, secretKey);
            Timber.e(TAG + " - encryptedReqString - " + encryptedReqString);


            String firstDec = cryptLib.decrypt(encryptedReqString, publicKey, secretKey);
            Timber.e(TAG + "encryptRequest: firstDec " + firstDec);

            String secondDec = cryptLib.decrypt(firstDec, publicKey, privateKey);
            Timber.e(TAG + "encryptRequest: secondDec " + secondDec);

            Call<ResponseBody> encryResponseBodyCall = apiService.getReqResponse("text/plain", key, ConnectionId, Authorization, VersionNumber, DeviceId,
                    DeviceType, Constants.DEVICE_NAME, encryptedReqString.trim().replace("\n", ""));
            encryResponseBodyCall.enqueue(callbacks);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void encryptRequestRemoveRegistration(final Context context, final String data, final String key, final String ConnectionId,
                                                 final String Authorization, final String VersionNumber, final String DeviceId,
                                                 final String IPv4, final String DeviceType, final RetrofitCallbacks<ResponseBody> callbacks) {
        try {
            publicKey = CryptLib.SHA256(Constants.REMOVE_REGISTRATION_KEY, 32);

            Timber.e(TAG + " - publicKey - " + publicKey);
            Timber.e(TAG + " - privateKey - " + Constants.PRIVATE_KEY);
            Timber.e(TAG + " - secretKey - " + Constants.SECRET_KEY);

            privateKey = Constants.PRIVATE_KEY;
            secretKey = Constants.SECRET_KEY;

            Timber.e(TAG + " - data - " + data);

            String reqString = cryptLib.encrypt(data.replace("\n", ""), publicKey, privateKey);

            Timber.e(TAG + " - reqString first - " + reqString);

            String encryptedReqString = cryptLib.encrypt(reqString, publicKey, secretKey);
            Timber.e(TAG + " - encryptedReqString - " + encryptedReqString);


            String firstDec = cryptLib.decrypt(encryptedReqString, publicKey, secretKey);
            Timber.e(TAG, "encryptRequest: firstDec " + firstDec);

            String secondDec = cryptLib.decrypt(firstDec, publicKey, privateKey);
            Timber.e(TAG, "encryptRequest: secondDec " + secondDec);

            Call<ResponseBody> encryResponseBodyCall = apiService.getReqResponse("text/plain", key, ConnectionId, Authorization, VersionNumber, DeviceId,
                    IPv4, DeviceType, encryptedReqString.trim().replace("\n", ""));
            encryResponseBodyCall.enqueue(callbacks);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void encryptRequestForTC(final Context context, final String data, final String key, final String ConnectionId,
                                    final String Authorization, final String VersionNumber, final String DeviceId,
                                    final String IPv4, final String DeviceType, final RetrofitCallbacks<ResponseBody> callbacks) {
        try {
            publicKey = CryptLib.SHA256(Constants.REMOVE_REGISTRATION_KEY, 32);

            Timber.e(TAG + " - publicKey - " + publicKey);
            Timber.e(TAG + " - privateKey - " + Constants.PRIVATE_KEY);
            Timber.e(TAG + " - secretKey - " + Constants.SECRET_KEY);

            privateKey = Constants.PRIVATE_KEY;
            secretKey = Constants.SECRET_KEY;

            Timber.e(TAG + " - data - " + data);

            String reqString = cryptLib.encrypt(data.replace("\n", ""), publicKey, privateKey);

            Timber.e(TAG + " - reqString first - " + reqString);

            String encryptedReqString = cryptLib.encrypt(reqString, publicKey, secretKey);
            Timber.e(TAG + " - encryptedReqString - " + encryptedReqString);


            String firstDec = cryptLib.decrypt(encryptedReqString, publicKey, secretKey);
            Timber.e(TAG + "encryptRequest: firstDec " + firstDec);

            String secondDec = cryptLib.decrypt(firstDec, publicKey, privateKey);
            Timber.e(TAG + "encryptRequest: secondDec " + secondDec);

            Call<ResponseBody> encryResponseBodyCall = apiService.getReqResponse("text/plain", key, ConnectionId, Authorization, VersionNumber, DeviceId,
                    DeviceType, Constants.DEVICE_NAME, encryptedReqString.trim().replace("\n", ""));
            encryResponseBodyCall.enqueue(callbacks);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
