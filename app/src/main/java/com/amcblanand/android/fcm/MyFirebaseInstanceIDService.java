package com.amcblanand.android.fcm;

import android.preference.PreferenceManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import com.amcblanand.android.extra.Constants;
import timber.log.Timber;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FCMId";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Timber.d("TOKEN", refreshedToken);

        PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .putString(Constants.FCM_KEY, refreshedToken)
                .apply();
    }
}
